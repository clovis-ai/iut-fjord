/**
 * Tests des couleurs.
 */
package fjord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;


import java.util.Arrays;

/**
 * Tests des couleurs.
 * 
 * @author Olivier
 */
public class CouleurTest {

	/**
	 * Tests des étiquettes des couleurs.
	 */
	@Test
	public void testsEtiquettes() {
		// Nombre de couleurs.
		assertEquals(2, Couleur.values().length);
		// Étiquettes des couleurs.
		assertTrue(Arrays.asList(Couleur.values()).contains(Couleur.FONCE));
		assertTrue(Arrays.asList(Couleur.values()).contains(Couleur.CLAIR));
	}

	/**
	 * Tests des noms des couleurs.
	 */
	@Test
	public void testsNoms() {
		assertFalse(Couleur.FONCE.getNom().equals(Couleur.CLAIR.getNom()));
	}

	/**
	 * Tests des couleurs RGB des couleurs.
	 */
	@Test
	public void testsRGB() {
		assertFalse(Couleur.FONCE.getRGB().getRGB() == Couleur.CLAIR.getRGB().getRGB());
	}

	/**
	 * Tests des couleurs RGB des couleurs au format hexadécimal.
	 */
	@Test
	public void testsRGBHex() {
		for (Couleur couleur : Couleur.values()) {
			String rgbHex = couleur.getRGBHex();
			assertNotNull(rgbHex);
			assertEquals(6, rgbHex.length());
		}
	}

}
