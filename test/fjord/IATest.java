/**
 * Tests des joueurs « Intelligence artificielle ».
 */
package fjord;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;


/**
 * Tests des joueurs « Intelligence artificielle ».
 * 
 * @author Olivier
 */
public class IATest {

	/**
	 * Tests des joueurs « Intelligence artificielle ».
	 */
	@Test
	public void tests() {
		IA joueurIA;
		IA autreJoueurIA;
		// Joueur.
		joueurIA = new IA(Couleur.FONCE);
		// Autre joueur « Intelligence artificielle » (de même couleur !).
		autreJoueurIA = new IA(Couleur.FONCE);
		new Case(TuileOrientee.AAAAAA, null, null);
		new Case(TuileOrientee.AAAAAB, joueurIA, Pion.HUTTE);
		new Case(TuileOrientee.AAAAAC, joueurIA, Pion.CHAMP);
		new Case(TuileOrientee.AAAABA, autreJoueurIA, Pion.HUTTE);
		new Case(TuileOrientee.AAAABB, autreJoueurIA, Pion.CHAMP);
		// Erreur car pas de couleur.
		try {
			joueurIA = new IA(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
	}

	/**
	 * Tests, lors de la phase de découverte de la manche, joue une tuile soit
	 * en la choisissant parmi celles de la pioche des tuiles faces visibles qui
	 * peuvent être posées, soit demandant qu'elle soit tirée aléatoirement
	 * parmi celles de la pioche des tuiles faces cachées.
	 */
	@Test
	public void testsJouerDecouvertePioche() {
		IA joueurIA = new IA(Couleur.FONCE);
		// Erreur car pas de pioche.
		try {
			joueurIA.jouerDecouvertePioche(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Erreur car pioche vide.
		try {
			joueurIA.jouerDecouvertePioche(new ArrayList<Tuile>());
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Une seule ou toutes les tuiles.
		List<Tuile> piocheTuilesVisiblesPossiblePoser1SeuleTuile = new ArrayList<Tuile>();
		Tuile laSeuleTuile = Tuile.AAAAAA;
		piocheTuilesVisiblesPossiblePoser1SeuleTuile.add(laSeuleTuile);
		List<Tuile> piocheTuilesVisiblesPossiblePoserToutesTuiles = new ArrayList<Tuile>();
		piocheTuilesVisiblesPossiblePoserToutesTuiles.addAll(Arrays.asList(Tuile.values()));
		final int NB_ITERATIONS = 10000;
		for (int cpt = 1; cpt <= NB_ITERATIONS; ++cpt) {
			Tuile tuile;
			tuile = joueurIA.jouerDecouvertePioche(piocheTuilesVisiblesPossiblePoser1SeuleTuile);
			assertTrue(tuile == null || tuile == laSeuleTuile);
			tuile = joueurIA.jouerDecouvertePioche(piocheTuilesVisiblesPossiblePoserToutesTuiles);
			assertTrue(tuile == null || piocheTuilesVisiblesPossiblePoserToutesTuiles.contains(tuile));
		}
	}

	/**
	 * Tests, lors de la phase de découverte de la manche, joue une coordonnée
	 * (ligne et colonne) où poser la tuile piochée.
	 */
	@Test
	public void testsJouerDecouvertePoseCoordLigCol() {
		IA joueurIA = new IA(Couleur.FONCE);
		// Erreur car pas de coordonnées.
		try {
			joueurIA.jouerDecouvertePoseCoordLigCol(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Erreur car coordonnées vide.
		try {
			joueurIA.jouerDecouvertePoseCoordLigCol(new ArrayList<Point>());
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Une seule ou plusieurs coordonnées.
		List<Point> coordLigColTuilePossiblePoser1SeuleCoord = new ArrayList<Point>();
		Point laSeuleCoord = new Point(0, 0);
		coordLigColTuilePossiblePoser1SeuleCoord.add(laSeuleCoord);
		List<Point> coordLigColTuilePossiblePoserPlusieursCoord = new ArrayList<Point>();
		coordLigColTuilePossiblePoserPlusieursCoord
				.addAll(Arrays.asList(new Point(1, 1), new Point(2, 2), new Point(3, 3)));
		final int NB_ITERATIONS = 10000;
		for (int cpt = 1; cpt <= NB_ITERATIONS; ++cpt) {
			Point coord;
			coord = joueurIA.jouerDecouvertePoseCoordLigCol(coordLigColTuilePossiblePoser1SeuleCoord);
			assertTrue(coord == null || coord == laSeuleCoord);
			coord = joueurIA.jouerDecouvertePoseCoordLigCol(coordLigColTuilePossiblePoserPlusieursCoord);
			assertTrue(coord == null || coordLigColTuilePossiblePoserPlusieursCoord.contains(coord));
		}
	}

	/**
	 * Tests, lors de la phase de découverte de la manche, joue une tuile
	 * orientée à poser pour la tuile piochée.
	 */
	@Test
	public void testsJouerDecouvertePoseTuileOrientee() {
		IA joueurIA = new IA(Couleur.FONCE);
		// Erreur car pas de tuiles orientées.
		try {
			joueurIA.jouerDecouvertePoseTuileOrientee(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Erreur car tuiles orientées vide.
		try {
			joueurIA.jouerDecouvertePoseTuileOrientee(new ArrayList<TuileOrientee>());
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Une seule ou toutes les tuiles orientées.
		List<TuileOrientee> tuilesOrienteesTuilePossiblePoser1SeuleTuile = new ArrayList<TuileOrientee>();
		TuileOrientee laSeuleTuileOrientee = TuileOrientee.AAAAAA;
		tuilesOrienteesTuilePossiblePoser1SeuleTuile.add(laSeuleTuileOrientee);
		List<TuileOrientee> tuilesOrienteesTuilePossiblePoserToutesTuiles = new ArrayList<TuileOrientee>();
		tuilesOrienteesTuilePossiblePoserToutesTuiles.addAll(Arrays.asList(TuileOrientee.values()));
		final int NB_ITERATIONS = 10000;
		for (int cpt = 1; cpt <= NB_ITERATIONS; ++cpt) {
			TuileOrientee tuileOrientee;
			tuileOrientee = joueurIA.jouerDecouvertePoseTuileOrientee(tuilesOrienteesTuilePossiblePoser1SeuleTuile);
			assertTrue(tuileOrientee == null || tuileOrientee == laSeuleTuileOrientee);
			tuileOrientee = joueurIA.jouerDecouvertePoseTuileOrientee(tuilesOrienteesTuilePossiblePoserToutesTuiles);
			assertTrue(tuileOrientee == null || tuilesOrienteesTuilePossiblePoserToutesTuiles.contains(tuileOrientee));
		}
	}

	/**
	 * Tests, lors de la phase de découverte de la manche, indique si une hutte
	 * doit être installée sur la tuile piochée et posée.
	 */
	@Test
	public void testsJouerDecouverteInstallationHutte() {
		// Rien à tester !
	}

	/**
	 * Tests, lors de la phase de colonisation de la manche, joue une coordonnée
	 * (ligne et colonne) d'une case où installer un champ.
	 */
	@Test
	public void testsJouerColonisationInstallationChamp() {
		IA joueurIA = new IA(Couleur.FONCE);
		// Erreur car pas de coordonnées.
		try {
			joueurIA.jouerColonisationInstallationChamp(null, null, null, null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Erreur car coordonnées (pour le joueur) vide.
		try {
			joueurIA.jouerColonisationInstallationChamp(new ArrayList<Point>(), null, null, null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Une seule ou plusieurs coordonnées.
		List<Point> coordLigColPossibleInstallerChamp1SeuleCoord = new ArrayList<Point>();
		Point laSeuleCoord = new Point(0, 0);
		coordLigColPossibleInstallerChamp1SeuleCoord.add(laSeuleCoord);
		List<Point> coordLigColPossibleInstallerChampPlusieursCoord = new ArrayList<Point>();
		coordLigColPossibleInstallerChampPlusieursCoord
				.addAll(Arrays.asList(new Point(1, 1), new Point(2, 2), new Point(3, 3)));
		final int NB_ITERATIONS = 10000;
		for (int cpt = 1; cpt <= NB_ITERATIONS; ++cpt) {
			Point coord;
			coord = joueurIA.jouerColonisationInstallationChamp(coordLigColPossibleInstallerChamp1SeuleCoord, null,
					null, null);
			assertTrue(coord == null || coord == laSeuleCoord);
			coord = joueurIA.jouerColonisationInstallationChamp(coordLigColPossibleInstallerChampPlusieursCoord, null,
					null, null);
			assertTrue(coord == null || coordLigColPossibleInstallerChampPlusieursCoord.contains(coord));
		}
	}

}
