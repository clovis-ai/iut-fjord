/**
 * Tests des états des manches.
 */
package fjord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import java.util.Arrays;


/**
 * Tests des états des manches.
 * 
 * @author Olivier
 */
public class EtatMancheTest {

	/**
	 * Tests étiquettes des états des manches.
	 */
	@Test
	public void testsEtiquettes() {
		// Nombre d'états des manches.
		assertEquals(4, EtatManche.values().length);
		// Étiquettes des états des manches.
		assertTrue(Arrays.asList(EtatManche.values()).contains(EtatManche.DEBUT));
		assertTrue(Arrays.asList(EtatManche.values()).contains(EtatManche.DECOUVERTE));
		assertTrue(Arrays.asList(EtatManche.values()).contains(EtatManche.COLONISATION));
		assertTrue(Arrays.asList(EtatManche.values()).contains(EtatManche.FIN));
	}

}
