/**
 * Tests des directions.
 */
package fjord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;


import java.util.Arrays;
import java.awt.Point;

/**
 * Tests des directions.
 * 
 * @author Olivier
 */
public class DirectionTest {

	/**
	 * Tests des étiquettes des directions.
	 */
	@Test
	public void testsEtiquettes() {
		// Nombre de directions.
		assertEquals(6, Direction.values().length);
		// Étiquettes des directions.
		assertTrue(Arrays.asList(Direction.values()).contains(Direction.DIRECTION_0_DEGRE));
		assertTrue(Arrays.asList(Direction.values()).contains(Direction.DIRECTION_60_DEGRES));
		assertTrue(Arrays.asList(Direction.values()).contains(Direction.DIRECTION_120_DEGRES));
		assertTrue(Arrays.asList(Direction.values()).contains(Direction.DIRECTION_180_DEGRES));
		assertTrue(Arrays.asList(Direction.values()).contains(Direction.DIRECTION_240_DEGRES));
		assertTrue(Arrays.asList(Direction.values()).contains(Direction.DIRECTION_300_DEGRES));
	}

	/**
	 * Tests des directions (en degrés).
	 */
	@Test
	public void testsDirectionDegres() {
		// Correspondance direction (en degrés) et indice.
		for (int indDirection = 0; indDirection < Direction.values().length; ++indDirection) {
			assertEquals(indDirection * 360 / Direction.NB_DIRECTIONS,
					Direction.values()[indDirection].getDirectionDegres());
		}
		// Correspondance direction (en degrés) et étiquette.
		for (Direction direction : Direction.values()) {
			assertEquals(direction.getDirectionDegres(), Integer.parseInt(direction.toString().split("_")[1]));
		}
	}

	/**
	 * Tests des directions (en degrés) opposées.
	 */
	@Test
	public void testsDirectionDegresOpposee() {
		for (Direction direction : Direction.values()) {
			assertTrue(Math.max(direction.getDirectionDegres(), direction.getDirectionDegresOpposee())
					- Math.min(direction.getDirectionDegres(), direction.getDirectionDegresOpposee()) == 180);
			assertTrue(Math.max(direction.getDirectionDegres(), direction.directionOpposee().getDirectionDegres())
					- Math.min(direction.getDirectionDegres(),
							direction.directionOpposee().getDirectionDegres()) == 180);
		}
	}

	/**
	 * Tests des directions opposées.
	 */
	@Test
	public void testsDirectionOpposee() {
		// Teste si les valeurs sont correctes.
		assertTrue(Direction.DIRECTION_0_DEGRE == Direction.DIRECTION_180_DEGRES.directionOpposee());
		assertTrue(Direction.DIRECTION_60_DEGRES == Direction.DIRECTION_240_DEGRES.directionOpposee());
		assertTrue(Direction.DIRECTION_120_DEGRES == Direction.DIRECTION_300_DEGRES.directionOpposee());
		assertTrue(Direction.DIRECTION_180_DEGRES == Direction.DIRECTION_0_DEGRE.directionOpposee());
		assertTrue(Direction.DIRECTION_240_DEGRES == Direction.DIRECTION_60_DEGRES.directionOpposee());
		assertTrue(Direction.DIRECTION_300_DEGRES == Direction.DIRECTION_120_DEGRES.directionOpposee());
		// Teste l'utilisation de la fonctionnelle.
		for (Direction directionFctlle : Direction.values()) {
			boolean trouve = false;
			for (Direction direction : Direction.values()) {
				if (direction.getDirectionDegres() == directionFctlle.getDirectionDegresOpposee()) {
					assertTrue(direction.equals(directionFctlle.directionOpposee()));
					trouve = true;
				}
			}
			if (!trouve) {
				assertNull(directionFctlle);
			}
		}
	}

	/**
	 * Tests des coordonnées (ligne et colonne) de la case (du plateau) de
	 * destination.
	 */
	@Test
	public void testsCoordLigColDestination() {
		// Coordonnées (ligne et colonne) de cases (du plateau) sources.
		final int LIG_PAIRE_SOURCE = 24;
		final int COL_PAIRE_SOURCE = 40;
		final int LIG_IMPAIRE_SOURCE = 33;
		final int COL_IMPAIRE_SOURCE = 47;
		final Point COORD_LIG_PAIRE_COL_PAIRE_SOURCE = new Point(LIG_PAIRE_SOURCE, COL_PAIRE_SOURCE);
		final Point COORD_LIG_PAIRE_COL_IMPAIRE_SOURCE = new Point(LIG_PAIRE_SOURCE, COL_IMPAIRE_SOURCE);
		final Point COORD_LIG_IMPAIRE_COL_PAIRE_SOURCE = new Point(LIG_IMPAIRE_SOURCE, COL_PAIRE_SOURCE);
		final Point COORD_LIG_IMPAIRE_COL_IMPAIRE_SOURCE = new Point(LIG_IMPAIRE_SOURCE, COL_IMPAIRE_SOURCE);
		// Ligne de destination.
		for (Point coordLigColSource : Arrays.asList(COORD_LIG_PAIRE_COL_PAIRE_SOURCE,
				COORD_LIG_PAIRE_COL_IMPAIRE_SOURCE, COORD_LIG_IMPAIRE_COL_PAIRE_SOURCE,
				COORD_LIG_IMPAIRE_COL_IMPAIRE_SOURCE)) {
			assertEquals(coordLigColSource.x, Direction.DIRECTION_0_DEGRE.coordLigColDestination(coordLigColSource).x);
			assertEquals(coordLigColSource.x - 1,
					Direction.DIRECTION_60_DEGRES.coordLigColDestination(coordLigColSource).x);
			assertEquals(coordLigColSource.x - 1,
					Direction.DIRECTION_120_DEGRES.coordLigColDestination(coordLigColSource).x);
			assertEquals(coordLigColSource.x,
					Direction.DIRECTION_180_DEGRES.coordLigColDestination(coordLigColSource).x);
			assertEquals(coordLigColSource.x + 1,
					Direction.DIRECTION_240_DEGRES.coordLigColDestination(coordLigColSource).x);
			assertEquals(coordLigColSource.x + 1,
					Direction.DIRECTION_300_DEGRES.coordLigColDestination(coordLigColSource).x);
		}
		// Colonne de destination : source (paire, paire).
		assertEquals(COL_PAIRE_SOURCE + 1,
				Direction.DIRECTION_0_DEGRE.coordLigColDestination(COORD_LIG_PAIRE_COL_PAIRE_SOURCE).y);
		assertEquals(COL_PAIRE_SOURCE + 1,
				Direction.DIRECTION_60_DEGRES.coordLigColDestination(COORD_LIG_PAIRE_COL_PAIRE_SOURCE).y);
		assertEquals(COL_PAIRE_SOURCE,
				Direction.DIRECTION_120_DEGRES.coordLigColDestination(COORD_LIG_PAIRE_COL_PAIRE_SOURCE).y);
		assertEquals(COL_PAIRE_SOURCE - 1,
				Direction.DIRECTION_180_DEGRES.coordLigColDestination(COORD_LIG_PAIRE_COL_PAIRE_SOURCE).y);
		assertEquals(COL_PAIRE_SOURCE,
				Direction.DIRECTION_240_DEGRES.coordLigColDestination(COORD_LIG_PAIRE_COL_PAIRE_SOURCE).y);
		assertEquals(COL_PAIRE_SOURCE + 1,
				Direction.DIRECTION_300_DEGRES.coordLigColDestination(COORD_LIG_PAIRE_COL_PAIRE_SOURCE).y);
		// Colonne de destination : source (paire, impaire).
		assertEquals(COL_IMPAIRE_SOURCE + 1,
				Direction.DIRECTION_0_DEGRE.coordLigColDestination(COORD_LIG_PAIRE_COL_IMPAIRE_SOURCE).y);
		assertEquals(COL_IMPAIRE_SOURCE + 1,
				Direction.DIRECTION_60_DEGRES.coordLigColDestination(COORD_LIG_PAIRE_COL_IMPAIRE_SOURCE).y);
		assertEquals(COL_IMPAIRE_SOURCE,
				Direction.DIRECTION_120_DEGRES.coordLigColDestination(COORD_LIG_PAIRE_COL_IMPAIRE_SOURCE).y);
		assertEquals(COL_IMPAIRE_SOURCE - 1,
				Direction.DIRECTION_180_DEGRES.coordLigColDestination(COORD_LIG_PAIRE_COL_IMPAIRE_SOURCE).y);
		assertEquals(COL_IMPAIRE_SOURCE,
				Direction.DIRECTION_240_DEGRES.coordLigColDestination(COORD_LIG_PAIRE_COL_IMPAIRE_SOURCE).y);
		assertEquals(COL_IMPAIRE_SOURCE + 1,
				Direction.DIRECTION_300_DEGRES.coordLigColDestination(COORD_LIG_PAIRE_COL_IMPAIRE_SOURCE).y);
		// Colonne de destination : source (impaire, paire).
		assertEquals(COL_PAIRE_SOURCE + 1,
				Direction.DIRECTION_0_DEGRE.coordLigColDestination(COORD_LIG_IMPAIRE_COL_PAIRE_SOURCE).y);
		assertEquals(COL_PAIRE_SOURCE,
				Direction.DIRECTION_60_DEGRES.coordLigColDestination(COORD_LIG_IMPAIRE_COL_PAIRE_SOURCE).y);
		assertEquals(COL_PAIRE_SOURCE - 1,
				Direction.DIRECTION_120_DEGRES.coordLigColDestination(COORD_LIG_IMPAIRE_COL_PAIRE_SOURCE).y);
		assertEquals(COL_PAIRE_SOURCE - 1,
				Direction.DIRECTION_180_DEGRES.coordLigColDestination(COORD_LIG_IMPAIRE_COL_PAIRE_SOURCE).y);
		assertEquals(COL_PAIRE_SOURCE - 1,
				Direction.DIRECTION_240_DEGRES.coordLigColDestination(COORD_LIG_IMPAIRE_COL_PAIRE_SOURCE).y);
		assertEquals(COL_PAIRE_SOURCE,
				Direction.DIRECTION_300_DEGRES.coordLigColDestination(COORD_LIG_IMPAIRE_COL_PAIRE_SOURCE).y);
		// Colonne de destination : source (impaire, impaire).
		assertEquals(COL_IMPAIRE_SOURCE + 1,
				Direction.DIRECTION_0_DEGRE.coordLigColDestination(COORD_LIG_IMPAIRE_COL_IMPAIRE_SOURCE).y);
		assertEquals(COL_IMPAIRE_SOURCE,
				Direction.DIRECTION_60_DEGRES.coordLigColDestination(COORD_LIG_IMPAIRE_COL_IMPAIRE_SOURCE).y);
		assertEquals(COL_IMPAIRE_SOURCE - 1,
				Direction.DIRECTION_120_DEGRES.coordLigColDestination(COORD_LIG_IMPAIRE_COL_IMPAIRE_SOURCE).y);
		assertEquals(COL_IMPAIRE_SOURCE - 1,
				Direction.DIRECTION_180_DEGRES.coordLigColDestination(COORD_LIG_IMPAIRE_COL_IMPAIRE_SOURCE).y);
		assertEquals(COL_IMPAIRE_SOURCE - 1,
				Direction.DIRECTION_240_DEGRES.coordLigColDestination(COORD_LIG_IMPAIRE_COL_IMPAIRE_SOURCE).y);
		assertEquals(COL_IMPAIRE_SOURCE,
				Direction.DIRECTION_300_DEGRES.coordLigColDestination(COORD_LIG_IMPAIRE_COL_IMPAIRE_SOURCE).y);
	}

	/**
	 * Tests de la direction de la case source vers la case destination lorsque
	 * les coordonnées (ligne et colonne) correspondent à deux cases (du
	 * plateau) adjacentes.
	 */
	@Test
	public void testsDirection2CasesAdjacentes() {
		Point coordLigColTest = new Point(1, 1);
		// Au même endroit.
		assertNull(Direction.direction2CasesAdjacentes(coordLigColTest, coordLigColTest));
		// Autour d'une case (dans toutes les directions) en testant la
		// symétrie.
		for (Point coordLigCol : Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(2, 1),
				new Point(1, 2), new Point(0, 1))) {
			Direction directionDeTest = Direction.direction2CasesAdjacentes(coordLigColTest, coordLigCol);
			Direction directionVersTest = Direction.direction2CasesAdjacentes(coordLigCol, coordLigColTest);
			assertNotNull(directionDeTest);
			assertNotNull(directionVersTest);
			assertTrue(directionDeTest.directionOpposee().equals(directionVersTest));
		}
		// Autour d'une case, pour chaque direction.
		assertTrue(Direction.direction2CasesAdjacentes(coordLigColTest, new Point(0, 0))
				.equals(Direction.DIRECTION_120_DEGRES));
		assertTrue(Direction.direction2CasesAdjacentes(coordLigColTest, new Point(1, 0))
				.equals(Direction.DIRECTION_180_DEGRES));
		assertTrue(Direction.direction2CasesAdjacentes(coordLigColTest, new Point(2, 0))
				.equals(Direction.DIRECTION_240_DEGRES));
		assertTrue(Direction.direction2CasesAdjacentes(coordLigColTest, new Point(2, 1))
				.equals(Direction.DIRECTION_300_DEGRES));
		assertTrue(Direction.direction2CasesAdjacentes(coordLigColTest, new Point(1, 2))
				.equals(Direction.DIRECTION_0_DEGRE));
		assertTrue(Direction.direction2CasesAdjacentes(coordLigColTest, new Point(0, 1))
				.equals(Direction.DIRECTION_60_DEGRES));
		// Au loin.
		assertNull(Direction.direction2CasesAdjacentes(coordLigColTest, new Point(-42, +42)));
	}

}
