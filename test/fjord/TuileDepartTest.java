/**
 * Tests des tuiles de départ.
 */
package fjord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Point;
import java.util.Arrays;

import org.junit.Test;


/**
 * Tests des tuiles de départ.
 * 
 * @author Olivier
 */
public class TuileDepartTest {

	/**
	 * Tests des étiquettes des tuiles de départ.
	 */
	@Test
	public void testsEtiquettes() {
		// Nombre de tuiles de départ.
		assertEquals(3, TuileDepart.values().length);
		// Étiquettes des tuiles de départ.
		assertTrue(Arrays.asList(TuileDepart.values()).contains(TuileDepart.DEPART_1));
		assertTrue(Arrays.asList(TuileDepart.values()).contains(TuileDepart.DEPART_2));
		assertTrue(Arrays.asList(TuileDepart.values()).contains(TuileDepart.DEPART_3));
	}

	/**
	 * Tests de cohérence entre la tuile et la tuile orientée.
	 */
	@Test
	public void testsCoherenceTuileTuileOrientee() {
		for (TuileDepart tuileDepart : TuileDepart.values()) {
			Tuile tuile = tuileDepart.getTuile();
			assertNotNull(tuile);
			TuileOrientee tuileOrientee = tuileDepart.getTuileOrientee();
			assertNotNull(tuileOrientee);
			assertTrue(tuileOrientee.getTuileBase().equals(tuile));
		}
	}

	/**
	 * Tests des nombres d'exemplaires disponibles au départ.
	 */
	@Test
	public void testsNbExemplaires() {
		int totalNbExemplaires = 0;
		for (TuileDepart tuileDepart : TuileDepart.values()) {
			assertTrue(tuileDepart.getNbExemplaires() >= 1);
			totalNbExemplaires += tuileDepart.getNbExemplaires();
		}
		assertEquals(TuileDepart.TOTAL_NB_EXEMPLAIRES, totalNbExemplaires);
	}

	/**
	 * Tests d'unicité des coordonnées.
	 */
	@Test
	public void testsUniciteCoordLigCol() {
		for (int ind1reTuileDepart = 0; ind1reTuileDepart < TuileDepart.values().length; ++ind1reTuileDepart) {
			for (int ind2deTuileDepart = ind1reTuileDepart + 1; ind2deTuileDepart < TuileDepart
					.values().length; ++ind2deTuileDepart) {
				assertFalse(TuileDepart.values()[ind1reTuileDepart].getCoordLigCol()
						.equals(TuileDepart.values()[ind2deTuileDepart].getCoordLigCol()));
			}
		}
	}

	/**
	 * Tests de correspondance entre tuiles de départ côte à côte.
	 */
	@Test
	public void testsCorrespondanceCoordLigCol() {
		// N. B. : ce test est volontairement spécifique pour éviter une
		// réutilisation par les étudiants du code qu'ils doivent écrire.
		assertTrue(TuileDepart.DEPART_1.getTuileOrientee().equals(TuileOrientee.AAAAAA)
				&& TuileDepart.DEPART_1.getCoordLigCol().equals(new Point(1, 0)));
		assertTrue(TuileDepart.DEPART_2.getTuileOrientee().equals(TuileOrientee.CAAAAA)
				&& TuileDepart.DEPART_2.getCoordLigCol().equals(new Point(1, 1)));
		assertTrue(TuileDepart.DEPART_3.getTuileOrientee().equals(TuileOrientee.ABAACC)
				&& TuileDepart.DEPART_3.getCoordLigCol().equals(new Point(0, 0)));
		// Correspondances.
		assertTrue(TuileDepart.DEPART_1.getTuileOrientee().toString().charAt(4) == Terrain.TERRES_ARABLES.getCode()
				&& TuileDepart.DEPART_2.getTuileOrientee().toString().charAt(2) == Terrain.TERRES_ARABLES.getCode());
		assertTrue(TuileDepart.DEPART_1.getTuileOrientee().toString().charAt(5) == Terrain.TERRES_ARABLES.getCode()
				&& TuileDepart.DEPART_2.getTuileOrientee().toString().charAt(1) == Terrain.TERRES_ARABLES.getCode());
		assertTrue(TuileDepart.DEPART_1.getTuileOrientee().toString().charAt(0) == Terrain.TERRES_ARABLES.getCode()
				&& TuileDepart.DEPART_3.getTuileOrientee().toString().charAt(2) == Terrain.TERRES_ARABLES.getCode());
		assertTrue(TuileDepart.DEPART_1.getTuileOrientee().toString().charAt(5) == Terrain.TERRES_ARABLES.getCode()
				&& TuileDepart.DEPART_3.getTuileOrientee().toString().charAt(3) == Terrain.TERRES_ARABLES.getCode());
		assertTrue(TuileDepart.DEPART_2.getTuileOrientee().toString().charAt(0) == Terrain.MONTAGNE.getCode()
				&& TuileDepart.DEPART_3.getTuileOrientee().toString().charAt(4) == Terrain.MONTAGNE.getCode());
		assertTrue(TuileDepart.DEPART_2.getTuileOrientee().toString().charAt(1) == Terrain.TERRES_ARABLES.getCode()
				&& TuileDepart.DEPART_3.getTuileOrientee().toString().charAt(3) == Terrain.TERRES_ARABLES.getCode());
	}

}
