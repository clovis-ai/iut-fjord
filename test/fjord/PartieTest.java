/**
 * Tests des parties.
 */
package fjord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;


/**
 * Tests des parties.
 * 
 * @author Olivier
 */
public class PartieTest {

	/**
	 * Tests des parties.
	 */
	@Test
	public void tests() {
		// Constantes.
		assertTrue(Partie.NB_JOUEURS >= 1);
		assertTrue(Partie.NB_MANCHES >= 1);
		assertEquals(Couleur.values().length, Partie.NB_JOUEURS);
		Partie partie = new Partie();
		// Joueurs.
		assertEquals(Partie.NB_JOUEURS, partie.getJoueurs().size());
		// Joueur suivant du joueur actif.
		assertTrue(partie.getJoueurs().get(1) == partie.joueurSuivant(partie.getJoueurs().get(0)));
		assertTrue(partie.getJoueurs().get(0) == partie.joueurSuivant(partie.getJoueurs().get(1)));
		assertFalse(partie.getJoueurs().get(0) == partie.joueurSuivant(partie.getJoueurs().get(0)));
		assertFalse(partie.getJoueurs().get(1) == partie.joueurSuivant(partie.getJoueurs().get(1)));
		// Erreur car pas de joueur actif.
		try {
			partie.joueurSuivant(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
	}

	/**
	 * Tests du joueur suivant.
	 */
	@Test
	public void testsJoueurSuivant() {
		Partie partie = new Partie();
		// Erreur car pas de joueur.
		try {
			partie.joueurSuivant(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		//
		for (int indJoueurs = 0; indJoueurs < partie.getJoueurs().size() - 1; ++indJoueurs) {
			assertTrue(partie.joueurSuivant(partie.getJoueurs().get(indJoueurs))
					.equals(partie.getJoueurs().get(indJoueurs + 1)));
		}
		assertTrue(partie.joueurSuivant(partie.getJoueurs().get(partie.getJoueurs().size() - 1))
				.equals(partie.getJoueurs().get(0)));
	}

	/**
	 * Tests du nombre de points de victoire cumulés des joueurs et du joueur
	 * gagnant de la partie.
	 */
	@Test
	public void testsPointsVictoireCumulesJoueurGagnant() {
		Partie partie = new Partie();
		Joueur premierJoueur = partie.getJoueurs().get(0);
		Joueur secondJoueur = partie.getJoueurs().get(1);
		Map<Joueur, Integer> pointsVictoireCumules;
		Manche[] manches = new Manche[Partie.NB_MANCHES];
		// Aucune manche.
		pointsVictoireCumules = partie.pointsVictoireCumules();
		pointsVictoireCumules(partie, pointsVictoireCumules);
		assertTrue(pointsVictoireCumules.isEmpty());
		assertNull(partie.getJoueurGagnant(pointsVictoireCumules));
		// Manche 0 - 0.
		Manche manche00 = new Manche(partie, premierJoueur);
		manche00.toursJeuFinScores();
		// Manche 0 - 1.
		Manche manche01 = new Manche(partie, premierJoueur);
		manche01.getPlateau().poserTuile(new Point(1, 1), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche01.toursJeuFinScores();
		// Manche 0 - 2.
		Manche manche02 = new Manche(partie, premierJoueur);
		manche02.getPlateau().poserTuile(new Point(1, 1), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche02.getPlateau().poserTuile(new Point(2, 2), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche02.toursJeuFinScores();
		// Manche 0 - 3.
		Manche manche03 = new Manche(partie, premierJoueur);
		manche03.getPlateau().poserTuile(new Point(1, 1), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche03.getPlateau().poserTuile(new Point(2, 2), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche03.getPlateau().poserTuile(new Point(3, 3), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche03.toursJeuFinScores();
		// Manche 1 - 0.
		Manche manche10 = new Manche(partie, premierJoueur);
		manche10.getPlateau().poserTuile(new Point(1, 1), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche10.toursJeuFinScores();
		// Manche 1 - 1.
		Manche manche11 = new Manche(partie, premierJoueur);
		manche11.getPlateau().poserTuile(new Point(1, 1), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche11.getPlateau().poserTuile(new Point(2, 2), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche11.toursJeuFinScores();
		// Manche 1 - 2.
		Manche manche12 = new Manche(partie, premierJoueur);
		manche12.getPlateau().poserTuile(new Point(1, 1), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche12.getPlateau().poserTuile(new Point(2, 2), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche12.getPlateau().poserTuile(new Point(3, 3), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche12.toursJeuFinScores();
		// Manche 2 - 0.
		Manche manche20 = new Manche(partie, premierJoueur);
		manche20.getPlateau().poserTuile(new Point(1, 1), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche20.getPlateau().poserTuile(new Point(2, 2), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche20.toursJeuFinScores();
		// Manche 2 - 1.
		Manche manche21 = new Manche(partie, premierJoueur);
		manche21.getPlateau().poserTuile(new Point(1, 1), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche21.getPlateau().poserTuile(new Point(2, 2), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche21.getPlateau().poserTuile(new Point(3, 3), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche21.toursJeuFinScores();
		// Manche 3 - 0.
		Manche manche30 = new Manche(partie, premierJoueur);
		manche30.getPlateau().poserTuile(new Point(1, 1), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche30.getPlateau().poserTuile(new Point(2, 2), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche30.getPlateau().poserTuile(new Point(3, 3), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche30.toursJeuFinScores();
		// Partie : 1 - 1 ; 3 - 0 ; 1 - 0.
		manches[0] = manche11;
		manches[1] = manche30;
		manches[2] = manche10;
		partie.setManches(manches);
		pointsVictoireCumules = partie.pointsVictoireCumules();
		pointsVictoireCumules(partie, pointsVictoireCumules);
		assertEquals(2, pointsVictoireCumules.size());
		assertEquals(5, (int) pointsVictoireCumules.get(premierJoueur));
		assertEquals(1, (int) pointsVictoireCumules.get(secondJoueur));
		assertTrue(partie.getJoueurGagnant(pointsVictoireCumules).equals(premierJoueur));
		// Partie : 0 - 0 ; 0 - 0 ; 0 - 0.
		manches[0] = manche00;
		manches[1] = manche00;
		manches[2] = manche00;
		partie.setManches(manches);
		pointsVictoireCumules = partie.pointsVictoireCumules();
		pointsVictoireCumules(partie, pointsVictoireCumules);
		assertTrue(pointsVictoireCumules.isEmpty());
		assertEquals(0, pointsVictoireCumules.size());
		assertNull(partie.getJoueurGagnant(pointsVictoireCumules));
		// Partie : 0 - 0 ; 1 - 0 ; 0 - 0.
		manches[0] = manche00;
		manches[1] = manche10;
		manches[2] = manche00;
		partie.setManches(manches);
		pointsVictoireCumules = partie.pointsVictoireCumules();
		pointsVictoireCumules(partie, pointsVictoireCumules);
		assertEquals(1, pointsVictoireCumules.size());
		assertEquals(1, (int) pointsVictoireCumules.get(premierJoueur));
		assertTrue(partie.getJoueurGagnant(pointsVictoireCumules).equals(premierJoueur));
		// Partie : 0 - 0 ; 1 - 0 ; 0 - 2.
		manches[0] = manche00;
		manches[1] = manche10;
		manches[2] = manche02;
		partie.setManches(manches);
		pointsVictoireCumules = partie.pointsVictoireCumules();
		pointsVictoireCumules(partie, pointsVictoireCumules);
		assertEquals(2, pointsVictoireCumules.size());
		assertEquals(1, (int) pointsVictoireCumules.get(premierJoueur));
		assertEquals(2, (int) pointsVictoireCumules.get(secondJoueur));
		assertTrue(partie.getJoueurGagnant(pointsVictoireCumules).equals(secondJoueur));
		// Partie : 0 - 3 ; 1 - 0 ; 1 - 0.
		manches[0] = manche03;
		manches[1] = manche10;
		manches[2] = manche10;
		partie.setManches(manches);
		pointsVictoireCumules = partie.pointsVictoireCumules();
		pointsVictoireCumules(partie, pointsVictoireCumules);
		assertEquals(2, pointsVictoireCumules.size());
		assertEquals(2, (int) pointsVictoireCumules.get(premierJoueur));
		assertEquals(3, (int) pointsVictoireCumules.get(secondJoueur));
		assertTrue(partie.getJoueurGagnant(pointsVictoireCumules).equals(secondJoueur));
		// Partie : 1 - 0 ; 0 - 3 ; 1 - 0.
		manches[0] = manche10;
		manches[1] = manche03;
		manches[2] = manche10;
		partie.setManches(manches);
		pointsVictoireCumules = partie.pointsVictoireCumules();
		pointsVictoireCumules(partie, pointsVictoireCumules);
		assertEquals(2, pointsVictoireCumules.size());
		assertEquals(2, (int) pointsVictoireCumules.get(premierJoueur));
		assertEquals(3, (int) pointsVictoireCumules.get(secondJoueur));
		assertTrue(partie.getJoueurGagnant(pointsVictoireCumules).equals(secondJoueur));
		// Partie : 1 - 0 ; 1 - 0 ; 0 - 3.
		manches[0] = manche10;
		manches[1] = manche10;
		manches[2] = manche03;
		partie.setManches(manches);
		pointsVictoireCumules = partie.pointsVictoireCumules();
		pointsVictoireCumules(partie, pointsVictoireCumules);
		assertEquals(2, pointsVictoireCumules.size());
		assertEquals(2, (int) pointsVictoireCumules.get(premierJoueur));
		assertEquals(3, (int) pointsVictoireCumules.get(secondJoueur));
		assertTrue(partie.getJoueurGagnant(pointsVictoireCumules).equals(secondJoueur));
		// Partie : 2 - 0 ; 0 - 1 ; 0 - 1.
		manches[0] = manche20;
		manches[1] = manche01;
		manches[2] = manche01;
		partie.setManches(manches);
		pointsVictoireCumules = partie.pointsVictoireCumules();
		pointsVictoireCumules(partie, pointsVictoireCumules);
		assertEquals(2, pointsVictoireCumules.size());
		assertEquals(2, (int) pointsVictoireCumules.get(premierJoueur));
		assertEquals(2, (int) pointsVictoireCumules.get(secondJoueur));
		assertTrue(partie.getJoueurGagnant(pointsVictoireCumules).equals(secondJoueur));
		// Partie : 0 - 1 ; 2 - 0 ; 0 - 1.
		manches[0] = manche01;
		manches[1] = manche20;
		manches[2] = manche01;
		partie.setManches(manches);
		pointsVictoireCumules = partie.pointsVictoireCumules();
		pointsVictoireCumules(partie, pointsVictoireCumules);
		assertEquals(2, pointsVictoireCumules.size());
		assertEquals(2, (int) pointsVictoireCumules.get(premierJoueur));
		assertEquals(2, (int) pointsVictoireCumules.get(secondJoueur));
		assertTrue(partie.getJoueurGagnant(pointsVictoireCumules).equals(secondJoueur));
		// Partie : 0 - 1 ; 0 - 1 ; 2 - 0.
		manches[0] = manche01;
		manches[1] = manche01;
		manches[2] = manche20;
		partie.setManches(manches);
		pointsVictoireCumules = partie.pointsVictoireCumules();
		pointsVictoireCumules(partie, pointsVictoireCumules);
		assertEquals(2, pointsVictoireCumules.size());
		assertEquals(2, (int) pointsVictoireCumules.get(premierJoueur));
		assertEquals(2, (int) pointsVictoireCumules.get(secondJoueur));
		assertTrue(partie.getJoueurGagnant(pointsVictoireCumules).equals(secondJoueur));
		// Partie : 1 - 1 ; 0 - 0 ; 1 - 1.
		manches[0] = manche11;
		manches[1] = manche00;
		manches[2] = manche11;
		partie.setManches(manches);
		pointsVictoireCumules = partie.pointsVictoireCumules();
		pointsVictoireCumules(partie, pointsVictoireCumules);
		assertEquals(2, pointsVictoireCumules.size());
		assertEquals(2, (int) pointsVictoireCumules.get(premierJoueur));
		assertEquals(2, (int) pointsVictoireCumules.get(secondJoueur));
		assertNull(partie.getJoueurGagnant(pointsVictoireCumules));
		// Partie : 1 - 1 ; 1 - 2 ; 2 - 1.
		manches[0] = manche11;
		manches[1] = manche12;
		manches[2] = manche21;
		partie.setManches(manches);
		pointsVictoireCumules = partie.pointsVictoireCumules();
		pointsVictoireCumules(partie, pointsVictoireCumules);
		assertEquals(2, pointsVictoireCumules.size());
		assertEquals(4, (int) pointsVictoireCumules.get(premierJoueur));
		assertEquals(4, (int) pointsVictoireCumules.get(secondJoueur));
		assertNull(partie.getJoueurGagnant(pointsVictoireCumules));
		// TODO : fctlle
	}

	/**
	 * Tests de l'utilisation de la fonctionnelle pour le calcul du cumul des
	 * points de victoire en fin de la partie.
	 * 
	 * @param partie
	 *            Partie.
	 * @param pointsVictoireCumules
	 *            Cumul des points de victoire en fin de la partie (calculés par
	 *            la fonctionnelle).
	 */
	private void pointsVictoireCumules(final Partie partie, final Map<Joueur, Integer> pointsVictoireCumules) {
		Map<Joueur, Integer> ptsVictoireCumul = new HashMap<Joueur, Integer>();
		if (partie.getManches() != null) {
			for (Manche manche : partie.getManches()) {
				for (Entry<Joueur, Integer> joueurNbPointsVictoire : manche.getPointsVictoire().entrySet()) {
					ptsVictoireCumul.put(joueurNbPointsVictoire.getKey(),
							ptsVictoireCumul.getOrDefault(joueurNbPointsVictoire.getKey(), 0)
									+ joueurNbPointsVictoire.getValue());
				}
			}
		}
		assertEquals(ptsVictoireCumul.size(), pointsVictoireCumules.size());
		for (Entry<Joueur, Integer> joueurPointsVictoireCumulesFctlle : pointsVictoireCumules.entrySet()) {
			assertNotNull(ptsVictoireCumul.get(joueurPointsVictoireCumulesFctlle.getKey()));
			assertTrue(ptsVictoireCumul.get(joueurPointsVictoireCumulesFctlle.getKey())
					.equals(joueurPointsVictoireCumulesFctlle.getValue()));
		}
	}

	/**
	 * Tests du premier joueur d'une manche.
	 */
	@Test
	public void testsPremierJoueurManche() {
		Partie partie = new Partie();
		Joueur premierJoueur = partie.getJoueurs().get(0);
		Joueur secondJoueur = partie.getJoueurs().get(1);
		Manche[] manches = new Manche[Partie.NB_MANCHES];
		// Manche 0 - 0.
		Manche manche00 = new Manche(partie, premierJoueur);
		manche00.toursJeuFinScores();
		// Manche 0 - 1.
		Manche manche01 = new Manche(partie, premierJoueur);
		manche01.getPlateau().poserTuile(new Point(1, 1), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche01.toursJeuFinScores();
		// Manche 1 - 0.
		Manche manche10 = new Manche(partie, premierJoueur);
		manche10.getPlateau().poserTuile(new Point(1, 1), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche10.toursJeuFinScores();
		// Manche 1 - 1.
		Manche manche11 = new Manche(partie, premierJoueur);
		manche11.getPlateau().poserTuile(new Point(1, 1), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche11.getPlateau().poserTuile(new Point(2, 2), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche11.toursJeuFinScores();
		// Partie : 0 - 1 ; 0 - 0 ; ...
		manches[0] = manche01;
		manches[1] = manche00;
		manches[2] = manche11;
		partie.setManches(manches);
		assertTrue(partie.getPremierJoueurManche(1).equals(premierJoueur));
		// Partie : 0 - 0 ; 1 - 0 ; ...
		manches[0] = manche00;
		manches[1] = manche10;
		manches[2] = manche11;
		partie.setManches(manches);
		assertTrue(partie.getPremierJoueurManche(2).equals(secondJoueur));
		// Partie : 1 - 1 ; 0 - 1 ; ...
		manches[0] = manche11;
		manches[1] = manche01;
		manches[2] = manche11;
		partie.setManches(manches);
		assertTrue(partie.getPremierJoueurManche(2).equals(premierJoueur));
	}

	/**
	 * Tests du premier joueur d'une manche lors d'un tirage au sort.
	 */
	@Test
	public void testsPremierJoueurMancheTirageAuSort() {
		// Rien à tester !
	}

	/**
	 * Tests des joueurs qui ont la valeur (entier naturel) maximale.
	 */
	@Test
	public void testsJoueursValMax() {
		// Variables.
		List<Joueur> joueursValMax = new ArrayList<Joueur>();
		Map<Joueur, Integer> valeurs = new HashMap<Joueur, Integer>();
		// Tous les joueurs ont la même couleur !
		Joueur joueur1 = new Joueur(Couleur.FONCE);
		Joueur joueur2 = new Joueur(Couleur.FONCE);
		Joueur joueur3 = new Joueur(Couleur.FONCE);
		Joueur joueur4 = new Joueur(Couleur.FONCE);
		Joueur joueur5 = new Joueur(Couleur.FONCE);
		Joueur joueur6 = new Joueur(Couleur.FONCE);
		// Aucune valeur.
		assertTrue(Partie.joueursValMax(valeurs).isEmpty());
		// Une seule valeur.
		valeurs.clear();
		valeurs.put(joueur1, 33);
		joueursValMax = Partie.joueursValMax(valeurs);
		assertEquals(1, joueursValMax.size());
		assertTrue(joueur1 == joueursValMax.get(0));
		// Deux valeurs identiques.
		valeurs.clear();
		valeurs.put(joueur1, 24);
		valeurs.put(joueur2, 24);
		joueursValMax = Partie.joueursValMax(valeurs);
		assertEquals(2, joueursValMax.size());
		assertTrue(joueursValMax.contains(joueur1));
		assertTrue(joueursValMax.contains(joueur2));
		// Deux valeurs différentes, le maximum en dernier.
		valeurs.clear();
		valeurs.put(joueur1, 40);
		valeurs.put(joueur2, 64);
		joueursValMax = Partie.joueursValMax(valeurs);
		assertEquals(1, joueursValMax.size());
		assertTrue(joueur2 == joueursValMax.get(0));
		// Deux valeurs différentes, le maximum en premier.
		valeurs.clear();
		valeurs.put(joueur1, 47);
		valeurs.put(joueur2, 16);
		joueursValMax = Partie.joueursValMax(valeurs);
		assertEquals(1, joueursValMax.size());
		assertTrue(joueur1 == joueursValMax.get(0));
		// Plusieurs valeurs toutes différentes.
		valeurs.clear();
		valeurs.put(joueur1, 23);
		valeurs.put(joueur2, 16);
		valeurs.put(joueur3, 86);
		valeurs.put(joueur4, 17);
		valeurs.put(joueur5, 87);
		valeurs.put(joueur6, 19);
		joueursValMax = Partie.joueursValMax(valeurs);
		assertEquals(1, joueursValMax.size());
		assertTrue(joueur5 == joueursValMax.get(0));
		// Plusieurs valeurs répétées (dont le maximum).
		valeurs.clear();
		valeurs.put(joueur1, 19);
		valeurs.put(joueur2, 23);
		valeurs.put(joueur3, 87);
		valeurs.put(joueur4, 19);
		valeurs.put(joueur5, 87);
		valeurs.put(joueur6, 23);
		joueursValMax = Partie.joueursValMax(valeurs);
		assertEquals(2, joueursValMax.size());
		assertTrue(joueursValMax.contains(joueur3));
		assertTrue(joueursValMax.contains(joueur5));
	}

}
