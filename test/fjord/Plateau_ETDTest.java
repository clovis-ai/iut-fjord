/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fjord;

import fjord.Plateau_ETD.Relation;
import java.awt.Point;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ivan
 */
public class Plateau_ETDTest {
    
    public Plateau_ETDTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     * Test of exportGvColonisation method, of class Plateau_ETD.
     */
    @Test
    public void testExportGvColonisation() {
        System.out.println("-- Relation --");
        
        Relation<Integer> r1 = new Relation<>(1, 2);
        Relation<Integer> r2 = new Relation<>(2, 1);
        Relation<Integer> r3 = new Relation<>(0, 1);
        Relation<Integer> r4 = new Relation<>(2, 2);
        Relation<Integer> r5 = new Relation<>(1, 2);
        
        System.out.println("equals");
        assertTrue(r1.equals(r1));
        assertTrue(r1.equals(r2));
        assertTrue(r2.equals(r1));
        assertFalse(r1.equals(r3));
        assertFalse(r1.equals(r4));
        
        assertEquals(r1.hashCode(), r2.hashCode());
        
        System.out.println("oppose");
        assertEquals(r1, r1.getOppose());
        
        Point a = new Point(1, 1);
        Point b = new Point(1, 1);
        assertTrue(a.equals(b));
        assertTrue(Objects.equals(a, b));
        
        Set<Relation<Integer>> s = new HashSet<>();
        s.add(r1);
        s.add(r5);
        s.add(r2);
        
        assertEquals(1, s.size());
    }
    
}
