/**
 * Tests du plateau.
 */
package fjord;

import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static org.junit.Assert.*;


/**
 * Tests du plateau.
 *
 * @author Olivier
 */
public class PlateauTest {

    /**
     * Tests du plateau.
     */
    @Test
    public void tests() {
    }

    /**
     * Tests de la pose d'une tuile dans une nouvelle case du plateau.
     */
    @Test
    public void testsPoserTuile() {
        Joueur joueur = new Joueur(Couleur.FONCE);
        // Création plateau.
        Plateau plateau = new Plateau_ETD();
        assertEquals(0, plateau.getCases().size());
        // Case inexistante.
        assertNull(plateau.getCase(new Point(0, 0)));
        // Case inoccupée.
        plateau.poserTuile(new Point(0, 0), TuileOrientee.AAAAAA, null, null);
        assertEquals(1, plateau.getCases().size());
        assertNotNull(plateau.getCase(new Point(0, 0)));
        assertNull(plateau.getCase(new Point(1, 1)));
        // Case occupée.
        plateau.poserTuile(new Point(1, 1), TuileOrientee.AAAAAB, joueur, Pion.HUTTE);
        assertEquals(2, plateau.getCases().size());
        assertNotNull(plateau.getCase(new Point(0, 0)));
        assertNotNull(plateau.getCase(new Point(1, 1)));
        assertNull(plateau.getCase(new Point(2, 2)));
        // Erreur car pas de coordonnées pour accéder à une case du plateau.
        try {
            plateau.getCase(null);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            assertFalse(true);
        }
        // Erreur car pas de coordonnée pour poser une case.
        try {
            plateau.poserTuile(null, TuileOrientee.AAAAAC, null, null);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            assertFalse(true);
        }
        // Erreur car pas de tuile orientée pour poser une case.
        try {
            plateau.poserTuile(new Point(3, 3), null, null, null);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            assertFalse(true);
        }
        // Erreur car pion sans joueur pour poser une case.
        try {
            plateau.poserTuile(new Point(4, 4), TuileOrientee.AAAABA, null, Pion.CHAMP);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            assertFalse(true);
        }
        // Erreur car joueur sans pion pour poser une case.
        try {
            plateau.poserTuile(new Point(5, 5), TuileOrientee.AAAABB, joueur, null);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            assertFalse(true);
        }
        // Erreur car case existante pour poser une case.
        try {
            plateau.poserTuile(new Point(0, 0), TuileOrientee.AAAACA, null, null);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            assertFalse(true);
        }
    }

    /**
     * Tests d'installation d'un champ dans une ancienne case du plateau.
     */
    @Test
    public void testsInstallerChamp() {
        Plateau plateau = new Plateau_ETD();
        // Erreur car pas de coordonnée pour installer un champ.
        try {
            plateau.installerChamp(null, new Joueur(Couleur.FONCE));
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            assertFalse(true);
        }
        // Erreur car pas de joueur pour installer un champ.
        try {
            plateau.installerChamp(new Point(0, 0), null);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            assertFalse(true);
        }
        // Installations.
        Joueur joueur = new Joueur(Couleur.FONCE);
        Point coordLigCol;
        assertEquals(0, plateau.nbCasesOccupees(joueur, Pion.CHAMP));
        // 1re case.
        coordLigCol = new Point(1, 1);
        plateau.poserTuile(coordLigCol, TuileOrientee.AAAAAA, null, null);
        assertEquals(0, plateau.nbCasesOccupees(joueur, Pion.CHAMP));
        plateau.installerChamp(coordLigCol, joueur);
        assertEquals(1, plateau.nbCasesOccupees(joueur, Pion.CHAMP));
        plateau.installerChamp(coordLigCol, joueur);
        assertEquals(1, plateau.nbCasesOccupees(joueur, Pion.CHAMP));
        // 2e case.
        coordLigCol = new Point(2, 2);
        plateau.poserTuile(coordLigCol, TuileOrientee.AAAAAA, joueur, Pion.HUTTE);
        assertEquals(1, plateau.nbCasesOccupees(joueur, Pion.CHAMP));
        plateau.installerChamp(coordLigCol, joueur);
        assertEquals(2, plateau.nbCasesOccupees(joueur, Pion.CHAMP));
        // 3e case.
        coordLigCol = new Point(3, 3);
        plateau.poserTuile(coordLigCol, TuileOrientee.AAAAAA, joueur, Pion.CHAMP);
        assertEquals(3, plateau.nbCasesOccupees(joueur, Pion.CHAMP));
        plateau.installerChamp(coordLigCol, joueur);
        assertEquals(3, plateau.nbCasesOccupees(joueur, Pion.CHAMP));
    }

    /**
     * Tests du nombre de cases occupées par un joueur et un pion.
     */
    @Test
    public void testsNbCasesOccupees() {
        Plateau plateau = new Plateau_ETD();
        Joueur joueurFonce = new Joueur(Couleur.FONCE);
        Joueur joueurClair = new Joueur(Couleur.CLAIR);
        TuileOrientee tuileOrientee = TuileOrientee.AAAAAA;
        assertEquals(0, plateau.nbCasesOccupees(joueurFonce, Pion.HUTTE));
        assertEquals(0, plateau.nbCasesOccupees(joueurFonce, Pion.CHAMP));
        assertEquals(0, plateau.nbCasesOccupees(joueurClair, Pion.HUTTE));
        assertEquals(0, plateau.nbCasesOccupees(joueurClair, Pion.CHAMP));
        plateau.getCases().put(new Point(0, 0), new Case(tuileOrientee, null, null));
        assertEquals(0, plateau.nbCasesOccupees(joueurFonce, Pion.HUTTE));
        assertEquals(0, plateau.nbCasesOccupees(joueurFonce, Pion.CHAMP));
        assertEquals(0, plateau.nbCasesOccupees(joueurClair, Pion.HUTTE));
        assertEquals(0, plateau.nbCasesOccupees(joueurClair, Pion.CHAMP));
        plateau.getCases().put(new Point(0, 1), new Case(tuileOrientee, joueurFonce, Pion.HUTTE));
        assertEquals(1, plateau.nbCasesOccupees(joueurFonce, Pion.HUTTE));
        assertEquals(0, plateau.nbCasesOccupees(joueurFonce, Pion.CHAMP));
        assertEquals(0, plateau.nbCasesOccupees(joueurClair, Pion.HUTTE));
        assertEquals(0, plateau.nbCasesOccupees(joueurClair, Pion.CHAMP));
        plateau.getCases().put(new Point(0, 2), new Case(tuileOrientee, joueurFonce, Pion.HUTTE));
        assertEquals(2, plateau.nbCasesOccupees(joueurFonce, Pion.HUTTE));
        assertEquals(0, plateau.nbCasesOccupees(joueurFonce, Pion.CHAMP));
        assertEquals(0, plateau.nbCasesOccupees(joueurClair, Pion.HUTTE));
        assertEquals(0, plateau.nbCasesOccupees(joueurClair, Pion.CHAMP));
        plateau.getCases().put(new Point(0, 3), new Case(tuileOrientee, joueurFonce, Pion.CHAMP));
        assertEquals(2, plateau.nbCasesOccupees(joueurFonce, Pion.HUTTE));
        assertEquals(1, plateau.nbCasesOccupees(joueurFonce, Pion.CHAMP));
        assertEquals(0, plateau.nbCasesOccupees(joueurClair, Pion.HUTTE));
        assertEquals(0, plateau.nbCasesOccupees(joueurClair, Pion.CHAMP));
        plateau.getCases().put(new Point(0, 4), new Case(tuileOrientee, joueurClair, Pion.CHAMP));
        assertEquals(2, plateau.nbCasesOccupees(joueurFonce, Pion.HUTTE));
        assertEquals(1, plateau.nbCasesOccupees(joueurFonce, Pion.CHAMP));
        assertEquals(0, plateau.nbCasesOccupees(joueurClair, Pion.HUTTE));
        assertEquals(1, plateau.nbCasesOccupees(joueurClair, Pion.CHAMP));
        // Erreur car pas de joueur.
        try {
            plateau.nbCasesOccupees(null, Pion.HUTTE);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            assertFalse(true);
        }
        // Erreur car pas de pion.
        try {
            plateau.nbCasesOccupees(joueurFonce, null);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            assertFalse(true);
        }
    }

    /**
     * Tests pour l'exemple particulier.
     */
    @Test
    public void testsExemple() {
        List<Point> coordLigColAdjacentesAttendues = new ArrayList<Point>();
        Map<Point, List<TuileOrientee>> coordLigColAdjacentesTuilesOrientees;
        Plateau plateau = new Plateau_ETD();
        assertNull(plateau.coordLigColMinMax(true));
        assertNull(plateau.coordLigColMinMax(false));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(0, coordLigColAdjacentesTuilesOrientees.size());
        // Plateau avec les seules tuiles de départ.
        for (TuileDepart tuileDepart : TuileDepart.values()) {
            plateau.getCases().put(tuileDepart.getCoordLigCol(), new Case(tuileDepart.getTuileOrientee(), null, null));
        }
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues.addAll(Arrays.asList(new Point(0, -1), new Point(0, 1), new Point(2, 0)));
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        assertTrue(plateau.coordLigColMinMax(true).equals(new Point(0, 0)));
        assertTrue(plateau.coordLigColMinMax(false).equals(new Point(1, 1)));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(3, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(0, -1)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(0, 1)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(2, 0)));
        // Phase de découverte : pose des tuiles et installation des huttes.
        Joueur joueurFonce = new Joueur(Couleur.FONCE);
        Joueur joueurClair = new Joueur(Couleur.CLAIR);
        plateau.poserTuile(new Point(2, 0), TuileOrientee.AACCCA, null, null);
        plateau.poserTuile(new Point(2, 1), TuileOrientee.AACCCA, null, null);
        plateau.poserTuile(new Point(0, -1), TuileOrientee.BBAAAB, null, null);
        plateau.poserTuile(new Point(-1, 0), TuileOrientee.ABBBAA, null, null);
        plateau.poserTuile(new Point(-1, 1), TuileOrientee.AAACCA, joueurFonce, Pion.HUTTE);
        plateau.poserTuile(new Point(1, -1), TuileOrientee.ABBBAA, null, null);
        plateau.poserTuile(new Point(0, -2), TuileOrientee.BBBBAB, null, null);
        plateau.poserTuile(new Point(3, 1), TuileOrientee.CCCACC, joueurClair, Pion.HUTTE);
        plateau.poserTuile(new Point(3, 2), TuileOrientee.CCCCCC, null, null);
        plateau.poserTuile(new Point(2, 2), TuileOrientee.AACCCC, null, null);
        plateau.poserTuile(new Point(3, 3), TuileOrientee.CCCCAA, null, null);
        plateau.poserTuile(new Point(2, 3), TuileOrientee.ACCAAC, joueurClair, Pion.HUTTE);
        plateau.poserTuile(new Point(3, 4), TuileOrientee.AAAAAB, null, null);
        plateau.poserTuile(new Point(2, 4), TuileOrientee.CCABAA, null, null);
        plateau.poserTuile(new Point(3, 5), TuileOrientee.ABAACA, joueurFonce, Pion.HUTTE);
        plateau.poserTuile(new Point(4, 4), TuileOrientee.AABBBA, null, null);
        plateau.poserTuile(new Point(2, 5), TuileOrientee.CAAACC, joueurFonce, Pion.HUTTE);
        plateau.poserTuile(new Point(1, 5), TuileOrientee.AACACA, null, null);
        plateau.poserTuile(new Point(1, 6), TuileOrientee.BACCAB, null, null);
        plateau.poserTuile(new Point(2, 6), TuileOrientee.ACCACC, null, null);
        plateau.poserTuile(new Point(1, 7), TuileOrientee.BBACAA, null, null);
        plateau.poserTuile(new Point(0, 5), TuileOrientee.BAAABB, null, null);
        plateau.poserTuile(new Point(0, 4), TuileOrientee.AAAAAA, null, null);
        plateau.poserTuile(new Point(-2, 0), TuileOrientee.CAAAAA, null, null);
        plateau.poserTuile(new Point(-2, 1), TuileOrientee.AAAAAA, null, null);
        plateau.poserTuile(new Point(-1, 2), TuileOrientee.AACCAA, null, null);
        plateau.poserTuile(new Point(-2, 2), TuileOrientee.AAAACC, joueurFonce, Pion.HUTTE);
        plateau.poserTuile(new Point(-3, 2), TuileOrientee.BAAAAB, joueurClair, Pion.HUTTE);
        plateau.poserTuile(new Point(-3, 1), TuileOrientee.AACAAA, null, null);
        plateau.poserTuile(new Point(-4, 1), TuileOrientee.BBAABB, null, null);
        plateau.poserTuile(new Point(-4, 0), TuileOrientee.BBBAAB, null, null);
        plateau.poserTuile(new Point(-3, 0), TuileOrientee.BBBACA, null, null);
        plateau.poserTuile(new Point(-2, -1), TuileOrientee.BBBBAA, null, null);
        plateau.poserTuile(new Point(-3, 3), TuileOrientee.BBACAB, null, null);
        plateau.poserTuile(new Point(-2, 3), TuileOrientee.ACCAAA, null, null);
        plateau.poserTuile(new Point(-3, 4), TuileOrientee.BBAABB, null, null);
        plateau.poserTuile(new Point(2, 7), TuileOrientee.ACCAAA, null, null);
        // Coordonnées (ligne et colonne) des cases adjacentes en fin de phase
        // de découverte.
        coordLigColAdjacentesAttendues.clear();
        List<Point> coordLigColAdjacentesAttenduesMerSeparantTerre = new ArrayList<Point>();
        List<Point> coordLigColAdjacentesAttenduesTerreNonSeparee = new ArrayList<Point>();
        coordLigColAdjacentesAttenduesMerSeparantTerre = Arrays.asList(new Point(-5, 1), new Point(-4, -1),
                new Point(-4, 2), new Point(-4, 3), new Point(-3, -1), new Point(-1, -1), new Point(0, 6),
                new Point(1, -2));
        coordLigColAdjacentesAttenduesTerreNonSeparee = Arrays.asList(new Point(-2, 4), new Point(-1, 3),
                new Point(-1, 4), new Point(-1, 5), new Point(0, 1), new Point(1, 2), new Point(1, 3), new Point(1, 4),
                new Point(1, 8), new Point(2, -1), new Point(3, 0), new Point(3, 6), new Point(3, 7), new Point(4, 1),
                new Point(4, 2), new Point(4, 3), new Point(4, 5));
        coordLigColAdjacentesAttendues.addAll(coordLigColAdjacentesAttenduesMerSeparantTerre);
        coordLigColAdjacentesAttendues.addAll(coordLigColAdjacentesAttenduesTerreNonSeparee);
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        assertTrue(plateau.coordLigColMinMax(true).equals(new Point(-4, -2)));
        assertTrue(plateau.coordLigColMinMax(false).equals(new Point(4, 7)));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(17, coordLigColAdjacentesTuilesOrientees.size());
        assertNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-5, 1)));
        assertNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-4, -1)));
        assertNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-4, 2)));
        assertNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-4, 3)));
        assertNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-3, -1)));
        assertNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-1, -1)));
        assertNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, -2)));
        assertNull(coordLigColAdjacentesTuilesOrientees.get(new Point(0, 6)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-2, 4)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-1, 3)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-1, 4)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-1, 5)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(0, 1)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 2)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 3)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 4)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 8)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(2, -1)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(3, 0)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(3, 6)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(3, 7)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(4, 1)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(4, 2)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(4, 3)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(4, 5)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(-1, 4))
                .equals(Arrays.asList(TuileOrientee.AAAAAA, TuileOrientee.AAAAAB, TuileOrientee.AAAAAC,
                        TuileOrientee.AABAAA, TuileOrientee.AABAAC, TuileOrientee.AACAAA, TuileOrientee.AACAAB)));
        // Nombre de pions en fin de phase de découverte.
        assertEquals(4, plateau.nbCasesOccupees(joueurFonce, Pion.HUTTE));
        assertEquals(0, plateau.nbCasesOccupees(joueurFonce, Pion.CHAMP));
        assertEquals(3, plateau.nbCasesOccupees(joueurClair, Pion.HUTTE));
        assertEquals(0, plateau.nbCasesOccupees(joueurClair, Pion.CHAMP));
        // Phase de colonisation : installation des champs.
        plateau.installerChamp(new Point(2, 4), joueurClair);
        plateau.installerChamp(new Point(1, 5), joueurFonce);
        plateau.installerChamp(new Point(-2, 1), joueurClair);
        plateau.installerChamp(new Point(-2, 0), joueurFonce);
        plateau.installerChamp(new Point(-3, 1), joueurClair);
        plateau.installerChamp(new Point(-3, 0), joueurFonce);
        plateau.installerChamp(new Point(-3, 3), joueurClair);
        plateau.installerChamp(new Point(3, 4), joueurFonce);
        plateau.installerChamp(new Point(-4, 0), joueurClair);
        plateau.installerChamp(new Point(3, 3), joueurFonce);
        plateau.installerChamp(new Point(-2, 3), joueurClair);
        plateau.installerChamp(new Point(-1, 2), joueurFonce);
        plateau.installerChamp(new Point(-3, 4), joueurClair);
        plateau.installerChamp(new Point(4, 4), joueurFonce);
        plateau.installerChamp(new Point(-4, 1), joueurClair);
        plateau.installerChamp(new Point(-2, -1), joueurFonce);
        plateau.installerChamp(new Point(-1, 0), joueurFonce);
        plateau.installerChamp(new Point(0, 0), joueurFonce);
        plateau.installerChamp(new Point(0, -1), joueurFonce);
        plateau.installerChamp(new Point(0, -2), joueurFonce);
        plateau.installerChamp(new Point(1, -1), joueurFonce);
        plateau.installerChamp(new Point(1, 0), joueurFonce);
        plateau.installerChamp(new Point(1, 1), joueurFonce);
        plateau.installerChamp(new Point(2, 0), joueurFonce);
        plateau.installerChamp(new Point(2, 1), joueurFonce);
        plateau.installerChamp(new Point(2, 2), joueurFonce);
        plateau.installerChamp(new Point(0, 4), joueurFonce);
        plateau.installerChamp(new Point(0, 5), joueurFonce);
        // Nombre de pions en fin de phase de colonisation.
        assertEquals(4, plateau.nbCasesOccupees(joueurFonce, Pion.HUTTE));
        assertEquals(20, plateau.nbCasesOccupees(joueurFonce, Pion.CHAMP));
        assertEquals(3, plateau.nbCasesOccupees(joueurClair, Pion.HUTTE));
        assertEquals(8, plateau.nbCasesOccupees(joueurClair, Pion.CHAMP));
        // Cases occupées/inoccupées.
        List<Point> coordLigColCasesInoccupees = new ArrayList<Point>();
        coordLigColCasesInoccupees
                .addAll(Arrays.asList(new Point(1, 6), new Point(2, 6), new Point(2, 7), new Point(1, 7)));
        List<Point> coordLigColCasesDepart = new ArrayList<Point>();
        coordLigColCasesDepart.addAll(Arrays.asList(new Point(1, 0), new Point(1, 1), new Point(0, 0)));
        for (Entry<Point, Case> coordLigColCase : plateau.getCases().entrySet()) {
            assertTrue(((coordLigColCasesInoccupees.contains(coordLigColCase.getKey())
                    || coordLigColCasesDepart.contains(coordLigColCase.getKey())
                    || coordLigColCase.getValue().getTuileOrientee().equals(TuileOrientee.CCCCCC))
                    && coordLigColCase.getValue().estInoccupee())
                    || (!coordLigColCasesInoccupees.contains(coordLigColCase.getKey())
                    && !coordLigColCase.getValue().estInoccupee()));
        }
        // Sommets du graphe des tuiles du plateau en considérant les terres
        // arables durant la phase de découverte.
        assertEquals(plateau.getCases().size(), plateau.sommetsGraphes().size());
        for (Point coordLigCol : Arrays.asList(new Point(1, 6), new Point(1, 7), new Point(2, 6), new Point(2, 7),
                new Point(3, 2))) {
            assertNull(plateau.sommetsGraphes().get(coordLigCol));
        }
        for (Point coordLigCol : Arrays.asList(new Point(0, 0), new Point(0, -1), new Point(0, -2), new Point(0, 4),
                new Point(0, 5), new Point(1, 0), new Point(-1, 0), new Point(1, 1), new Point(1, -1), new Point(-1, 1),
                new Point(-1, 2), new Point(1, 5), new Point(2, 0), new Point(-2, 0), new Point(2, 1),
                new Point(-2, -1), new Point(2, 2), new Point(-2, 2), new Point(2, 5), new Point(-3, 0),
                new Point(3, 3), new Point(3, 4), new Point(3, 5), new Point(4, 4))) {
            assertTrue(plateau.sommetsGraphes().get(coordLigCol).equals(joueurFonce));
        }
        for (Point coordLigCol : Arrays.asList(new Point(-2, 1), new Point(2, 3), new Point(-2, 3), new Point(2, 4),
                new Point(3, 1), new Point(-3, 1), new Point(-3, 2), new Point(-3, 3), new Point(-3, 4),
                new Point(-4, 0), new Point(-4, 1))) {
            assertTrue(plateau.sommetsGraphes().get(coordLigCol).equals(joueurClair));
        }
        // Graphe des tuiles du plateau en considérant les terres arables durant
        // la phase de découverte.
        List<Point> pairesCoordLigColReliesTerresArables = new ArrayList<Point>();
        pairesCoordLigColReliesTerresArables.addAll(Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(0, 0),
                new Point(1, 1), new Point(0, -1), new Point(0, 0), new Point(0, -1), new Point(1, 0), new Point(0, -1),
                new Point(1, -1), new Point(0, -2), new Point(0, -1), new Point(0, -2), new Point(1, -1),
                new Point(0, 4), new Point(0, 5), new Point(0, 4), new Point(1, 5), new Point(0, 5), new Point(1, 5),
                new Point(0, 5), new Point(1, 6), new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(1, 1),
                new Point(-1, 0), new Point(-1, 1), new Point(1, 0), new Point(2, 0), new Point(-1, 1), new Point(0, 0),
                new Point(1, -1), new Point(1, 0), new Point(-1, 1), new Point(-1, 2), new Point(1, 1), new Point(2, 0),
                new Point(1, 1), new Point(2, 1), new Point(1, 5), new Point(2, 5), new Point(1, 6), new Point(1, 7),
                new Point(1, 6), new Point(2, 6), new Point(1, 7), new Point(2, 6), new Point(1, 7), new Point(2, 7),
                new Point(1, 5), new Point(1, 6), new Point(1, 5), new Point(2, 4), new Point(-2, 0), new Point(-1, 0),
                new Point(-2, 0), new Point(-1, 1), new Point(2, 0), new Point(2, 1), new Point(-2, -1),
                new Point(-1, 0), new Point(-2, -1), new Point(-2, 0), new Point(2, 1), new Point(2, 2),
                new Point(-2, 2), new Point(-1, 2), new Point(2, 3), new Point(2, 4), new Point(2, 5), new Point(3, 5),
                new Point(-2, 0), new Point(-2, 1), new Point(-2, 1), new Point(-1, 1), new Point(-2, 1),
                new Point(-1, 2), new Point(-2, 1), new Point(-2, 2), new Point(2, 3), new Point(3, 3), new Point(2, 3),
                new Point(3, 4), new Point(2, 4), new Point(2, 5), new Point(2, 4), new Point(3, 4), new Point(2, 4),
                new Point(3, 5), new Point(-3, 0), new Point(-2, 0), new Point(-3, 0), new Point(-2, -1),
                new Point(-3, 1), new Point(-2, 1), new Point(-3, 1), new Point(-3, 2), new Point(-3, 2),
                new Point(-2, 1), new Point(-3, 2), new Point(-3, 3), new Point(-3, 3), new Point(-2, 3),
                new Point(3, 3), new Point(3, 4), new Point(-3, 3), new Point(-3, 4), new Point(-3, 4),
                new Point(-2, 3), new Point(3, 4), new Point(3, 5), new Point(3, 4), new Point(4, 4), new Point(3, 5),
                new Point(4, 4), new Point(-3, 0), new Point(-3, 1), new Point(-3, 1), new Point(-2, 0),
                new Point(-3, 2), new Point(-2, 2), new Point(-3, 3), new Point(-2, 2), new Point(-4, 0),
                new Point(-3, 1), new Point(-4, 0), new Point(-4, 1), new Point(-4, 1), new Point(-3, 1),
                new Point(-4, 1), new Point(-3, 2), new Point(-4, 0), new Point(-3, 0)));
        Map<Point, Map<Point, Boolean>> grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        for (int indPairesCoordLigColReliesTerresArables = 0; indPairesCoordLigColReliesTerresArables <= pairesCoordLigColReliesTerresArables
                .size() - 2; indPairesCoordLigColReliesTerresArables += 2) {
            assertTrue(grapheDecouverteTerresArables
                    .get(pairesCoordLigColReliesTerresArables.get(indPairesCoordLigColReliesTerresArables))
                    .get(pairesCoordLigColReliesTerresArables.get(indPairesCoordLigColReliesTerresArables + 1)));
            assertTrue(grapheDecouverteTerresArables
                    .get(pairesCoordLigColReliesTerresArables.get(indPairesCoordLigColReliesTerresArables + 1))
                    .get(pairesCoordLigColReliesTerresArables.get(indPairesCoordLigColReliesTerresArables)));
        }
        int nbLiaisonsTerresArables = 0;
        for (Point coordLigCol1re : grapheDecouverteTerresArables.keySet()) {
            assertFalse(grapheDecouverteTerresArables.get(coordLigCol1re).get(coordLigCol1re));
            for (Point coordLigCol2de : grapheDecouverteTerresArables.get(coordLigCol1re).keySet()) {
                assertTrue(grapheDecouverteTerresArables.get(coordLigCol1re)
                        .get(coordLigCol2de) == grapheDecouverteTerresArables.get(coordLigCol2de).get(coordLigCol1re));
                if (grapheDecouverteTerresArables.get(coordLigCol1re).get(coordLigCol2de)) {
                    ++nbLiaisonsTerresArables;
                }
            }
        }
        assertEquals(pairesCoordLigColReliesTerresArables.size() * 2 / 2, nbLiaisonsTerresArables);
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        // Export Graphviz du graphe de la phase de colonisation.
        plateau.exportGvColonisation();
    }

    /**
     * Tests des coordonnées (ligne et colonne) de toutes les cases sans tuile
     * (inexistante) adjacentes à au moins deux cases où sont posées des tuiles
     * (existantes).
     */
    @Test
    public void testsCoordLigColAdjacentes() {
        List<Point> coordLigColAdjacentesAttendues = new ArrayList<Point>();
        Plateau plateau;
        Case caseDefaut = new Case(TuileOrientee.AAAAAA, null, null);
        // 0 case adjacente.
        plateau = new Plateau_ETD();
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues.addAll(Arrays.asList());
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        // 1 case adjacente.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), caseDefaut);
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues.addAll(Arrays.asList());
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        // 2 cases adjacentes : 1er exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), caseDefaut);
        plateau.getCases().put(new Point(2, 0), caseDefaut);
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues.addAll(Arrays.asList(new Point(1, 0), new Point(1, 1)));
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        // 2 cases adjacentes : 2e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), caseDefaut);
        plateau.getCases().put(new Point(0, 1), caseDefaut);
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues.addAll(Arrays.asList(new Point(-1, 1), new Point(1, 1)));
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        // 2 cases adjacentes : 3e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), caseDefaut);
        plateau.getCases().put(new Point(2, 1), caseDefaut);
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues.addAll(Arrays.asList(new Point(1, 1)));
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        // 3 cases adjacentes : 1er exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), caseDefaut);
        plateau.getCases().put(new Point(1, 0), caseDefaut);
        plateau.getCases().put(new Point(2, 0), caseDefaut);
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues.addAll(Arrays.asList(new Point(1, 1), new Point(0, -1), new Point(2, -1)));
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        // 3 cases adjacentes : 2e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), caseDefaut);
        plateau.getCases().put(new Point(0, 1), caseDefaut);
        plateau.getCases().put(new Point(2, 0), caseDefaut);
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues.addAll(Arrays.asList(new Point(1, 1), new Point(1, 0), new Point(-1, 1)));
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        // 3 cases adjacentes : 3e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), caseDefaut);
        plateau.getCases().put(new Point(1, 2), caseDefaut);
        plateau.getCases().put(new Point(2, 0), caseDefaut);
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues
                .addAll(Arrays.asList(new Point(1, 1), new Point(0, 1), new Point(1, 0), new Point(2, 1)));
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        // 4 cases adjacentes : 1er exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), caseDefaut);
        plateau.getCases().put(new Point(0, 1), caseDefaut);
        plateau.getCases().put(new Point(1, 0), caseDefaut);
        plateau.getCases().put(new Point(1, 2), caseDefaut);
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues
                .addAll(Arrays.asList(new Point(1, 1), new Point(-1, 1), new Point(0, -1), new Point(0, 2)));
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        // 4 cases adjacentes : 2e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), caseDefaut);
        plateau.getCases().put(new Point(1, 0), caseDefaut);
        plateau.getCases().put(new Point(2, 0), caseDefaut);
        plateau.getCases().put(new Point(1, 2), caseDefaut);
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues.addAll(
                Arrays.asList(new Point(1, 1), new Point(0, -1), new Point(2, -1), new Point(0, 1), new Point(2, 1)));
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        // 4 cases adjacentes : 3e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), caseDefaut);
        plateau.getCases().put(new Point(0, 1), caseDefaut);
        plateau.getCases().put(new Point(2, 0), caseDefaut);
        plateau.getCases().put(new Point(2, 1), caseDefaut);
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues.addAll(
                Arrays.asList(new Point(1, 1), new Point(-1, 1), new Point(1, 0), new Point(1, 2), new Point(3, 1)));
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        // 5 cases adjacentes.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), caseDefaut);
        plateau.getCases().put(new Point(0, 1), caseDefaut);
        plateau.getCases().put(new Point(1, 0), caseDefaut);
        plateau.getCases().put(new Point(2, 0), caseDefaut);
        plateau.getCases().put(new Point(2, 1), caseDefaut);
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues.addAll(Arrays.asList(new Point(1, 1), new Point(-1, 1), new Point(0, -1),
                new Point(1, 2), new Point(2, -1), new Point(3, 1)));
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
        // 6 cases adjacentes.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), caseDefaut);
        plateau.getCases().put(new Point(0, 1), caseDefaut);
        plateau.getCases().put(new Point(1, 0), caseDefaut);
        plateau.getCases().put(new Point(1, 2), caseDefaut);
        plateau.getCases().put(new Point(2, 0), caseDefaut);
        plateau.getCases().put(new Point(2, 1), caseDefaut);
        coordLigColAdjacentesAttendues.clear();
        coordLigColAdjacentesAttendues.addAll(Arrays.asList(new Point(1, 1), new Point(-1, 1), new Point(0, -1),
                new Point(2, -1), new Point(3, 1), new Point(2, 2), new Point(0, 2)));
        testsCoordLigColAdjacentes(coordLigColAdjacentesAttendues, plateau.coordLigColAdjacentes(), plateau);
    }

    /**
     * Un seul test des coordonnées (ligne et colonne) de toutes les cases sans
     * tuile (inexistante) adjacentes à au moins deux cases où sont posées des
     * tuiles (existantes).
     * <p>
     * Comparaison entre les données attendues, les données calculées, et les
     * données recalculées par un autre algorithme (cf. la classe développée
     * pour une autre version) = on calcule les coordonnées minimale et
     * maximale, on balaye entièrement le plateau et on regarde autour des cases
     * sans tuile (inexistante).
     * <p>
     * Pré-condition : coordLigColAdjacentesAttendues ne contient aucun doublon
     * (ces données viennent des tests).
     *
     * @param coordLigColAdjacentesAttendues Coordonnées (ligne et colonne) attendues.
     * @param coordLigColAdjacentesCalculees Coordonnées (ligne et colonne) calculées.
     * @param plateau                        Plateau.
     */
    public void testsCoordLigColAdjacentes(List<Point> coordLigColAdjacentesAttendues,
                                           final List<Point> coordLigColAdjacentesCalculees, final Plateau plateau) {
        // A-t-on données recalculées = données calculées ?
        List<Point> coordLigColAdjacentesRecalculees = new ArrayList<Point>();
        Point coordLigColMin = plateau.coordLigColMinMax(true);
        if (coordLigColMin != null) {
            Point coordLigColMax = plateau.coordLigColMinMax(false);
            for (int lig = coordLigColMin.x - 1; lig <= coordLigColMax.x + 1; ++lig) {
                for (int col = coordLigColMin.y - 1; col <= coordLigColMax.y + 1; ++col) {
                    Point coodLigColCaseInexist = new Point(lig, col);
                    if (plateau.getCase(coodLigColCaseInexist) == null) {
                        int nbCasesExistAdjCaseInexist = 0;
                        for (Direction directionCaseInexist : Direction.values()) {
                            if (plateau.getCase(
                                    directionCaseInexist.coordLigColDestination(coodLigColCaseInexist)) != null) {
                                ++nbCasesExistAdjCaseInexist;
                            }
                        }
                        if (nbCasesExistAdjCaseInexist >= 2
                                && !coordLigColAdjacentesRecalculees.contains(coodLigColCaseInexist)) {
                            coordLigColAdjacentesRecalculees.add(coodLigColCaseInexist);
                        }
                    }
                }
            }
        }
        assertEquals(coordLigColAdjacentesRecalculees.size(), coordLigColAdjacentesCalculees.size());
        coordLigColAdjacentesRecalculees.removeAll(coordLigColAdjacentesCalculees);
        assertTrue(coordLigColAdjacentesRecalculees.isEmpty());
        // A-t-on données attendues = données calculées ?
        assertEquals(coordLigColAdjacentesAttendues.size(), coordLigColAdjacentesCalculees.size());
        coordLigColAdjacentesAttendues.removeAll(coordLigColAdjacentesCalculees);
        assertTrue(coordLigColAdjacentesAttendues.isEmpty());
    }

    /**
     * Tests des coordonnées (ligne et colonne) de toutes les cases sans tuile
     * (inexistante) adjacentes à au moins deux cases où sont posées des tuiles
     * (existantes) avec toutes les tuiles orientées qui peuvent y être posées
     * en vérifiant qu'il y a toujours un seul ensemble de terres (3e règle de
     * pose des tuiles).
     */
    @Test
    public void testsCoordLigColAdjacentesTuilesOrientees() {
        Map<Point, List<TuileOrientee>> coordLigColAdjacentesTuilesOrientees;
        Plateau plateau;
        // 2 cases adjacentes, 1er exemple, mer séparant la terre.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABBA, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AABBAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(1, coordLigColAdjacentesTuilesOrientees.size());
        assertNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-1, 1)));
        // 2 cases adjacentes, 1er exemple, terre non séparée, 1er cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAABA, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AABBAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(2, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.BAAAAB));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-1, 1)));
        // 2 cases adjacentes, 1er exemple, terre non séparée, 2e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABAA, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AAABAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(2, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.ABBBBB));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-1, 1)));
        // 2 cases adjacentes, 1er exemple, terre non séparée, 3e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABAA, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AAACAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(2, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.ABAACC));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-1, 1)));
        // 2 cases adjacentes, 1er exemple, terre non séparée, 4e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAABA, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AABAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(2, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.BAAAAA));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-1, 1)));
        // 2 cases adjacentes, 1er exemple, terre non séparée, 5e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAACA, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AACCAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(2, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.CAAAAC));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-1, 1)));
        // 2 cases adjacentes, 1er exemple, terre non séparée, 6e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAAAA, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AAAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(2, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.AAAAAA));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(-1, 1)));
        // 2 cases adjacentes, 2e exemple, mer séparant la terre.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABBA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.BBAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(0, coordLigColAdjacentesTuilesOrientees.size());
        assertNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        // 2 cases adjacentes, 2e exemple, terre non séparée, 1er cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABBA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.ABAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(1, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.BBBBAB));
        // 2 cases adjacentes, 2e exemple, terre non séparée, 2e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABBA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.ACAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(1, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.BBACAA));
        // 2 cases adjacentes, 2e exemple, terre non séparée, 3e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABBA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.AAAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(1, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.BBAAAA));
        // 2 cases adjacentes, 2e exemple, terre non séparée, 4e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAABA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.BAAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(1, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.BAAABB));
        // 2 cases adjacentes, 2e exemple, terre non séparée, 5e cas (pas de
        // tuile orientée "BA_BA_").
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAABA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.ABAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(0, coordLigColAdjacentesTuilesOrientees.size());
        assertNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        // 2 cases adjacentes, 2e exemple, terre non séparée, 6e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAABA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.AAAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(1, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.BAAAAA));
        // 2 cases adjacentes, 2e exemple, terre non séparée, 7e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAABA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.ACAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(1, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.BAACAA));
        // 2 cases adjacentes, 2e exemple, terre non séparée, 8e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAABA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(1, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.BAACCA));
        // 2 cases adjacentes, 2e exemple, terre non séparée, 9e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAAAA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.AAAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(1, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.AAAAAA));
        // 2 cases adjacentes, 2e exemple, terre non séparée, 9e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAAAA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(1, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.AAACCA));
        // 2 cases adjacentes, 2e exemple, terre non séparée, 10e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAACA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.AAAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(1, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.CAAAAA));
        // 2 cases adjacentes, 2e exemple, terre non séparée, 11e cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAACA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.ACAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(1, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.CAACAC));
        // 3 cases adjacentes, 1er exemple, mer séparant la terre.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABBA, null, null));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.ABBAAA, null, null));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.BAAAAB, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(3, coordLigColAdjacentesTuilesOrientees.size());
        assertNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(0, 1)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 0)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(2, 1)));
        // 3 cases adjacentes, 1er exemple, terre non séparée, 1er cas.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABBA, null, null));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.AAAAAA, null, null));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.BAAAAA, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(4, coordLigColAdjacentesTuilesOrientees.size());
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertTrue(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)).contains(TuileOrientee.BBBAAA));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(0, 1)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 0)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(2, 1)));
        // 3 cases adjacentes, 1er exemple, terre non séparée, 2e cas (pas de
        // tuile orientée "BBABAA").
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABBA, null, null));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.AAAAAA, null, null));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.AAAAAB, null, null));
        coordLigColAdjacentesTuilesOrientees = plateau.coordLigColAdjacentesTuilesOrientees();
        assertEquals(3, coordLigColAdjacentesTuilesOrientees.size());
        assertNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 1)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(0, 1)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(1, 0)));
        assertNotNull(coordLigColAdjacentesTuilesOrientees.get(new Point(2, 1)));
    }

    /**
     * Tests d'installation d'un champ dans une ancienne case du plateau.
     */
    @Test
    public void testsCoordLigColPossibleInstallerChamp() {
        Plateau plateau;
        Joueur joueur = new Joueur(Couleur.FONCE);
        Joueur joueurAdversaire = new Joueur(Couleur.CLAIR);
        List<Point> coordLigColPossibleInstallerChamp;
        // Erreur car pas de joueur.
        try {
            (new Plateau_ETD()).coordLigColPossibleInstallerChamp(null);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            assertFalse(true);
        }
        // 1er exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABBA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.AAAAAC, joueurAdversaire, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCAAAA, joueur, Pion.HUTTE));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AABAAA, joueurAdversaire, Pion.HUTTE));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.BBACCA, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(1, coordLigColPossibleInstallerChamp.size());
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 1)));
        // 2e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AACCCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.CAAACC, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCAAAC, joueurAdversaire, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.ACCCAA, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.CCCCCC, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 3e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AACCCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.CAAACC, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.CAAAAC, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCAAAC, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.ACCCAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AACCAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.CCCCCC, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 4e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.CCCAAC, null, null));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.CCCCCA, null, null));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.CCCCCA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.AACCCC, null, null));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.BBACCA, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.CCABBA, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.AACAAB, joueur, Pion.CHAMP));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(6, coordLigColPossibleInstallerChamp.size());
        for (Point coordLigCol : Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(2, 1),
                new Point(1, 2), new Point(0, 1))) {
            assertTrue(coordLigColPossibleInstallerChamp.contains(coordLigCol));
        }
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 5e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.AAAAAA, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(1, coordLigColPossibleInstallerChamp.size());
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 1)));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 6e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAAAA, null, null));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.AAAAAA, null, null));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.AAAAAA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.AAAAAA, null, null));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.AAAAAA, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AAAAAA, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.AAAAAA, joueur, Pion.HUTTE));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(6, coordLigColPossibleInstallerChamp.size());
        for (Point coordLigCol : Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(2, 1),
                new Point(1, 2), new Point(0, 1))) {
            assertTrue(coordLigColPossibleInstallerChamp.contains(coordLigCol));
        }
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 7e exemple.
        plateau = new Plateau_ETD();
        for (int lig = -10; lig <= 10; ++lig) {
            for (int col = -10; col <= 10; ++col) {
                plateau.getCases().put(new Point(lig, col), new Case(TuileOrientee.AAAAAA, null, null));
            }
        }
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.AAAAAA, joueur, Pion.HUTTE));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(6, coordLigColPossibleInstallerChamp.size());
        for (Point coordLigCol : Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(2, 1),
                new Point(1, 2), new Point(0, 1))) {
            assertTrue(coordLigColPossibleInstallerChamp.contains(coordLigCol));
        }
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 8e exemple.
        plateau = new Plateau_ETD();
        for (int lig = -10; lig <= 10; ++lig) {
            for (int col = -10; col <= 10; ++col) {
                plateau.getCases().put(new Point(lig, col), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
            }
        }
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.AAAAAA, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(1, coordLigColPossibleInstallerChamp.size());
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 1)));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 9e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.AAAAAA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.AAAAAA, joueurAdversaire, Pion.CHAMP));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(1, coordLigColPossibleInstallerChamp.size());
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 1)));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(1, coordLigColPossibleInstallerChamp.size());
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 1)));
        // 10e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(1, -1), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.AAAAAA, joueurAdversaire, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.AAAAAA, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(1, coordLigColPossibleInstallerChamp.size());
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 1)));
        // 11e exemple.
        plateau = new Plateau_ETD();
        final int LIG_MAX = 100;
        for (int lig = -LIG_MAX; lig <= LIG_MAX; ++lig) {
            if (lig % 4 == 0) {
                plateau.getCases().put(new Point(lig, 0), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
            } else {
                plateau.getCases().put(new Point(lig, 0), new Case(TuileOrientee.AAAAAA, null, null));
            }
        }
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        for (int lig = -LIG_MAX; lig <= LIG_MAX; ++lig) {
            if (lig % 2 == 1) {
                assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(lig, 0)));
            }
        }
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 12e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABAA, null, null));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.AAAAAB, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.ABAAAA, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 13e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.AAAAAB, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.ABAAAA, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(2, coordLigColPossibleInstallerChamp.size());
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 0)));
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 1)));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 14e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAACAA, joueur, Pion.HUTTE));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.AAAAAC, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.ACAAAA, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(2, coordLigColPossibleInstallerChamp.size());
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 0)));
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 1)));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 15e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.BBBABB, joueur, Pion.HUTTE));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.BBBBBA, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.BABBBB, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(2, coordLigColPossibleInstallerChamp.size());
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 0)));
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 1)));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 16e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.CCCACC, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.CCCCCA, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.CACCCC, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(2, coordLigColPossibleInstallerChamp.size());
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 0)));
        assertTrue(coordLigColPossibleInstallerChamp.contains(new Point(1, 1)));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 17e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.CCACCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.CACCAC, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCACCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.CACCAC, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.CCCCCC, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(4, coordLigColPossibleInstallerChamp.size());
        for (Point coordLigCol : Arrays.asList(new Point(1, 0), new Point(2, 0), new Point(1, 2), new Point(0, 1))) {
            assertTrue(coordLigColPossibleInstallerChamp.contains(coordLigCol));
        }
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 18e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.CCACCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.CACCAC, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCACCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.CACCAC, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(4, coordLigColPossibleInstallerChamp.size());
        for (Point coordLigCol : Arrays.asList(new Point(1, 0), new Point(2, 0), new Point(1, 2), new Point(0, 1))) {
            assertTrue(coordLigColPossibleInstallerChamp.contains(coordLigCol));
        }
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 19e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.CCACCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.CACCAC, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCACCA, null, null));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.CACCAC, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(3, coordLigColPossibleInstallerChamp.size());
        for (Point coordLigCol : Arrays.asList(new Point(1, 0), new Point(2, 1), new Point(0, 1))) {
            assertTrue(coordLigColPossibleInstallerChamp.contains(coordLigCol));
        }
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 20e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.CCACCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.ACCACC, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.CACCAC, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCACCA, null, null));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.CACCAC, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(2, coordLigColPossibleInstallerChamp.size());
        for (Point coordLigCol : Arrays.asList(new Point(2, 0), new Point(0, 1))) {
            assertTrue(coordLigColPossibleInstallerChamp.contains(coordLigCol));
        }
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 21e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.CCACCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.CACCAC, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCACCA, null, null));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.CACCAC, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(2, coordLigColPossibleInstallerChamp.size());
        for (Point coordLigCol : Arrays.asList(new Point(1, 0), new Point(0, 1))) {
            assertTrue(coordLigColPossibleInstallerChamp.contains(coordLigCol));
        }
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
        // 22e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.CCACCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.CACCAC, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCACCA, null, null));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.ACCACC, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.CACCAC, null, null));
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueur);
        assertEquals(3, coordLigColPossibleInstallerChamp.size());
        for (Point coordLigCol : Arrays.asList(new Point(1, 0), new Point(2, 1), new Point(0, 1))) {
            assertTrue(coordLigColPossibleInstallerChamp.contains(coordLigCol));
        }
        coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurAdversaire);
        assertEquals(0, coordLigColPossibleInstallerChamp.size());
    }

    /**
     * Tests des sommets représentant les joueurs (ou null si la case ne
     * contient pas de pion) pour les différents graphes.
     */
    @Test
    public void testsSommetsGraphes() {
        Joueur joueur1 = new Joueur(Couleur.FONCE);
        Joueur joueur2 = new Joueur(Couleur.CLAIR);
        Plateau plateau = new Plateau_ETD();
        int i = 0;
        assertEquals(plateau.getCases().size(), plateau.sommetsGraphes().size());
        ++i;
        plateau.poserTuile(new Point(i, i), TuileOrientee.AAAAAA, joueur1, Pion.HUTTE);
        assertEquals(plateau.getCases().size(), plateau.sommetsGraphes().size());
        ++i;
        plateau.poserTuile(new Point(i, i), TuileOrientee.AAAAAB, joueur1, Pion.CHAMP);
        assertEquals(plateau.getCases().size(), plateau.sommetsGraphes().size());
        ++i;
        plateau.poserTuile(new Point(i, i), TuileOrientee.AAAAAC, joueur2, Pion.HUTTE);
        assertEquals(plateau.getCases().size(), plateau.sommetsGraphes().size());
        ++i;
        plateau.poserTuile(new Point(i, i), TuileOrientee.AAAABA, joueur2, Pion.CHAMP);
        assertEquals(plateau.getCases().size(), plateau.sommetsGraphes().size());
        ++i;
        plateau.poserTuile(new Point(i, i), TuileOrientee.AAAABB, null, null);
        assertEquals(plateau.getCases().size(), plateau.sommetsGraphes().size());
        for (int j = 1; j <= 4; ++j) {
            assertTrue(plateau.getCases().get(new Point(j, j)) != null);
            assertTrue(plateau.sommetsGraphes().get(new Point(j, j)) != null);
        }
        assertTrue(plateau.getCases().get(new Point(5, 5)) != null);
        assertTrue(plateau.sommetsGraphes().get(new Point(5, 5)) == null);
        for (int j = 66; j <= 99; ++j) {
            assertTrue(plateau.getCases().get(new Point(j, j)) == null);
            assertTrue(plateau.sommetsGraphes().get(new Point(j, j)) == null);
        }
    }

    /**
     * Tests du graphe des tuiles du plateau en considérant les terres arables
     * durant la phase de découverte.
     */
    @Test
    public void testsGrapheDecouverteTerresArables() {
        Map<Point, Map<Point, Boolean>> grapheDecouverteTerresArables;
        List<Point> coordLigColTuilesPosees = new ArrayList<Point>();
        boolean[][] matriceAdjacenceGrapheDecouverteTerresArables;
        Plateau plateau;
        Joueur joueur = new Joueur(Couleur.FONCE);
        Joueur joueurAdversaire = new Joueur(Couleur.CLAIR);
        final int NB_TUILES_COTE_A_COTE = 42;
        // 1er exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABBA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.AAAAAC, joueurAdversaire, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCAAAA, joueur, Pion.HUTTE));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AABAAA, joueurAdversaire, Pion.HUTTE));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.BBACCA, null, null));
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        coordLigColTuilesPosees.clear();
        coordLigColTuilesPosees.addAll(
                Arrays.asList(new Point(0, 0), new Point(2, 0), new Point(2, 1), new Point(0, 1), new Point(1, 1)));
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 3);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 2);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 4);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 3, 4);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 2e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AACCCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.CAAACC, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCAAAC, joueurAdversaire, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.ACCCAA, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.CCCCCC, null, null));
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        coordLigColTuilesPosees.clear();
        coordLigColTuilesPosees.addAll(
                Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(2, 1), new Point(1, 2), new Point(1, 1)));
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 3e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AACCCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.CAAACC, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.CAAAAC, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCAAAC, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.ACCCAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AACCAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.CCCCCC, null, null));
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        coordLigColTuilesPosees.clear();
        coordLigColTuilesPosees.addAll(Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(2, 1),
                new Point(1, 2), new Point(0, 1), new Point(1, 1)));
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 5);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 2);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 2, 3);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 4, 5);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 4e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.CCCAAC, null, null));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.CCCCCA, null, null));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.CCCCCA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.AACCCC, null, null));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.BBACCA, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.CCABBA, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.AACAAB, joueur, Pion.CHAMP));
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        coordLigColTuilesPosees.clear();
        coordLigColTuilesPosees.addAll(Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(2, 1),
                new Point(1, 2), new Point(0, 1), new Point(1, 1)));
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 1);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 5);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 6);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 6);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 2, 3);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 2, 6);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 3, 4);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 3, 6);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 4, 6);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 5, 6);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 5e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.AAAAAA, null, null));
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        coordLigColTuilesPosees.clear();
        coordLigColTuilesPosees.addAll(Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(2, 1),
                new Point(1, 2), new Point(0, 1), new Point(1, 1)));
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 1);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 5);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 6);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 2);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 6);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 2, 3);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 2, 6);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 3, 4);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 3, 6);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 4, 5);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 4, 6);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 5, 6);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 6e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.AAAAAA, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.AAAAAA, joueurAdversaire, Pion.CHAMP));
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        coordLigColTuilesPosees.clear();
        coordLigColTuilesPosees.addAll(Arrays.asList(new Point(0, 0), new Point(1, 1), new Point(2, 1)));
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 1);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 2);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 7e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(1, -1), new Case(TuileOrientee.AAAAAA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.AAAAAA, joueurAdversaire, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.AAAAAA, null, null));
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        coordLigColTuilesPosees.clear();
        coordLigColTuilesPosees.addAll(Arrays.asList(new Point(1, -1), new Point(1, 0), new Point(1, 1)));
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 1);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 2);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 8e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAABAA, null, null));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.AAAAAB, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.ABAAAA, null, null));
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        coordLigColTuilesPosees.clear();
        coordLigColTuilesPosees.addAll(Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(1, 1)));
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 1);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 2);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 2);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 9e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.AAACAA, joueur, Pion.HUTTE));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.AAAAAC, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.ACAAAA, null, null));
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        coordLigColTuilesPosees.clear();
        coordLigColTuilesPosees.addAll(Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(1, 1)));
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 1);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 2);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 2);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 10e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.BBBABB, joueur, Pion.HUTTE));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.BBBBBA, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.BABBBB, null, null));
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        coordLigColTuilesPosees.clear();
        coordLigColTuilesPosees.addAll(Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(1, 1)));
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 1);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 2);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 2);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 11e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.CCCACC, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.CCCCCA, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.CACCCC, null, null));
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        coordLigColTuilesPosees.clear();
        coordLigColTuilesPosees.addAll(Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(1, 1)));
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 1);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 2);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 2);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 12e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.CCACCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.CACCAC, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCACCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.CACCAC, null, null));
        plateau.getCases().put(new Point(1, 1), new Case(TuileOrientee.CCCCCC, null, null));
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        coordLigColTuilesPosees.clear();
        coordLigColTuilesPosees.addAll(Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(2, 1),
                new Point(1, 2), new Point(0, 1), new Point(1, 1)));
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 1);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 5);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 2);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 2, 3);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 3, 4);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 4, 5);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 13e exemple.
        plateau = new Plateau_ETD();
        plateau.getCases().put(new Point(0, 0), new Case(TuileOrientee.CCACCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 0), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(2, 0), new Case(TuileOrientee.CACCAC, null, null));
        plateau.getCases().put(new Point(2, 1), new Case(TuileOrientee.CCACCA, joueur, Pion.CHAMP));
        plateau.getCases().put(new Point(1, 2), new Case(TuileOrientee.ACCACC, null, null));
        plateau.getCases().put(new Point(0, 1), new Case(TuileOrientee.CACCAC, null, null));
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        coordLigColTuilesPosees.clear();
        coordLigColTuilesPosees.addAll(Arrays.asList(new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(2, 1),
                new Point(1, 2), new Point(0, 1)));
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 1);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 0, 5);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 1, 2);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 2, 3);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 3, 4);
        setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, 4, 5);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 14e exemple.
        plateau = new Plateau_ETD();
        coordLigColTuilesPosees.clear();
        for (int cpt = 0; cpt < NB_TUILES_COTE_A_COTE; ++cpt) {
            Point coordLigCol = new Point(0, 0 + cpt);
            plateau.poserTuile(coordLigCol, TuileOrientee.AAAAAA, null, null);
            coordLigColTuilesPosees.add(coordLigCol);
        }
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        for (int cpt = 1; cpt < NB_TUILES_COTE_A_COTE; ++cpt) {
            setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, cpt - 1,
                    cpt);
        }
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 15e exemple.
        plateau = new Plateau_ETD();
        coordLigColTuilesPosees.clear();
        for (int cpt = 0; cpt < NB_TUILES_COTE_A_COTE; ++cpt) {
            Point coordLigCol = new Point(0, 0 + cpt);
            plateau.poserTuile(coordLigCol, TuileOrientee.AACCCA, null, null);
            coordLigColTuilesPosees.add(coordLigCol);
        }
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        for (int cpt = 1; cpt < NB_TUILES_COTE_A_COTE; ++cpt) {
            setTrueMatriceAdjacenceGrapheDecouverteTerresArables(matriceAdjacenceGrapheDecouverteTerresArables, cpt - 1,
                    cpt);
        }
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 16e exemple.
        plateau = new Plateau_ETD();
        coordLigColTuilesPosees.clear();
        for (int cpt = 0; cpt < NB_TUILES_COTE_A_COTE; ++cpt) {
            Point coordLigCol = new Point(0, 0 + cpt);
            plateau.poserTuile(coordLigCol, TuileOrientee.ACCACC, null, null);
            coordLigColTuilesPosees.add(coordLigCol);
        }
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 17e exemple.
        plateau = new Plateau_ETD();
        coordLigColTuilesPosees.clear();
        for (int cpt = 0; cpt < NB_TUILES_COTE_A_COTE; ++cpt) {
            Point coordLigCol = new Point(0, 0 + cpt);
            plateau.poserTuile(coordLigCol, TuileOrientee.CCCCCC, null, null);
            coordLigColTuilesPosees.add(coordLigCol);
        }
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
        // 18e exemple.
        plateau = new Plateau_ETD();
        coordLigColTuilesPosees.clear();
        for (int cpt = 0; cpt < NB_TUILES_COTE_A_COTE; ++cpt) {
            Point coordLigCol = new Point(0, 0 + cpt);
            plateau.poserTuile(coordLigCol, TuileOrientee.ABBBBB, null, null);
            coordLigColTuilesPosees.add(coordLigCol);
        }
        grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        testSymetrieGrapheDecouverteTerresArables(grapheDecouverteTerresArables);
        testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(grapheDecouverteTerresArables,
                coordLigColTuilesPosees);
        matriceAdjacenceGrapheDecouverteTerresArables = matriceAdjacenceGrapheDecouverteTerresArables(
                coordLigColTuilesPosees);
        cmpMatriceAdjacenceGrapheDecouverteTerresArables(coordLigColTuilesPosees,
                matriceAdjacenceGrapheDecouverteTerresArables, grapheDecouverteTerresArables);
    }

    /**
     * Compare la matrice d'adjacence et le graphe des tuiles du plateau en
     * considérant les terres arables durant la phase de découverte.
     *
     * @param coordLigColTuilesPosees                       Coordonnées (ligne et colonne) des tuiles posées.
     * @param matriceAdjacenceGrapheDecouverteTerresArables Matrice d'adjacence pour le graphe des tuiles du plateau en
     *                                                      considérant les terres arables durant la phase de découverte.
     * @param grapheDecouverteTerresArables                 Graphe des tuiles du plateau en considérant les terres arables
     *                                                      durant la phase de découverte.
     */
    private void cmpMatriceAdjacenceGrapheDecouverteTerresArables(final List<Point> coordLigColTuilesPosees,
                                                                  final boolean[][] matriceAdjacenceGrapheDecouverteTerresArables,
                                                                  final Map<Point, Map<Point, Boolean>> grapheDecouverteTerresArables) {
        final int nbCoordLigColTuilesPosees = coordLigColTuilesPosees.size();
        for (int lig = 0; lig < nbCoordLigColTuilesPosees; ++lig) {
            for (int col = 0; col < nbCoordLigColTuilesPosees; ++col) {
                assertTrue(matriceAdjacenceGrapheDecouverteTerresArables[lig][col] == grapheDecouverteTerresArables
                       .get(coordLigColTuilesPosees.get(lig)).get(coordLigColTuilesPosees.get(col)));
            }
        }
    }

    /**
     * Matrice d'adjacence pour le graphe des tuiles du plateau en considérant
     * les terres arables durant la phase de découverte.
     *
     * @param coordLigColTuilesPosees Coordonnées (ligne et colonne) des tuiles posées.
     * @return Matrice d'adjacence pour le graphe des tuiles du plateau en
     * considérant les terres arables durant la phase de découverte.
     */
    private boolean[][] matriceAdjacenceGrapheDecouverteTerresArables(final List<Point> coordLigColTuilesPosees) {
        final int nbCoordLigColTuilesPosees = coordLigColTuilesPosees.size();
        boolean[][] matriceAdjacenceGrapheDecouverteTerresArables = new boolean[nbCoordLigColTuilesPosees][nbCoordLigColTuilesPosees];
        for (int lig = 0; lig < nbCoordLigColTuilesPosees; ++lig) {
            for (int col = 0; col < nbCoordLigColTuilesPosees; ++col) {
                matriceAdjacenceGrapheDecouverteTerresArables[lig][col] = false;
            }
        }
        return matriceAdjacenceGrapheDecouverteTerresArables;
    }

    /**
     * Matrice d'adjacence pour le graphe des tuiles du plateau en considérant
     * les terres arables durant la phase de découverte.
     *
     * @param lig Ligne de la matrice.
     * @param col Colonne de la matrice.
     * @return Matrice d'adjacence pour le graphe des tuiles du plateau en
     * considérant les terres arables durant la phase de découverte.
     */
    private void setTrueMatriceAdjacenceGrapheDecouverteTerresArables(
            boolean[][] matriceAdjacenceGrapheDecouverteTerresArables, final int lig, final int col) {
        matriceAdjacenceGrapheDecouverteTerresArables[lig][col] = true;
        matriceAdjacenceGrapheDecouverteTerresArables[col][lig] = true;
    }

    /**
     * Test des coordonnées (ligne et colonne) des tuiles posées pour le graphe
     * des tuiles du plateau en considérant les terres arables durant la phase
     * de découverte.
     *
     * @param grapheDecouverteTerresArables Graphe des tuiles du plateau en considérant les terres arables
     *                                      durant la phase de découverte.
     * @param coordLigColTuilesPosees       Coordonnées (ligne et colonne) des tuiles posées.
     */
    private void testCoordLigColTuilesPoseesGrapheDecouverteTerresArables(
            final Map<Point, Map<Point, Boolean>> grapheDecouverteTerresArables,
            final List<Point> coordLigColTuilesPosees) {
        final int nbCoordLigColTuilesPosees = coordLigColTuilesPosees.size();
        assertEquals(nbCoordLigColTuilesPosees, grapheDecouverteTerresArables.size());
        for (Point coordLigCol1re : coordLigColTuilesPosees) {
            assertNotNull(grapheDecouverteTerresArables.get(coordLigCol1re));
            assertEquals(nbCoordLigColTuilesPosees, grapheDecouverteTerresArables.get(coordLigCol1re).size());
            for (Point coordLigCol2de : coordLigColTuilesPosees) {
                assertNotNull(grapheDecouverteTerresArables.get(coordLigCol1re).get(coordLigCol2de));
            }
        }
    }

    /**
     * Test de la symétrie du graphe des tuiles du plateau en considérant les
     * terres arables durant la phase de découverte.
     *
     * @param grapheDecouverteTerresArables Graphe des tuiles du plateau en considérant les terres arables
     *                                      durant la phase de découverte.
     */
    private void testSymetrieGrapheDecouverteTerresArables(
            final Map<Point, Map<Point, Boolean>> grapheDecouverteTerresArables) {
        for (Point coordLigCol1re : grapheDecouverteTerresArables.keySet()) {
            assertFalse(grapheDecouverteTerresArables.get(coordLigCol1re).get(coordLigCol1re));
            for (Point coordLigCol2de : grapheDecouverteTerresArables.get(coordLigCol1re).keySet()) {
                assertTrue(grapheDecouverteTerresArables.get(coordLigCol1re)
                        .get(coordLigCol2de) == grapheDecouverteTerresArables.get(coordLigCol2de).get(coordLigCol1re));
            }
        }
    }

}
