/**
 * Tests des pions.
 */
package fjord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;


import java.util.Arrays;

/**
 * Tests des pions.
 * 
 * @author Olivier
 */
public class PionTest {

	/**
	 * Tests des étiquettes des pions.
	 */
	@Test
	public void testsEtiquettes() {
		// Nombre de pions.
		assertEquals(2, Pion.values().length);
		// Étiquettes des pions.
		assertTrue(Arrays.asList(Pion.values()).contains(Pion.HUTTE));
		assertTrue(Arrays.asList(Pion.values()).contains(Pion.HUTTE));
	}

	/**
	 * Tests des nombres d'exemplaires des pions.
	 */
	@Test
	public void testsNbExemplaires() {
		for (Pion pion : Pion.values()) {
			assertTrue(pion.getNbExemplaires() >= 0);
		}
	}

}
