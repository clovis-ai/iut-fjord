/**
 * Tests des terrains.
 */
package fjord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Tests des terrains.
 * 
 * @author Olivier
 */
public class TerrainTest {

	/**
	 * Tests des étiquettes des terrains.
	 */
	@Test
	public void testsEtiquettes() {
		// Nombre de terrains.
		assertEquals(3, Terrain.values().length);
		// Étiquettes des terrains.
		assertTrue(Arrays.asList(Terrain.values()).contains(Terrain.TERRES_ARABLES));
		assertTrue(Arrays.asList(Terrain.values()).contains(Terrain.MONTAGNE));
		assertTrue(Arrays.asList(Terrain.values()).contains(Terrain.MER));
	}

	/**
	 * Tests des noms des terrains.
	 */
	@Test
	public void testsNoms() {
		assertFalse(Terrain.TERRES_ARABLES.getNom().equals(Terrain.MER.getNom()));
		assertFalse(Terrain.TERRES_ARABLES.getNom().equals(Terrain.MONTAGNE.getNom()));
		assertFalse(Terrain.MER.getNom().equals(Terrain.MONTAGNE.getNom()));
	}

	/**
	 * Tests des codes des terrains.
	 */
	@Test
	public void testsCodes() {
		assertFalse(Terrain.TERRES_ARABLES.getCode() == Terrain.MER.getCode());
		assertFalse(Terrain.TERRES_ARABLES.getCode() == Terrain.MONTAGNE.getCode());
		assertFalse(Terrain.MER.getCode() == Terrain.MONTAGNE.getCode());
	}

	/**
	 * Tests des couleurs RGB des terrains.
	 */
	@Test
	public void testsRGB() {
		assertFalse(Terrain.TERRES_ARABLES.getRGB().getRGB() == Terrain.MER.getRGB().getRGB());
		assertFalse(Terrain.TERRES_ARABLES.getRGB().getRGB() == Terrain.MONTAGNE.getRGB().getRGB());
		assertFalse(Terrain.MER.getRGB().getRGB() == Terrain.MONTAGNE.getRGB().getRGB());
	}

	/**
	 * Tests des codes des terrains.
	 */
	@Test
	public void testsCodesTerrains() {
		// Teste si les valeurs sont correctes.
		assertTrue(Arrays.asList('A', 'B', 'C').equals(Terrain.codesTerrains()));
		// Teste l'utilisation de la fonctionnelle.
		List<Character> codesTerrains = new ArrayList<Character>();
		for (Terrain terrain : Terrain.values()) {
			codesTerrains.add(terrain.getCode());
		}
		assertEquals(codesTerrains.size(), Terrain.codesTerrains().size());
		codesTerrains.removeAll(Terrain.codesTerrains());
		assertTrue(codesTerrains.isEmpty());
	}

	/**
	 * Tests du terrain d'un code donné.
	 */
	@Test
	public void testsGetTerrain() {
		// Teste si les valeurs sont correctes.
		assertTrue(Terrain.TERRES_ARABLES == Terrain.getTerrain('A'));
		assertTrue(Terrain.MER == Terrain.getTerrain('B'));
		assertTrue(Terrain.MONTAGNE == Terrain.getTerrain('C'));
		assertNull(Terrain.getTerrain('z'));
		// Teste l'utilisation de la fonctionnelle.
		for (char code : Arrays.asList('A', 'B', 'C', 'z')) {
			boolean codeTerrainTrouve = false;
			for (Terrain terrain : Terrain.values()) {
				if (terrain.getCode() == code) {
					assertTrue(terrain.equals(Terrain.getTerrain(code)));
					codeTerrainTrouve = true;
				}
			}
			if (!codeTerrainTrouve) {
				assertNull(Terrain.getTerrain(code));
			}
		}
	}

}
