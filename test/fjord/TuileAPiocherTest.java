/**
 * Tests des pioches.
 */
package fjord;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;


/**
 * Tests des tuiles à piocher.
 * 
 * @author Olivier
 */
public class TuileAPiocherTest {

	/**
	 * Tests des tuiles à piocher.
	 */
	@Test
	public void tests() {
		TuileAPiocher tuileAPiocher;
		// Tuile face cachée.
		tuileAPiocher = new TuileAPiocher(Tuile.AAAAAA, true);
		assertTrue(tuileAPiocher.estCachee());
		tuileAPiocher.setVisible();
		assertFalse(tuileAPiocher.estCachee());
		// Tuile face visible.
		tuileAPiocher = new TuileAPiocher(Tuile.AAAAAB, false);
		assertFalse(tuileAPiocher.estCachee());
		tuileAPiocher.setVisible();
		assertFalse(tuileAPiocher.estCachee());
		// Erreur car pas de tuile.
		try {
			tuileAPiocher = new TuileAPiocher(null, true);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
	}

}
