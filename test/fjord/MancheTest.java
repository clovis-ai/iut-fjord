/**
 * Tests des manches.
 */
package fjord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.awt.Point;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;


/**
 * Tests des manches.
 * 
 * @author Olivier
 */
public class MancheTest {

	/**
	 * Tests des manches.
	 */
	@Test
	public void tests() {
		// Erreur car pas de partie en cours.
		try {
			new Manche(null, new Joueur(Couleur.FONCE));
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Erreur car pas de premier joueur.
		try {
			new Manche(new Partie(), null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Manche.
		Manche manche = new Manche(new Partie(), new Joueur(Couleur.FONCE));
		assertNotNull(manche.getPlateau());
		assertNull(manche.getPointsVictoire());
		assertNull(manche.getJoueurGagnant());
	}

	/**
	 * Tests du nombre de points de victoire des joueurs et le joueur gagnant de
	 * la manche.
	 */
	@Test
	public void testsJoueurGagnantPointsVictoire() {
		Partie partie = new Partie();
		Joueur premierJoueur = partie.getJoueurs().get(0);
		Joueur secondJoueur = partie.getJoueurs().get(1);
		Manche manche = new Manche(partie, premierJoueur);
		// Aucune tuile.
		manche.toursJeuFinScores();
		testToursJeuFinScores(manche);
		assertTrue(manche.getPointsVictoire().isEmpty());
		assertNull(manche.getJoueurGagnant());
		// 1re tuile.
		manche.getPlateau().poserTuile(new Point(1, 1), TuileOrientee.AAAAAA, null, null);
		manche.toursJeuFinScores();
		testToursJeuFinScores(manche);
		assertTrue(manche.getPointsVictoire().isEmpty());
		assertNull(manche.getJoueurGagnant());
		// 2e tuile.
		manche.getPlateau().poserTuile(new Point(2, 2), TuileOrientee.AAAAAA, premierJoueur, Pion.HUTTE);
		manche.toursJeuFinScores();
		testToursJeuFinScores(manche);
		assertTrue(manche.getPointsVictoire().isEmpty());
		assertNull(manche.getJoueurGagnant());
		// 3e tuile.
		manche.getPlateau().poserTuile(new Point(3, 3), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche.toursJeuFinScores();
		testToursJeuFinScores(manche);
		assertEquals(1, manche.getPointsVictoire().size());
		assertEquals(1, (int) manche.getPointsVictoire().get(premierJoueur));
		assertTrue(manche.getJoueurGagnant().equals(premierJoueur));
		// 4e tuile.
		manche.getPlateau().poserTuile(new Point(4, 4), TuileOrientee.AAAAAA, secondJoueur, Pion.HUTTE);
		manche.toursJeuFinScores();
		testToursJeuFinScores(manche);
		assertEquals(1, manche.getPointsVictoire().size());
		assertEquals(1, (int) manche.getPointsVictoire().get(premierJoueur));
		assertTrue(manche.getJoueurGagnant().equals(premierJoueur));
		// 5e tuile.
		manche.getPlateau().poserTuile(new Point(5, 5), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche.toursJeuFinScores();
		testToursJeuFinScores(manche);
		assertEquals(2, manche.getPointsVictoire().size());
		assertEquals(1, (int) manche.getPointsVictoire().get(premierJoueur));
		assertEquals(1, (int) manche.getPointsVictoire().get(secondJoueur));
		assertNull(manche.getJoueurGagnant());
		// 6e tuile.
		manche.getPlateau().poserTuile(new Point(6, 6), TuileOrientee.AAAAAA, secondJoueur, Pion.HUTTE);
		manche.toursJeuFinScores();
		testToursJeuFinScores(manche);
		assertEquals(2, manche.getPointsVictoire().size());
		assertEquals(1, (int) manche.getPointsVictoire().get(premierJoueur));
		assertEquals(1, (int) manche.getPointsVictoire().get(secondJoueur));
		assertNull(manche.getJoueurGagnant());
		// 7e tuile.
		manche.getPlateau().poserTuile(new Point(7, 7), TuileOrientee.AAAAAA, secondJoueur, Pion.CHAMP);
		manche.toursJeuFinScores();
		testToursJeuFinScores(manche);
		assertEquals(2, manche.getPointsVictoire().size());
		assertEquals(1, (int) manche.getPointsVictoire().get(premierJoueur));
		assertEquals(2, (int) manche.getPointsVictoire().get(secondJoueur));
		assertTrue(manche.getJoueurGagnant().equals(secondJoueur));
		// 8e tuile.
		manche.getPlateau().poserTuile(new Point(8, 8), TuileOrientee.AAAAAA, premierJoueur, Pion.HUTTE);
		manche.toursJeuFinScores();
		testToursJeuFinScores(manche);
		assertEquals(2, manche.getPointsVictoire().size());
		assertEquals(1, (int) manche.getPointsVictoire().get(premierJoueur));
		assertEquals(2, (int) manche.getPointsVictoire().get(secondJoueur));
		assertTrue(manche.getJoueurGagnant().equals(secondJoueur));
		// 9e tuile.
		manche.getPlateau().poserTuile(new Point(9, 9), TuileOrientee.AAAAAA, premierJoueur, Pion.CHAMP);
		manche.toursJeuFinScores();
		testToursJeuFinScores(manche);
		assertEquals(2, manche.getPointsVictoire().size());
		assertEquals(2, (int) manche.getPointsVictoire().get(premierJoueur));
		assertEquals(2, (int) manche.getPointsVictoire().get(secondJoueur));
		assertNull(manche.getJoueurGagnant());
	}

	/**
	 * Tests de l'utilisation de la fonctionnelle pour le calcul des scores en
	 * fin de la manche.
	 * 
	 * @param manche
	 *            Manche dont le calcul des scores en fin de la manche a été
	 *            effectué.
	 */
	private void testToursJeuFinScores(final Manche manche) {
		Map<Joueur, Integer> pointsVictoire = new HashMap<Joueur, Integer>();
		for (Case c : manche.getPlateau().getCases().values()) {
			// manche.getPlateau().nbCasesOccupees(joueur, Pion.CHAMP)
			Joueur joueur = c.getJoueur();
			if (joueur != null && c.getPion().equals(Pion.CHAMP)) {
				pointsVictoire.put(joueur, pointsVictoire.getOrDefault(joueur, 0) + 1);
			}
		}
		assertEquals(pointsVictoire.size(), manche.getPointsVictoire().size());
		for (Entry<Joueur, Integer> joueurPointsVictoire : manche.getPointsVictoire().entrySet()) {
			assertNotNull(pointsVictoire.get(joueurPointsVictoire.getKey()));
			assertTrue(pointsVictoire.get(joueurPointsVictoire.getKey()).equals(joueurPointsVictoire.getValue()));
		}
	}

}
