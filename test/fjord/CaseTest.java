/**
 * Tests des cases du plateau.
 */
package fjord;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;


/**
 * Tests des cases du plateau.
 * 
 * @author Olivier
 */
public class CaseTest {

	/**
	 * Tests des cases du plateau.
	 */
	@Test
	public void tests() {
		Case c;
		Joueur joueur = new Joueur(Couleur.FONCE);
		// Case inoccupée.
		c = new Case(TuileOrientee.AAAAAA, null, null);
		assertTrue(c.estInoccupee());
		assertTrue(c.getJoueur() == null && c.getPion() == null);
		// Case occupée.
		c = new Case(TuileOrientee.AAAAAB, joueur, Pion.HUTTE);
		assertFalse(c.estInoccupee());
		assertTrue(c.getJoueur() != null && c.getPion() != null);
		// Erreur car pas de tuile orientée.
		try {
			c = new Case(null, null, null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Erreur car pion sans joueur.
		try {
			c = new Case(TuileOrientee.AAAAAC, null, Pion.CHAMP);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Erreur car joueur sans pion.
		try {
			c = new Case(TuileOrientee.AAAAAC, joueur, null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
	}

}
