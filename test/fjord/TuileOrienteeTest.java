/**
 * Tests des tuiles orientées.
 */
package fjord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;


/**
 * Tests des tuiles orientées.
 * 
 * @author Olivier
 */
public class TuileOrienteeTest {

	/**
	 * Tests des étiquettes des tuiles orientées.
	 */
	@Test
	public void testsEtiquettesTuilesOrientees() {
		// Nombre de tuiles orientées.
		assertEquals(107, TuileOrientee.values().length);
		// Étiquettes des tuiles orientées.
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAAAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAAAAB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAAAAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAAABA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAAABB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAAACA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAAACC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAABAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAABBA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAABBB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAACAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAACAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAACCA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AAACCC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AABAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AABAAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AABBAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AABBAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AABBBA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AABBBB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AACAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AACAAB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AACACA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AACACC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AACCAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AACCAB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AACCCA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.AACCCC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ABAAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ABAACA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ABAACC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ABBAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ABBACA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ABBACC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ABBBAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ABBBAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ABBBBA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ABBBBB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACAAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACAAAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACAABA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACAABB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACABBB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACACAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACACCA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACCAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACCAAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACCABA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACCABB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACCACC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACCCAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACCCCA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.ACCCCC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BAAAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BAAAAB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BAAABB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BAABBB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BAACAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BAACCA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BABBBB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BACAAB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BACABB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BACCAB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBAAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBAAAB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBAABB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBABBB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBACAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBACAB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBACCA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBBAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBBAAB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBBABB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBBACA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBBBAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBBBAB));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.BBBBBA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CAAAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CAAAAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CAAACA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CAAACC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CAABAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CAABBA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CAACAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CAACCC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CABAAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CABBAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CABBBA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CACAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CACCAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CACCAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CACCCC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCAAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCAAAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCAACA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCAACC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCABAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCABBA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCACCA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCACCC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCCAAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCCAAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCCACC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCCCAA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCCCAC));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCCCCA));
		assertTrue(Arrays.asList(TuileOrientee.values()).contains(TuileOrientee.CCCCCC));
		// Étiquettes toutes de la bonne longueur.
		for (TuileOrientee tuileOrientee : TuileOrientee.values()) {
			assertEquals(Direction.NB_DIRECTIONS, tuileOrientee.toString().length());
		}
		// Étiquettes toutes sur l'alphabet des codes des terrains.
		for (TuileOrientee tuileOrientee : TuileOrientee.values()) {
			for (char codeTerrain : tuileOrientee.toString().toCharArray()) {
				assertTrue(Terrain.codesTerrains().contains(codeTerrain));
			}
		}
	}

	/**
	 * Tests de cohérence entre les tuiles orientées et les tuiles.
	 */
	@Test
	public void testsCoherenceTuiles() {
		// Conjugaison.
		for (TuileOrientee tuileOrientee : TuileOrientee.values()) {
			assertNotNull(tuileOrientee.getTuileBase());
			assertTrue(tuileOrientee.toString().equals(
					new String(tuileOrientee.getTuileBase().toString().substring(tuileOrientee.getLgPrefixeBase())
							+ tuileOrientee.getTuileBase().toString().substring(0, tuileOrientee.getLgPrefixeBase()))));
		}
	}

	/**
	 * Tests de la longueur du préfixe du mot correspondant à la tuile de base
	 * pour obtenir par conjugaison la tuile orientée.
	 */
	@Test
	public void testsLgPrefixeBase() {
		for (TuileOrientee tuileOrientee : TuileOrientee.values()) {
			assertTrue(tuileOrientee.getLgPrefixeBase() >= 0);
			assertTrue(tuileOrientee.getLgPrefixeBase() < Direction.NB_DIRECTIONS);
		}
	}

	/**
	 * Tests du codage dans la direction.
	 */
	@Test
	public void testsCodageDirection() {
		// Erreur car pas de direction.
		try {
			TuileOrientee.AAAAAA.getCodageDirection(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Renseigné par 2 caractères.
		for (TuileOrientee tuileOrientee : TuileOrientee.values()) {
			for (Direction direction : Direction.values()) {
				String codageDirection = tuileOrientee.getCodageDirection(direction);
				assertNotNull(codageDirection);
				assertEquals(2, codageDirection.length());
			}
		}
		TuileOrientee tuileOrienteeTestee;
		// Tuile orientée d'un seul terrain.
		for (Direction direction : Direction.values()) {
			assertTrue(TuileOrientee.AAAAAA.getCodageDirection(direction).equals("AA"));
			assertTrue(TuileOrientee.CCCCCC.getCodageDirection(direction).equals("CC"));
		}
		// Tuile orientée "AACCAB".
		tuileOrienteeTestee = TuileOrientee.AACCAB;
		assertTrue(tuileOrienteeTestee.getCodageDirection(Direction.DIRECTION_120_DEGRES).equals("AA"));
		assertTrue(tuileOrienteeTestee.getCodageDirection(Direction.DIRECTION_180_DEGRES).equals("AC"));
		assertTrue(tuileOrienteeTestee.getCodageDirection(Direction.DIRECTION_240_DEGRES).equals("CC"));
		assertTrue(tuileOrienteeTestee.getCodageDirection(Direction.DIRECTION_300_DEGRES).equals("CA"));
		assertTrue(tuileOrienteeTestee.getCodageDirection(Direction.DIRECTION_0_DEGRE).equals("AB"));
		assertTrue(tuileOrienteeTestee.getCodageDirection(Direction.DIRECTION_60_DEGRES).equals("BA"));
		// Tuile orientée "AACACC".
		tuileOrienteeTestee = TuileOrientee.AACACC;
		assertTrue(tuileOrienteeTestee.getCodageDirection(Direction.DIRECTION_120_DEGRES).equals("AA"));
		assertTrue(tuileOrienteeTestee.getCodageDirection(Direction.DIRECTION_180_DEGRES).equals("AC"));
		assertTrue(tuileOrienteeTestee.getCodageDirection(Direction.DIRECTION_240_DEGRES).equals("CA"));
		assertTrue(tuileOrienteeTestee.getCodageDirection(Direction.DIRECTION_300_DEGRES).equals("AC"));
		assertTrue(tuileOrienteeTestee.getCodageDirection(Direction.DIRECTION_0_DEGRE).equals("CC"));
		assertTrue(tuileOrienteeTestee.getCodageDirection(Direction.DIRECTION_60_DEGRES).equals("CA"));
	}

	/**
	 * Tests des tuiles orientées dont la direction contient ou non de la terre.
	 */
	@Test
	public void testsATerreDirection() {
		TuileOrientee tuileOrientee;
		// Erreur car pas de direction.
		try {
			TuileOrientee.AAAAAA.aTerreDirection(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Plusieurs tuiles orientées ayant de la terre, pour chaque direction.
		for (Direction direction : Direction.values()) {
			assertTrue(TuileOrientee.AAAAAA.aTerreDirection(direction));
			assertTrue(TuileOrientee.ABAAAA.aTerreDirection(direction));
			assertTrue(TuileOrientee.AAAAAB.aTerreDirection(direction));
			assertTrue(TuileOrientee.BAAAAA.aTerreDirection(direction));
			assertTrue(TuileOrientee.CCCAAA.aTerreDirection(direction));
			assertTrue(TuileOrientee.CAACAC.aTerreDirection(direction));
			assertTrue(TuileOrientee.CCCCCC.aTerreDirection(direction));
		}
		// Tuile orientée "AABBAA" ayant de la mer pouvant séparer la terre.
		tuileOrientee = TuileOrientee.AABBAA;
		assertTrue(tuileOrientee.aTerreDirection(Direction.DIRECTION_120_DEGRES));
		assertTrue(tuileOrientee.aTerreDirection(Direction.DIRECTION_180_DEGRES));
		assertFalse(tuileOrientee.aTerreDirection(Direction.DIRECTION_240_DEGRES));
		assertTrue(tuileOrientee.aTerreDirection(Direction.DIRECTION_300_DEGRES));
		assertTrue(tuileOrientee.aTerreDirection(Direction.DIRECTION_0_DEGRE));
		assertTrue(tuileOrientee.aTerreDirection(Direction.DIRECTION_60_DEGRES));
		// Tuile orientée "BBAAAA" ayant de la mer pouvant séparer la terre.
		tuileOrientee = TuileOrientee.BBAAAA;
		assertFalse(tuileOrientee.aTerreDirection(Direction.DIRECTION_120_DEGRES));
		assertTrue(tuileOrientee.aTerreDirection(Direction.DIRECTION_180_DEGRES));
		assertTrue(tuileOrientee.aTerreDirection(Direction.DIRECTION_60_DEGRES));
		// Tuile orientée "BBAAAB" ayant de la mer pouvant séparer la terre.
		tuileOrientee = TuileOrientee.BBAAAB;
		assertFalse(tuileOrientee.aTerreDirection(Direction.DIRECTION_120_DEGRES));
		assertTrue(tuileOrientee.aTerreDirection(Direction.DIRECTION_180_DEGRES));
		assertTrue(tuileOrientee.aTerreDirection(Direction.DIRECTION_0_DEGRE));
		assertFalse(tuileOrientee.aTerreDirection(Direction.DIRECTION_60_DEGRES));
		// Tuile orientée "BABBBB" ayant de la mer pouvant séparer la terre.
		tuileOrientee = TuileOrientee.BABBBB;
		assertTrue(tuileOrientee.aTerreDirection(Direction.DIRECTION_120_DEGRES));
		assertTrue(tuileOrientee.aTerreDirection(Direction.DIRECTION_180_DEGRES));
		assertFalse(tuileOrientee.aTerreDirection(Direction.DIRECTION_240_DEGRES));
		assertFalse(tuileOrientee.aTerreDirection(Direction.DIRECTION_300_DEGRES));
		assertFalse(tuileOrientee.aTerreDirection(Direction.DIRECTION_0_DEGRE));
		assertFalse(tuileOrientee.aTerreDirection(Direction.DIRECTION_60_DEGRES));
		// Teste l'utilisation de la fonctionnelle.
		for (TuileOrientee tuileOrienteeFctlle : TuileOrientee.values()) {
			for (Direction direction : Direction.values()) {
				boolean aTerreDirection = false;
				String codageDirection = tuileOrienteeFctlle.getCodageDirection(direction);
				for (char codeTerrain : codageDirection.toCharArray()) {
					if (Terrain.getTerrain(codeTerrain).estTerre()) {
						aTerreDirection = true;
					}
				}
				assertTrue(aTerreDirection == tuileOrienteeFctlle.aTerreDirection(direction));
			}
		}
	}

	/**
	 * Tests de l'installation d'un pion dans la direction.
	 */
	@Test
	public void testsInstallationPossibleDirection() {
		TuileOrientee tuileOrientee;
		// Erreur car pas de direction.
		try {
			TuileOrientee.AAAAAA.installationPossibleDirection(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Plusieurs tuiles orientées ayant des terres arables, pour chaque
		// direction.
		for (Direction direction : Direction.values()) {
			assertTrue(TuileOrientee.AAAAAA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.AAAAAB.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.AAAAAC.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.AAAABA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.AAAACA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.AAABAA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.AAACAA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.AAACAC.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.AABAAA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.AABAAC.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.AACAAA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.AACAAB.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.AACACA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.ABAAAA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.ABAACA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.ACAAAA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.ACAAAC.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.ACAABA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.ACACAA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.BAAAAA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.BAACAA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.CAAAAA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.CAAACA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.CAABAA.installationPossibleDirection(direction));
			assertTrue(TuileOrientee.CACAAA.installationPossibleDirection(direction));
		}
		// Tuile orientée "AABBAA" ayant de la mer pouvant séparer les terres
		// arables.
		tuileOrientee = TuileOrientee.AABBAA;
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_120_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_180_DEGRES));
		assertFalse(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_240_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_300_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_0_DEGRE));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_60_DEGRES));
		// Tuile orientée "AACCAA" ayant de la montagne pouvant séparer les
		// terres arables.
		tuileOrientee = TuileOrientee.AACCAA;
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_120_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_180_DEGRES));
		assertFalse(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_240_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_300_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_0_DEGRE));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_60_DEGRES));
		// Tuile orientée "BBAAAA" ayant de la mer pouvant séparer les terres
		// arables.
		tuileOrientee = TuileOrientee.BBAAAA;
		assertFalse(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_120_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_180_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_60_DEGRES));
		// Tuile orientée "CCAAAA" ayant de la montagne pouvant séparer les
		// terres arables.
		tuileOrientee = TuileOrientee.CCAAAA;
		assertFalse(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_120_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_180_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_60_DEGRES));
		// Tuile orientée "BAAAAB" ayant de la mer ou de la montagne pouvant
		// séparer les terres arables.
		tuileOrientee = TuileOrientee.BAAAAB;
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_120_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_180_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_0_DEGRE));
		assertFalse(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_60_DEGRES));
		// Tuile orientée "CAAAAC" ayant de la mer ou de la montagne pouvant
		// séparer les terres arables.
		tuileOrientee = TuileOrientee.CAAAAC;
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_120_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_180_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_0_DEGRE));
		assertFalse(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_60_DEGRES));
		// Tuile orientée "BABBBB" ayant de la mer et de la montagne pouvant
		// séparer les terres arables.
		tuileOrientee = TuileOrientee.ABBACC;
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_120_DEGRES));
		assertFalse(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_180_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_240_DEGRES));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_300_DEGRES));
		assertFalse(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_0_DEGRE));
		assertTrue(tuileOrientee.installationPossibleDirection(Direction.DIRECTION_60_DEGRES));
		// Teste l'utilisation de la fonctionnelle.
		for (TuileOrientee tuileOrienteeFctlle : TuileOrientee.values()) {
			for (Direction direction : Direction.values()) {
				boolean installationPossibleDirection = false;
				String codageDirection = tuileOrienteeFctlle.getCodageDirection(direction);
				for (char codeTerrain : codageDirection.toCharArray()) {
					if (Terrain.getTerrain(codeTerrain).estInstallationPossible()) {
						installationPossibleDirection = true;
					}
				}
				assertTrue(
						installationPossibleDirection == tuileOrienteeFctlle.installationPossibleDirection(direction));
			}
		}
	}

	/**
	 * Tests des tuiles orientées (adjacentes) dont les terrains correspondent,
	 * pour chaque direction.
	 */
	@Test
	public void testsTerrainCorrespondre() {
		TuileOrientee tuileOrienteeTestee;
		// Tuile orientée "AACCAB", pour chaque direction.
		tuileOrienteeTestee = TuileOrientee.AACCAB;
		assertTrue(tuileOrienteeTestee.terrainCorrespondre().get(Direction.DIRECTION_120_DEGRES)
				.equals(Arrays.asList(TuileOrientee.AAAAAA, TuileOrientee.AAAAAB, TuileOrientee.AAAAAC,
						TuileOrientee.AABAAA, TuileOrientee.AABAAC, TuileOrientee.AACAAA, TuileOrientee.AACAAB,
						TuileOrientee.ABAAAA, TuileOrientee.ABBAAA, TuileOrientee.ACAAAA, TuileOrientee.ACAAAC,
						TuileOrientee.ACCAAA, TuileOrientee.ACCAAC, TuileOrientee.BAAAAA, TuileOrientee.BAAAAB,
						TuileOrientee.BACAAB, TuileOrientee.BBAAAA, TuileOrientee.BBAAAB, TuileOrientee.BBBAAA,
						TuileOrientee.BBBAAB, TuileOrientee.CAAAAA, TuileOrientee.CAAAAC, TuileOrientee.CABAAC,
						TuileOrientee.CACAAA, TuileOrientee.CCAAAA, TuileOrientee.CCAAAC, TuileOrientee.CCCAAA,
						TuileOrientee.CCCAAC)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre().get(Direction.DIRECTION_180_DEGRES)
				.equals(Arrays.asList(TuileOrientee.AAAACA, TuileOrientee.AAACCA, TuileOrientee.AACACA,
						TuileOrientee.AACCCA, TuileOrientee.ABAACA, TuileOrientee.ABBACA, TuileOrientee.ACACCA,
						TuileOrientee.ACCCCA, TuileOrientee.BAACCA, TuileOrientee.BBACCA, TuileOrientee.BBBACA,
						TuileOrientee.CAAACA, TuileOrientee.CCAACA, TuileOrientee.CCACCA, TuileOrientee.CCCCCA)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre().get(Direction.DIRECTION_240_DEGRES)
				.equals(Arrays.asList(TuileOrientee.CAAAAC, TuileOrientee.CAAACC, TuileOrientee.CAACAC,
						TuileOrientee.CAACCC, TuileOrientee.CABAAC, TuileOrientee.CABBAC, TuileOrientee.CACCAC,
						TuileOrientee.CACCCC, TuileOrientee.CCAAAC, TuileOrientee.CCAACC, TuileOrientee.CCACCC,
						TuileOrientee.CCCAAC, TuileOrientee.CCCACC, TuileOrientee.CCCCAC, TuileOrientee.CCCCCC)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre().get(Direction.DIRECTION_300_DEGRES)
				.equals(Arrays.asList(TuileOrientee.ACAAAA, TuileOrientee.ACAAAC, TuileOrientee.ACAABA,
						TuileOrientee.ACAABB, TuileOrientee.ACABBB, TuileOrientee.ACACAA, TuileOrientee.ACACCA,
						TuileOrientee.ACCAAA, TuileOrientee.ACCAAC, TuileOrientee.ACCABA, TuileOrientee.ACCABB,
						TuileOrientee.ACCACC, TuileOrientee.ACCCAA, TuileOrientee.ACCCCA, TuileOrientee.ACCCCC)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre().get(Direction.DIRECTION_0_DEGRE)
				.equals(Arrays.asList(TuileOrientee.ABAAAA, TuileOrientee.ABAACA, TuileOrientee.ABAACC,
						TuileOrientee.BBAAAA, TuileOrientee.BBAAAB, TuileOrientee.BBAABB, TuileOrientee.BBABBB,
						TuileOrientee.BBACAA, TuileOrientee.BBACAB, TuileOrientee.BBACCA)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre().get(Direction.DIRECTION_60_DEGRES)
				.equals(Arrays.asList(TuileOrientee.AAABAA, TuileOrientee.AAABBA, TuileOrientee.AAABBB,
						TuileOrientee.ACABBB, TuileOrientee.BAABBB, TuileOrientee.BBABBB, TuileOrientee.CAABAA,
						TuileOrientee.CAABBA, TuileOrientee.CCABAA, TuileOrientee.CCABBA)));
		// Tuile orientée "AACACC", pour chaque direction.
		tuileOrienteeTestee = TuileOrientee.AACACC;
		assertTrue(tuileOrienteeTestee.terrainCorrespondre().get(Direction.DIRECTION_120_DEGRES)
				.equals(Arrays.asList(TuileOrientee.AAAAAA, TuileOrientee.AAAAAB, TuileOrientee.AAAAAC,
						TuileOrientee.AABAAA, TuileOrientee.AABAAC, TuileOrientee.AACAAA, TuileOrientee.AACAAB,
						TuileOrientee.ABAAAA, TuileOrientee.ABBAAA, TuileOrientee.ACAAAA, TuileOrientee.ACAAAC,
						TuileOrientee.ACCAAA, TuileOrientee.ACCAAC, TuileOrientee.BAAAAA, TuileOrientee.BAAAAB,
						TuileOrientee.BACAAB, TuileOrientee.BBAAAA, TuileOrientee.BBAAAB, TuileOrientee.BBBAAA,
						TuileOrientee.BBBAAB, TuileOrientee.CAAAAA, TuileOrientee.CAAAAC, TuileOrientee.CABAAC,
						TuileOrientee.CACAAA, TuileOrientee.CCAAAA, TuileOrientee.CCAAAC, TuileOrientee.CCCAAA,
						TuileOrientee.CCCAAC)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre().get(Direction.DIRECTION_180_DEGRES)
				.equals(Arrays.asList(TuileOrientee.AAAACA, TuileOrientee.AAACCA, TuileOrientee.AACACA,
						TuileOrientee.AACCCA, TuileOrientee.ABAACA, TuileOrientee.ABBACA, TuileOrientee.ACACCA,
						TuileOrientee.ACCCCA, TuileOrientee.BAACCA, TuileOrientee.BBACCA, TuileOrientee.BBBACA,
						TuileOrientee.CAAACA, TuileOrientee.CCAACA, TuileOrientee.CCACCA, TuileOrientee.CCCCCA)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre().get(Direction.DIRECTION_240_DEGRES)
				.equals(Arrays.asList(TuileOrientee.CAAAAA, TuileOrientee.CAAACA, TuileOrientee.CAABAA,
						TuileOrientee.CAABBA, TuileOrientee.CABBBA, TuileOrientee.CACAAA, TuileOrientee.CACCAA,
						TuileOrientee.CCAAAA, TuileOrientee.CCAACA, TuileOrientee.CCABAA, TuileOrientee.CCABBA,
						TuileOrientee.CCACCA, TuileOrientee.CCCAAA, TuileOrientee.CCCCAA, TuileOrientee.CCCCCA)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre().get(Direction.DIRECTION_300_DEGRES)
				.equals(Arrays.asList(TuileOrientee.CAAAAA, TuileOrientee.CAAAAC, TuileOrientee.CAAACA,
						TuileOrientee.CAAACC, TuileOrientee.CAABAA, TuileOrientee.CAABBA, TuileOrientee.CAACAC,
						TuileOrientee.CAACCC, TuileOrientee.CABAAC, TuileOrientee.CABBAC, TuileOrientee.CABBBA,
						TuileOrientee.CACAAA, TuileOrientee.CACCAA, TuileOrientee.CACCAC, TuileOrientee.CACCCC)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre().get(Direction.DIRECTION_0_DEGRE)
				.equals(Arrays.asList(TuileOrientee.ACCAAA, TuileOrientee.ACCAAC, TuileOrientee.ACCABA,
						TuileOrientee.ACCABB, TuileOrientee.ACCACC, TuileOrientee.ACCCAA, TuileOrientee.ACCCCA,
						TuileOrientee.ACCCCC, TuileOrientee.CCCAAA, TuileOrientee.CCCAAC, TuileOrientee.CCCACC,
						TuileOrientee.CCCCAA, TuileOrientee.CCCCAC, TuileOrientee.CCCCCA, TuileOrientee.CCCCCC)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre().get(Direction.DIRECTION_60_DEGRES)
				.equals(Arrays.asList(TuileOrientee.AAACAA, TuileOrientee.AAACAC, TuileOrientee.AAACCA,
						TuileOrientee.AAACCC, TuileOrientee.ACACAA, TuileOrientee.ACACCA, TuileOrientee.BAACAA,
						TuileOrientee.BAACCA, TuileOrientee.BBACAA, TuileOrientee.BBACAB, TuileOrientee.BBACCA,
						TuileOrientee.CAACAC, TuileOrientee.CAACCC, TuileOrientee.CCACCA, TuileOrientee.CCACCC)));
		// Teste l'utilisation de la fonctionnelle.
		for (TuileOrientee tuileOrienteeFctlle : TuileOrientee.values()) {
			for (Direction direction : Direction.values()) {
				List<TuileOrientee> terrainCorrespondre = new ArrayList<TuileOrientee>();
				for (TuileOrientee tuileOrienteeFctlleACmp : TuileOrientee.values()) {
					if (tuileOrienteeFctlle.getCodageDirection(direction)
							.equals(tuileOrienteeFctlleACmp.getCodageInverseDirection(direction.directionOpposee()))) {
						terrainCorrespondre.add(tuileOrienteeFctlleACmp);
					}
				}
				assertEquals(terrainCorrespondre.size(),
						tuileOrienteeFctlle.terrainCorrespondre().get(direction).size());
				terrainCorrespondre.removeAll(tuileOrienteeFctlle.terrainCorrespondre().get(direction));
				assertTrue(terrainCorrespondre.isEmpty());
			}
		}
	}

	/**
	 * Tests des tuiles orientées (adjacentes) dont les terrains correspondent,
	 * pour une seule direction.
	 */
	@Test
	public void testsTerrainCorrespondre1Direction() {
		// Erreur car pas de direction.
		try {
			TuileOrientee.AAAAAA.terrainCorrespondre(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Test pour la tuile orientée "AAAAAA" et la direction 180°.
		assertTrue(TuileOrientee.AAAAAA.terrainCorrespondre(Direction.DIRECTION_180_DEGRES)
				.equals(Arrays.asList(TuileOrientee.AAAAAA, TuileOrientee.AAABAA, TuileOrientee.AAACAA,
						TuileOrientee.AABAAA, TuileOrientee.AABBAA, TuileOrientee.AACAAA, TuileOrientee.AACCAA,
						TuileOrientee.ABAAAA, TuileOrientee.ABBAAA, TuileOrientee.ABBBAA, TuileOrientee.ACAAAA,
						TuileOrientee.ACACAA, TuileOrientee.ACCAAA, TuileOrientee.ACCCAA, TuileOrientee.BAAAAA,
						TuileOrientee.BAACAA, TuileOrientee.BBAAAA, TuileOrientee.BBACAA, TuileOrientee.BBBAAA,
						TuileOrientee.BBBBAA, TuileOrientee.CAAAAA, TuileOrientee.CAABAA, TuileOrientee.CACAAA,
						TuileOrientee.CACCAA, TuileOrientee.CCAAAA, TuileOrientee.CCABAA, TuileOrientee.CCCAAA,
						TuileOrientee.CCCCAA)));
		// Test pour la tuile orientée "BBACAB" et toutes les directions.
		TuileOrientee tuileOrienteeTestee = TuileOrientee.BBACAB;
		assertTrue(tuileOrienteeTestee.terrainCorrespondre(Direction.DIRECTION_120_DEGRES)
				.equals(Arrays.asList(TuileOrientee.AAABBA, TuileOrientee.AAABBB, TuileOrientee.AABBBA,
						TuileOrientee.AABBBB, TuileOrientee.ABBBBA, TuileOrientee.ABBBBB, TuileOrientee.ACABBB,
						TuileOrientee.BAABBB, TuileOrientee.BABBBB, TuileOrientee.BBABBB, TuileOrientee.BBBBBA,
						TuileOrientee.CAABBA, TuileOrientee.CABBBA, TuileOrientee.CCABBA)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre(Direction.DIRECTION_180_DEGRES)
				.equals(Arrays.asList(TuileOrientee.AAAAAB, TuileOrientee.AACAAB, TuileOrientee.AACCAB,
						TuileOrientee.BAAAAB, TuileOrientee.BACAAB, TuileOrientee.BACCAB, TuileOrientee.BBAAAB,
						TuileOrientee.BBACAB, TuileOrientee.BBBAAB, TuileOrientee.BBBBAB)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre(Direction.DIRECTION_240_DEGRES)
				.equals(Arrays.asList(TuileOrientee.AAAAAC, TuileOrientee.AAAACC, TuileOrientee.AAACAC,
						TuileOrientee.AAACCC, TuileOrientee.AABAAC, TuileOrientee.AABBAC, TuileOrientee.AACACC,
						TuileOrientee.AACCCC, TuileOrientee.ABAACC, TuileOrientee.ABBACC, TuileOrientee.ABBBAC,
						TuileOrientee.ACAAAC, TuileOrientee.ACCAAC, TuileOrientee.ACCACC, TuileOrientee.ACCCCC)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre(Direction.DIRECTION_300_DEGRES)
				.equals(Arrays.asList(TuileOrientee.ACAAAA, TuileOrientee.ACAAAC, TuileOrientee.ACAABA,
						TuileOrientee.ACAABB, TuileOrientee.ACABBB, TuileOrientee.ACACAA, TuileOrientee.ACACCA,
						TuileOrientee.ACCAAA, TuileOrientee.ACCAAC, TuileOrientee.ACCABA, TuileOrientee.ACCABB,
						TuileOrientee.ACCACC, TuileOrientee.ACCCAA, TuileOrientee.ACCCCA, TuileOrientee.ACCCCC)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre(Direction.DIRECTION_0_DEGRE)
				.equals(Arrays.asList(TuileOrientee.ABAAAA, TuileOrientee.ABAACA, TuileOrientee.ABAACC,
						TuileOrientee.BBAAAA, TuileOrientee.BBAAAB, TuileOrientee.BBAABB, TuileOrientee.BBABBB,
						TuileOrientee.BBACAA, TuileOrientee.BBACAB, TuileOrientee.BBACCA)));
		assertTrue(tuileOrienteeTestee.terrainCorrespondre(Direction.DIRECTION_60_DEGRES)
				.equals(Arrays.asList(TuileOrientee.AABBAA, TuileOrientee.AABBAC, TuileOrientee.AABBBA,
						TuileOrientee.AABBBB, TuileOrientee.ABBBAA, TuileOrientee.ABBBAC, TuileOrientee.ABBBBA,
						TuileOrientee.ABBBBB, TuileOrientee.BABBBB, TuileOrientee.BBBBAA, TuileOrientee.BBBBAB,
						TuileOrientee.BBBBBA, TuileOrientee.CABBAC, TuileOrientee.CABBBA)));
		// On ne teste rien de plus car plusieurs cas de tests pour une
		// direction ont été déjà testés pour toutes les directions.
	}

}
