/**
 * Tests des tuiles.
 */
package fjord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.TreeSet;

import org.junit.Test;


/**
 * Tests des tuiles.
 * 
 * @author Olivier
 */
public class TuileTest {

	/**
	 * Tests des étiquettes des tuiles.
	 */
	@Test
	public void testsEtiquettes() {
		// Nombre de tuiles.
		assertEquals(20, Tuile.values().length);
		// Étiquettes des tuiles.
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AAAAAA));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AAAAAB));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AAAAAC));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AAAABB));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AAAACC));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AAABBB));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AAACAC));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AAACCC));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AABAAC));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AABBAC));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AABBBB));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AACACC));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AACCAB));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.AACCCC));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.ABBACC));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.ABBBAC));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.ABBBBB));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.ACCACC));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.ACCCCC));
		assertTrue(Arrays.asList(Tuile.values()).contains(Tuile.CCCCCC));
		// Étiquettes toutes de la bonne longueur.
		for (Tuile tuile : Tuile.values()) {
			assertEquals(Direction.NB_DIRECTIONS, tuile.toString().length());
		}
		// Étiquettes toutes sur l'alphabet des codes des terrains.
		for (Tuile tuile : Tuile.values()) {
			for (char codeTerrain : tuile.toString().toCharArray()) {
				assertTrue(Terrain.codesTerrains().contains(codeTerrain));
			}
		}
	}

	/**
	 * Tests de cohérence entre les tuiles et les tuiles orientées.
	 */
	@Test
	public void testsCoherenceTuilesOrientees() {
		int nbTuilesOrienteesCalculees = 0;
		// Conjugaison.
		for (Tuile tuile : Tuile.values()) {
			TreeSet<String> tuilesOrienteesCalculees = new TreeSet<String>();
			for (int cptDirection = 0; cptDirection < Direction.NB_DIRECTIONS; ++cptDirection) {
				tuilesOrienteesCalculees.add(new String(
						tuile.toString().substring(cptDirection) + tuile.toString().substring(0, cptDirection)));
			}
			nbTuilesOrienteesCalculees += tuilesOrienteesCalculees.size();
			for (String tuileOrienteeCalculee : tuilesOrienteesCalculees) {
				try {
					TuileOrientee.valueOf(tuileOrienteeCalculee);
				} catch (IllegalArgumentException e) {
					assertFalse(true);
				} catch (Exception e) {
					assertFalse(true);
				}
			}
		}
		// Nombre de tuiles orientées obtenues par conjuguaison de toutes les
		// tuiles.
		assertEquals(nbTuilesOrienteesCalculees, TuileOrientee.values().length);
	}

	/**
	 * Tests des nombres d'exemplaires.
	 */
	@Test
	public void testsNbExemplaires() {
		int totalNbExemplaires = 0;
		for (Tuile tuile : Tuile.values()) {
			assertTrue(tuile.getNbExemplaires() >= 1);
			totalNbExemplaires += tuile.getNbExemplaires();
		}
		assertEquals(Tuile.TOTAL_NB_EXEMPLAIRES, totalNbExemplaires);
	}

	/**
	 * Tests de la possibilité de l'installation d'un pion.
	 */
	@Test
	public void testsInstallationPossible() {
		// Teste si les valeurs sont correctes.
		for (Tuile tuile : Tuile.values()) {
			// installation possible <==> NON Tuile.CCCCCC
			assertTrue(tuile.equals(Tuile.CCCCCC) ? !tuile.estInstallationPossible() : tuile.estInstallationPossible());
		}
		// Teste l'utilisation de la fonctionnelle.
		for (Tuile tuile : Tuile.values()) {
			boolean estInstallationPossible = false;
			for (char codeTerrain : tuile.toString().toCharArray()) {
				if (Terrain.getTerrain(codeTerrain).estInstallationPossible()) {
					estInstallationPossible = true;
				}
			}
			assertTrue(estInstallationPossible == tuile.estInstallationPossible());
		}
	}

}
