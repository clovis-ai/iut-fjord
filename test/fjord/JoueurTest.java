/**
 * Tests des joueurs.
 */
package fjord;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Point;
import java.util.ArrayList;

import org.junit.Test;


/**
 * Tests des joueurs.
 * 
 * @author Olivier
 */
public class JoueurTest {

	/**
	 * Tests des joueurs.
	 */
	@Test
	public void tests() {
		Joueur joueur;
		Joueur autreJoueur;
		// Joueur.
		joueur = new Joueur(Couleur.FONCE);
		// Autre joueur (de même couleur !).
		autreJoueur = new Joueur(Couleur.FONCE);
		new Case(TuileOrientee.AAAAAA, null, null);
		new Case(TuileOrientee.AAAAAB, joueur, Pion.HUTTE);
		new Case(TuileOrientee.AAAAAC, joueur, Pion.CHAMP);
		new Case(TuileOrientee.AAAABA, autreJoueur, Pion.HUTTE);
		new Case(TuileOrientee.AAAABB, autreJoueur, Pion.CHAMP);
		// Erreur car pas de couleur.
		try {
			joueur = new Joueur(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
	}

	/**
	 * Tests, lors de la phase de découverte de la manche, joue une tuile soit
	 * en la choisissant parmi celles de la pioche des tuiles faces visibles qui
	 * peuvent être posées, soit demandant qu'elle soit tirée aléatoirement
	 * parmi celles de la pioche des tuiles faces cachées.
	 */
	@Test
	public void testsJouerDecouvertePioche() {
		Joueur joueur = new Joueur(Couleur.FONCE);
		// Erreur car pas de pioche.
		try {
			joueur.jouerDecouvertePioche(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Erreur car pioche vide.
		try {
			joueur.jouerDecouvertePioche(new ArrayList<Tuile>());
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Rien de plus à tester !
	}

	/**
	 * Tests, lors de la phase de découverte de la manche, joue une coordonnée
	 * (ligne et colonne) où poser la tuile piochée.
	 */
	@Test
	public void testsJouerDecouvertePoseCoordLigCol() {
		Joueur joueur = new Joueur(Couleur.FONCE);
		// Erreur car pas de coordonnées.
		try {
			joueur.jouerDecouvertePoseCoordLigCol(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Erreur car coordonnées vide.
		try {
			joueur.jouerDecouvertePoseCoordLigCol(new ArrayList<Point>());
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Rien de plus à tester !
	}

	/**
	 * Tests, lors de la phase de découverte de la manche, joue une tuile
	 * orientée à poser pour la tuile piochée.
	 */
	@Test
	public void testsJouerDecouvertePoseTuileOrientee() {
		Joueur joueur = new Joueur(Couleur.FONCE);
		// Erreur car pas de tuiles orientées.
		try {
			joueur.jouerDecouvertePoseTuileOrientee(null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Erreur car tuiles orientées vide.
		try {
			joueur.jouerDecouvertePoseTuileOrientee(new ArrayList<TuileOrientee>());
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Rien de plus à tester !
	}

	/**
	 * Tests, lors de la phase de découverte de la manche, indique si une hutte
	 * doit être installée sur la tuile piochée et posée.
	 */
	@Test
	public void testsJouerDecouverteInstallationHutte() {
		// Rien à tester !
	}

	/**
	 * Tests, lors de la phase de colonisation de la manche, joue une coordonnée
	 * (ligne et colonne) d'une case où installer un champ.
	 */
	@Test
	public void testsJouerColonisationInstallationChamp() {
		Joueur joueur = new Joueur(Couleur.FONCE);
		// Erreur car pas de coordonnées.
		try {
			joueur.jouerColonisationInstallationChamp(null, null, null, null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Erreur car coordonnées (pour le joueur) vide.
		try {
			joueur.jouerColonisationInstallationChamp(new ArrayList<Point>(), null, null, null);
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(true);
		}
		// Rien de plus à tester !
	}

}
