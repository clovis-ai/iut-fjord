/**
 * Plateau.
 * 
 * Partie développée par les étudiants.
 */
package fjord;

import static fjord.Couleur.CLAIR;
import static fjord.Pion.CHAMP;
import java.awt.Point;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

/**
 * Plateau.
 * 
 * @author Ivan Canet
 */
public class Plateau_ETD extends Plateau {

    /**
     * Parcours les case du plateau a la recherche des terres arables pour pouvoir creer un matrice
     * d'adjacence du graph
     * @return matrice d'adjacence du plateau en prenant en compte les terre Arables.
     * @author Maxime Cots
     */
	@Override
	public Map<Point, Map<Point, Boolean>> grapheDecouverteTerresArables() {

        Map<Point, Map<Point, Boolean>> grapheDecouverteTerresArables = new HashMap<Point, Map<Point, Boolean>>();
        Map<Point, Case> mapCases = getCases();
        boolean liaisonPossible;
        Direction direction;

        for (Point currentPoint : mapCases.keySet()) {

            Map<Point, Boolean> estLiee = new HashMap<>();
            for (Map.Entry<Point, Case> newPoint : mapCases.entrySet()) {

                liaisonPossible = false;

                if (!currentPoint.equals(newPoint.getKey()) && (estVoisin(currentPoint, newPoint.getKey()))) {

                    direction = Direction.direction2CasesAdjacentes(currentPoint, newPoint.getKey());
                    if (direction != null) {
                        liaisonPossible = mapCases.get(currentPoint).getTuileOrientee().installationPossibleDirection(direction);
                    }
                }

                estLiee.put(newPoint.getKey(), liaisonPossible);
            }
            grapheDecouverteTerresArables.put(currentPoint, estLiee);
        }
        return grapheDecouverteTerresArables;
    }

    /**
     * Verifie le voisinage des tuiles en fonction de leurs coordonnées
     * @param cP premiere tuile
     * @param nP deuxieme tuile
     * @return voisin ou pas
     * @author Maxime Cots
     */
    private boolean estVoisin(Point cP, Point nP){
	    return (cP.getX() + 1 == nP.getX() || cP.getX() -1 == nP.getX()) ||
                (cP.getY() +1 == nP.getY() || cP.getY() -1 == nP.getY());
    }

        /**
         * Génère le fichier .gv (format Graphviz) du graphe des pions reliés
	 * entre eux, de chaque couleur, du plateau en considérant les terres
	 * arables, au cours de la phase de colonisation.
         * 
         * <p>Le fichier est généré en utilisant les conventions suivantes :
         * <ul>
         *  <li>Les huttes sont représentées par des pentagones (foncés ou 
         * clairs)</li>
         *  <li>Les pions sont représentés par des disques (foncés ou 
         * clairs)</li>
         *  <li>Les cases inoccupées où pourraient être installé un pion sont 
         * représentées par des rectangles verts</li>
         *  <li>Les cases inoccupées où ne pourraient pas être installées un 
         * pion sont représentées par un triangle gris.</li>
         *  <li>Chaque tuile dans le graphe comporte ses coordonnées.</li>
         * </ul>
         * @author Ivan Canet
         */
	@Override
	public void exportGvColonisation() {
            File path = new File("fjordcc.gv");
            FileWriter f;
            try {
                f = new FileWriter(path);
            } catch (IOException ex) {
                throw new NullPointerException("Le fichier ne peut pas être "
                        + "ouvert: " + path.getAbsolutePath() + ":" 
                        + ex.getMessage());
            }
            
            try{
                // Ajouter le header
                f.write(GraphvizUtil.getHeader() + "\n");
                
                // Ajouter les sommets
                for(Entry<Point, Case> e : getCases().entrySet()){
                    f.write(GraphvizUtil.createSommet(
                            e.getKey(), 
                            e.getValue()) 
                            + "\n");
                }
                
                // Ajouter les relations dans un Set (pas de doublons)
                Set<Relation<Point>> relations = new HashSet<>();
                Map<Point, Map<Point, Boolean>> matrice = grapheDecouverteTerresArables();
                for(Entry<Point, Map<Point, Boolean>> e : matrice.entrySet())
                    for(Entry<Point, Boolean> voisins : e.getValue().entrySet())
                        if(voisins.getValue() && getCase(e.getKey()).getJoueur() == getCase(voisins.getKey()).getJoueur())
                            relations.add(
                                    new Relation<>(
                                            e.getKey(), 
                                            voisins.getKey(),
                                            getCase(e.getKey()).getJoueur())
                            );
                
                // Ajouter les relations dans le fichier
                for(Relation<Point> r : relations)
                    f.write(GraphvizUtil.createRelation(r, r.getJoueur()) + "\n");
                    
                // Ajouter le footer
                f.write(GraphvizUtil.getFooter() + "\n");
                
                // Fermer le fichier
                f.close();
            } catch (IOException ex){
                throw new RuntimeException("Erreur d'écriture du fichier: "
                        + path.getAbsolutePath() + ":" + ex.getMessage());
            }
	}
        
        /**
         * Une classe utilisée pour générer les différentes parties du fichier
         * {@code GRAPHVIZ}.
         * @author Ivan Canet
         */
        public static class GraphvizUtil {
            
            /**
             * Permet d'obtenir le header d'un fichier de format {@code GRAPHVIZ}.
             * @return Le header.
             * @author Ivan Canet
             */
            public static String getHeader(){
                return "graph \"Fjord colonisation\" {\n"
                    + "\tgraph [fontsize=12, size=\"6,6\"];\n"
                    + "\tnode [shape=box, regular=1, color=black, style=filled];\n"
                    + "\tedge [arrowhead=normal, arrowtail=normal, color=black, "
                        + "len=25.0, penwidth=8.0];\n";
            }
            
            /**
             * Génère la ligne {@code GRAPHVIZ} d'une {@link Case}.
             * @param p Coordonnées de la case
             * @param c La case
             * @return La ligne représentant un sommet du graphe qui correspond
             * à la case passée en paramètre.
             * @author Ivan Canet
             */
            public static String createSommet(Point p, Case c){
                String coords = String.format("\t\"%d %d\" ", p.x, p.y);
                
                if(c.estInoccupee()){
                    if(c.getTuileOrientee()
                            .getTuileBase()
                            .estInstallationPossible())
                        // Vide + possible: Rectangle vert
                        return coords + "[shape=plaintext, fillcolor=green];";
                    else
                        // Vide + !possible: triangle gris
                        return coords + "[shape=triangle, fillcolor=grey];";
                }else{
                    // !Vide: disque|pentagone blanc|noir
                    return coords
                            + String.format("[shape=%s] ",
                                    c.getPion() == CHAMP ? "circle" :
                                                           "pentagon")
                            + String.format("[fillcolor=\"#%s\", fontcolor=%s];",
                                    c.getJoueur().getCouleur().getRGBHex(),
                                    c.getJoueur().getCouleur() == CLAIR ?
                                            "black" :
                                            "white");
                }
            }
            
            private static String fromPointToString(Point p){
                return String.format("\"%d %d\"", p.x, p.y);
            }
            
            /**
             * Génère la ligne {@code GRAPHVIZ} d'une {@link Relation}.
             * @param r la relation à convertir
             * @return La ligne représentant une relation, qui correspond
             * à celle passée en paramètre.
             * @author Ivan Canet
             */
            public static String createRelation(Relation<Point> r, Joueur j){
                if(r == null)
                    throw new IllegalArgumentException("Non");
                
                String debut = String.format("\t%s -- %s",
                        fromPointToString(r.getPremier()),
                        fromPointToString(r.getDeuxieme()));
                
                String fin;
                if(j == null)
                    fin = ";";
                else
                    fin = String.format(" [color=\"#%s\"];",
                            j.getCouleur()
                                    .getRGBHex());
                
                return debut + fin;
            }
            
            /**
             * Permet d'obtenir le footer d'un fichier de format {@code GRAPHVIZ}.
             * @return Le footer.
             * @author Ivan Canet
             */
            public static String getFooter(){
                return "}";
            }
        }
        
        /**
         * Cette classe représente une relation dans un graphe <b>non-dirigé</b>.
         * <p>Cette classe permet d'éviter d'écrire des relations en double; elle
         * peut être utilisée dans un {@link java.util.Set Set}.
         * <p>Cette classe est immuable.
         * @param <C> Le type d'objet à associer (les deux sommets)
         * @author Ivan Canet
         */
        public static final class Relation<C> {
            private final C a, b;
            private final Joueur j;
            
            /**
             * Crée une Relation <b>non-dirigée</b> entre deux objets.
             * @param premier le premier objet
             * @param deuxieme le deuxième objet
             * @param joueur le joueur à qui cette relation appartient 
             * (peut valoir {@code null} si la relation n'appartient à aucun 
             * joueur)
             */
            public Relation(C premier, C deuxieme, Joueur joueur){
                if(premier == null)
                    throw new NullPointerException("Le paramètre 'premier' ne peut pas être null.");
                if(deuxieme == null)
                    throw new NullPointerException("Le paramètre 'deuxieme' ne peut pas être null.");
                
                a = premier;
                b = deuxieme;
                j = joueur;
            }
            
            /**
             * Crée une Relation <b>non-dirigée</b> entre deux objets.
             * @param premier le premier objet
             * @param deuxieme le deuxième objet
             */
            public Relation(C premier, C deuxieme){
                this(premier, deuxieme, null);
            }
            
            /**
             * Récupère les deux sommets de cette relation.
             * <p>Pour deux Relations égales (d'après 
             * {@link #equals(java.lang.Object) equals(Object)}), les deux
             * sommets seront les même, mais leur ordre d'apparition peut
             * être différent.
             * <p>Comme cet objet est immuable, la modification de la collection
             * renvoyée par cette méthode n'aura aucun impact.
             * @return Une collection contenant les deux sommets de cette
             * relation.
             */
            public Collection<C> get(){
                return Arrays.asList(a, b);
            }
            
            /**
             * Récupère le premier sommet de cette Relation.
             * @return le premier sommet de cette Relation.
             */
            public C getPremier(){
                return a;
            }
            
            /**
             * Récupère le deuxième sommet de cette Relation.
             * @return le deuxième sommet de cette Relation.
             */
            public C getDeuxieme(){
                return b;
            }
            
            /**
             * Récupère le joueur de cette Relation.
             * @return le joueur de cette Relation ou {@code null}.
             */
            public Joueur getJoueur(){
                return j;
            }
            
            /**
             * Crée un objet Relation identique, mais dans lequel les deux
             * sommets sont inversés.
             * <p>Ces deux objets sont identiques d'après {@link 
             * #equals(java.lang.Object) equals(Object)}, ils ne peuvent pas
             * cohabiter dans un Set.
             * @return La relation identique dans un sens opposé.
             */
            public Relation<C> getOppose(){
                return new Relation<>(b, a, j);
            }

            @Override
            public int hashCode() {
                int hash = 3;
                hash = hash + Objects.hashCode(this.a);
                hash = hash + Objects.hashCode(this.b);
                return hash;
            }

            @Override
            public boolean equals(Object obj) {
                if (this == obj)    return true;
                if (obj == null)    return false;
                if (getClass() != obj.getClass())   return false;
                
                final Relation<?> other = (Relation<?>) obj;
                
                // Si a et b sont identiques
                if(Objects.equals(this.a, other.a)
                        && Objects.equals(this.b, other.b))
                    return true;
                
                // Si a et b sont identiques mais échangés
                if(Objects.equals(this.a, other.b)
                        && Objects.equals(this.b, other.a))
                    return true;
                
                // Sinon
                return false;
            }
        }
}
