/**
 * États d'une manche.
 */
package fjord;

/**
 * États d'une manche.
 * 
 * @author Olivier
 */
public enum EtatManche {

	/**
	 * Début.
	 */
	DEBUT,
	/**
	 * Phase de découverte.
	 */
	DECOUVERTE,
	/**
	 * Phase de colonisation.
	 */
	COLONISATION,
	/**
	 * Fin.
	 */
	FIN;

	/**
	 * Création d'un état d'une manche.
	 */
	private EtatManche() {
	}

}
