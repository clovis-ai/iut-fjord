/**
 * Joueur.
 */
package fjord;

import java.awt.Point;
import java.util.List;
import java.util.Map;

/**
 * Joueur.
 * 
 * La stratégie de ce joueur est très simpliste : toujours choisir la première
 * des tuiles faces visibles, toujours jouer la première des coordonnées (ligne
 * et colonne) où poser la tuile piochée, toujours jouer la première des tuiles
 * orientées à poser pour la tuile piochée, toujours installer une hutte,
 * toujours jouer la première des coordonnées (ligne et colonne) d'une case où
 * poser un champ.
 * 
 * @author Olivier
 */
public class Joueur {

	/**
	 * Couleur.
	 */
	private Couleur couleur;

	/**
	 * Création d'un joueur.
	 * 
	 * @param couleur
	 *            Couleur.
	 */
	public Joueur(final Couleur couleur) {
		if (couleur == null) {
			throw new IllegalArgumentException("La couleur doit être renseignée.");
		}
		this.couleur = couleur;
	}

	/**
	 * Couleur.
	 * 
	 * @return Couleur.
	 */
	public Couleur getCouleur() {
		return couleur;
	}

	/**
	 * Nom.
	 * 
	 * @return Nom.
	 */
	public String getNom() {
		return "« " + couleur.getNom() + " »";
	}

	/**
	 * Lors de la phase de découverte de la manche, joue une tuile soit en la
	 * choisissant parmi celles de la pioche des tuiles faces visibles qui
	 * peuvent être posées, soit demandant qu'elle soit tirée aléatoirement
	 * parmi celles de la pioche des tuiles faces cachées.
	 * 
	 * @param piocheTuilesVisiblesPossiblePoser
	 *            Pioche des tuiles faces visibles qui peuvent être posées.
	 * @return Soit une tuile choisie parmi celles de la pioche des tuiles faces
	 *         visibles qui peuvent être posées, soit null pour demander qu'une
	 *         tuile soit tirée aléatoirement parmi celles de la pioche des
	 *         tuiles faces cachées.
	 */
	public Tuile jouerDecouvertePioche(final List<Tuile> piocheTuilesVisiblesPossiblePoser) {
		if (piocheTuilesVisiblesPossiblePoser == null || piocheTuilesVisiblesPossiblePoser.isEmpty()) {
			throw new IllegalArgumentException(
					"La pioche des tuiles faces visibles qui peuvent être posées doit être renseignée.");
		}
		return piocheTuilesVisiblesPossiblePoser.get(0);
	}

	/**
	 * Lors de la phase de découverte de la manche, joue une coordonnée (ligne
	 * et colonne) où poser la tuile piochée.
	 * 
	 * @param coordLigColTuilePossiblePoser
	 *            Coordonnées (ligne et colonne) où la tuile piochée peut être
	 *            posée.
	 * @return Coordonnée (ligne et colonne) où poser la tuile piochée
	 */
	public Point jouerDecouvertePoseCoordLigCol(final List<Point> coordLigColTuilePossiblePoser) {
		if (coordLigColTuilePossiblePoser == null || coordLigColTuilePossiblePoser.isEmpty()) {
			throw new IllegalArgumentException(
					"Les coordonnées (ligne et colonne) où la tuile piochée peut être posée doivent être renseignées.");
		}
		return coordLigColTuilePossiblePoser.get(0);
	}

	/**
	 * Lors de la phase de découverte de la manche, joue une tuile orientée à
	 * poser pour la tuile piochée.
	 * 
	 * @param tuileOrienteeTuilePossiblePoser
	 *            Tuiles orientées de la tuile piochée qui peut être posée.
	 * @return Tuile orientée à poser pour la tuile piochée.
	 */
	public TuileOrientee jouerDecouvertePoseTuileOrientee(final List<TuileOrientee> tuileOrienteeTuilePossiblePoser) {
		if (tuileOrienteeTuilePossiblePoser == null || tuileOrienteeTuilePossiblePoser.isEmpty()) {
			throw new IllegalArgumentException(
					"Les tuiles orientées de la tuile piochée qui peut être posée doivent être renseignées.");
		}
		return tuileOrienteeTuilePossiblePoser.get(0);
	}

	/**
	 * Lors de la phase de découverte de la manche, indique si une hutte doit
	 * être installée sur la tuile piochée et posée.
	 * 
	 * @return Indique si une hutte doit être installée sur la tuile piochée et
	 *         posée.
	 */
	public boolean jouerDecouverteInstallationHutte() {
		return true;
	}

	/**
	 * Lors de la phase de colonisation de la manche, joue une coordonnée (ligne
	 * et colonne) d'une case où installer un champ.
	 * 
	 * @param coordLigColPossibleInstallerChamp
	 *            Coordonnées (ligne et colonne) où un champ peut être installé.
	 * @param coordLigColPossibleInstallerChampAdversaire
	 *            Coordonnées (ligne et colonne) où un champ peut être installé
	 *            par le joueur adversaire du joueur.
	 * @param sommetsGrapheDecouverteTerresArables
	 *            Sommets représentant les joueurs (ou null si la case ne
	 *            contient pas de pion) du graphe des tuiles du plateau en
	 *            considérant les terres arables durant la phase de découverte.
	 * @param grapheDecouverteTerresArables
	 *            Graphe des tuiles du plateau en considérant les terres arables
	 *            durant la phase de découverte.
	 * @return Coordonnée (ligne et colonne) où installer un champ.
	 */
	public Point jouerColonisationInstallationChamp(final List<Point> coordLigColPossibleInstallerChamp,
			final List<Point> coordLigColPossibleInstallerChampAdversaire,
			final Map<Point, Joueur> sommetsGrapheDecouverteTerresArables,
			final Map<Point, Map<Point, Boolean>> grapheDecouverteTerresArables) throws Exception {
		if (coordLigColPossibleInstallerChamp == null || coordLigColPossibleInstallerChamp.isEmpty()) {
			throw new IllegalArgumentException(
					"Il doit y avoir au moins une coordonnée (ligne et colonne) où un champ peut être installé (par le joueur).");
		}
		if (coordLigColPossibleInstallerChampAdversaire == null || sommetsGrapheDecouverteTerresArables == null
				|| grapheDecouverteTerresArables == null) {
			// Non géré car non bloquant !
		}
		return coordLigColPossibleInstallerChamp.get(0);
	}

}
