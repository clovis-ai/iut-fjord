/**
 * Joueur « Intelligence artificielle ».
 */
package fjord;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.awt.*;
import java.util.List;
import java.util.Map;
import java.util.Random;


/**
 * Joueur « Intelligence artificielle ».
 *
 * @author Olivier
 */
public class IA extends Joueur {


    /**
     * Création d'un joueur « Intelligence artificielle ».
     * <p>
     * La stratégie de ce joueur est totalement aléatoire : à chaque fois,
     * l'action est tirée au sort parmi toutes celles proposées.
     *
     * @param couleur Couleur.
     */
    public IA(Couleur couleur) {
        super(couleur);
    }

    @Override
    public String getNom() {
        return super.getNom() + " (« Intelligence artificielle »)";
    }

    @Override
    public Tuile jouerDecouvertePioche(final List<Tuile> piocheTuilesVisiblesPossiblePoser) {
        if (piocheTuilesVisiblesPossiblePoser == null || piocheTuilesVisiblesPossiblePoser.isEmpty()) {
            throw new IllegalArgumentException(
                    "La pioche des tuiles faces visibles qui peuvent être posées doit être renseignée.");
        }
        final Double PROBA_CHOIX_PIOCHE_TUILES_VISIBLES_POSSIBLE_POSER = 0.5;
        return piocheTuilesVisiblesPossiblePoser != null && !piocheTuilesVisiblesPossiblePoser.isEmpty()
                && (new Random()).nextDouble() < PROBA_CHOIX_PIOCHE_TUILES_VISIBLES_POSSIBLE_POSER
                ? piocheTuilesVisiblesPossiblePoser
                .get((new Random()).nextInt(piocheTuilesVisiblesPossiblePoser.size()))
                : null;
    }

    @Override
    public Point jouerDecouvertePoseCoordLigCol(final List<Point> coordLigColTuilePossiblePoser) {
        if (coordLigColTuilePossiblePoser == null || coordLigColTuilePossiblePoser.isEmpty()) {
            throw new IllegalArgumentException(
                    "Les coordonnées (ligne et colonne) où la tuile piochée peut être posée doivent être renseignées.");
        }
        return coordLigColTuilePossiblePoser.get((new Random()).nextInt(coordLigColTuilePossiblePoser.size()));
    }

    @Override
    public TuileOrientee jouerDecouvertePoseTuileOrientee(final List<TuileOrientee> tuileOrienteeTuilePossiblePoser) {
        if (tuileOrienteeTuilePossiblePoser == null || tuileOrienteeTuilePossiblePoser.isEmpty()) {
            throw new IllegalArgumentException(
                    "Les tuiles orientées de la tuile piochée qui peut être posée doivent être renseignées.");
        }
        return tuileOrienteeTuilePossiblePoser.get((new Random()).nextInt(tuileOrienteeTuilePossiblePoser.size()));
    }

    @Override
    public boolean jouerDecouverteInstallationHutte() {
        final Double PROBA_INSTALLATION_HUTTE = 0.15;
        return (new Random()).nextDouble() < PROBA_INSTALLATION_HUTTE;
    }

    /**
     * IA du jeu
     *
     * @param coordLigColPossibleInstallerChamp           Coordonnées (ligne et colonne) où un champ peut être installé.
     * @param coordLigColPossibleInstallerChampAdversaire Coordonnées (ligne et colonne) où un champ peut être installé
     *                                                    par le joueur adversaire du joueur.
     * @param sommetsGrapheDecouverteTerresArables        Sommets représentant les joueurs (ou null si la case ne
     *                                                    contient pas de pion) du graphe des tuiles du plateau en
     *                                                    considérant les terres arables durant la phase de découverte.
     * @param grapheDecouverteTerresArables               Graphe des tuiles du plateau en considérant les terres arables
     *                                                    durant la phase de découverte.
     * @return coordonné du coup que elle décide de jouer
     * @author Maxime Cots
     */

    @Override
    public Point jouerColonisationInstallationChamp(final List<Point> coordLigColPossibleInstallerChamp,
                                                    final List<Point> coordLigColPossibleInstallerChampAdversaire,
                                                    final Map<Point, Joueur> sommetsGrapheDecouverteTerresArables,
                                                    final Map<Point, Map<Point, Boolean>> grapheDecouverteTerresArables) {

        Point coupFinal = null;

        if (coordLigColPossibleInstallerChamp == null || coordLigColPossibleInstallerChamp.isEmpty()) {
            throw new IllegalArgumentException("Dommage il n'y a pas de coordonnées dans les paramètres");
        }
        if (coordLigColPossibleInstallerChampAdversaire == null ||
                sommetsGrapheDecouverteTerresArables == null || grapheDecouverteTerresArables == null) {
            return null;
        }

        //Cherche un Point qui se trouve a l'intersection des deux Listes
        for (Point p : coordLigColPossibleInstallerChamp) {
            if (coordLigColPossibleInstallerChampAdversaire.contains(p)) {
                coupFinal = p;
                break;
            }
        }
        //renvoi un pion au hasard si aucune pose se trouvant dans les deux listes de poses possibles
        return (coupFinal == null) ?
                coordLigColPossibleInstallerChamp.get(new Random().nextInt(coordLigColPossibleInstallerChamp.size())) : coupFinal;
    }
}

