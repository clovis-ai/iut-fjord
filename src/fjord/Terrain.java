/**
 * Terrain.
 */
package fjord;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Terrain.
 * 
 * @author Olivier
 */
public enum Terrain {

	/**
	 * Premier terrain : "Terres arables" de code 'A'rable.
	 */
	TERRES_ARABLES("Terres arables", 'A', new Color(51, 153, 0), true, true),
	/**
	 * Deuxième terrain : "Mer" de code 'B'aie.
	 */
	MER("Mer", 'B', new Color(51, 51, 255), false, false),
	/**
	 * Troisième terrain : "Montagne" de code 'C'olline.
	 */
	MONTAGNE("Montagne", 'C', new Color(51, 51, 51), false, true);

	/**
	 * Nom du terrain.
	 */
	private final String nom;

	/**
	 * Code du terrain.
	 */
	private final char code;

	/**
	 * Couleur RGB du terrain.
	 */
	private final Color rgb;

	/**
	 * L'installation d'un pion est-elle possible sur ce terrain ?
	 */
	private final boolean installationPossible;

	/**
	 * Est une terre ?
	 */
	private final boolean estTerre;

	/**
	 * Création d'un terrain.
	 * 
	 * @param nom
	 *            Nom du terrain.
	 * @param code
	 *            Code du terrain.
	 * @param rgb
	 *            Couleur RGB du terrain.
	 * @param installationPossible
	 *            L'installation d'un pion est-elle possible sur ce terrain ?
	 * @param estTerre
	 *            Est une terre ?
	 */
	private Terrain(final String nom, final char code, final Color rgb, final boolean installationPossible,
			final boolean estTerre) {
		this.nom = nom;
		this.code = code;
		this.rgb = rgb;
		this.installationPossible = installationPossible;
		this.estTerre = estTerre;
	}

	/**
	 * Nom de la couleur.
	 * 
	 * @return Nom de la couleur.
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Code du terrain.
	 * 
	 * @return Code du terrain.
	 */
	public char getCode() {
		return code;
	}

	/**
	 * Couleur RGB de la couleur.
	 * 
	 * @return Couleur RGB de la couleur.
	 */
	public Color getRGB() {
		return rgb;
	}

	/**
	 * L'installation d'un pion est-elle possible sur ce terrain ?
	 * 
	 * @return L'installation d'un pion est-elle possible sur ce terrain ?
	 */
	public boolean estInstallationPossible() {
		return installationPossible;
	}

	/**
	 * Est une terre ?
	 * 
	 * @return Est une terre ?
	 */
	public boolean estTerre() {
		return estTerre;
	}

	/**
	 * Codes des terrains.
	 * 
	 * @return Codes des terrains.
	 */
	public static List<Character> codesTerrains() {
		return Arrays.stream(Terrain.values()).map(terrain -> terrain.code).collect(Collectors.toList());
	}

	/**
	 * Terrain d'un code de terrain donné.
	 * 
	 * @param code
	 *            Code du terrain.
	 * @return Terrain d'un code de terrain donné.
	 */
	public static Terrain getTerrain(final char code) {
		return Arrays.stream(Terrain.values()).filter(terrain -> terrain.code == code).findFirst().orElse(null);
	}

}
