/**
 * Tuile (de terrain) orientée.
 */
package fjord;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Tuile (de terrain) orientée.
 * 
 * @author Olivier
 */

/* Codage de l'étiquette : cf. Tuile */

/*
 * N. B. : aucune tuile orientée ne contient un bord ayant à la fois de la mer
 * et de la montagne (ni "BC" ni "CB").
 */

public enum TuileOrientee {

	/**
	 * Tuile orientée "AAAAAA".
	 */
	AAAAAA(Tuile.AAAAAA, 0),
	/**
	 * Tuile orientée "AAAAAB".
	 */
	AAAAAB(Tuile.AAAAAB, 0),
	/**
	 * Tuile orientée "AAAAAC".
	 */
	AAAAAC(Tuile.AAAAAC, 0),
	/**
	 * Tuile orientée "AAAABA".
	 */
	AAAABA(Tuile.AAAAAB, 1),
	/**
	 * Tuile orientée "AAAABB".
	 */
	AAAABB(Tuile.AAAABB, 0),
	/**
	 * Tuile orientée "AAAACA".
	 */
	AAAACA(Tuile.AAAAAC, 1),
	/**
	 * Tuile orientée "AAAACC".
	 */
	AAAACC(Tuile.AAAACC, 0),
	/**
	 * Tuile orientée "AAABAA".
	 */
	AAABAA(Tuile.AAAAAB, 2),
	/**
	 * Tuile orientée "AAABBA".
	 */
	AAABBA(Tuile.AAAABB, 1),
	/**
	 * Tuile orientée "AAABBB".
	 */
	AAABBB(Tuile.AAABBB, 0),
	/**
	 * Tuile orientée "AAACAA".
	 */
	AAACAA(Tuile.AAAAAC, 2),
	/**
	 * Tuile orientée "AAACAC".
	 */
	AAACAC(Tuile.AAACAC, 0),
	/**
	 * Tuile orientée "AAACCA".
	 */
	AAACCA(Tuile.AAAACC, 1),
	/**
	 * Tuile orientée "AAACCC".
	 */
	AAACCC(Tuile.AAACCC, 0),
	/**
	 * Tuile orientée "AABAAA".
	 */
	AABAAA(Tuile.AAAAAB, 3),
	/**
	 * Tuile orientée "AABAAC".
	 */
	AABAAC(Tuile.AABAAC, 0),
	/**
	 * Tuile orientée "AABBAA".
	 */
	AABBAA(Tuile.AAAABB, 2),
	/**
	 * Tuile orientée "AABBAC".
	 */
	AABBAC(Tuile.AABBAC, 0),
	/**
	 * Tuile orientée "AABBBA".
	 */
	AABBBA(Tuile.AAABBB, 1),
	/**
	 * Tuile orientée "AABBBB".
	 */
	AABBBB(Tuile.AABBBB, 0),
	/**
	 * Tuile orientée "AACAAA".
	 */
	AACAAA(Tuile.AAAAAC, 3),
	/**
	 * Tuile orientée "AACAAB".
	 */
	AACAAB(Tuile.AABAAC, 3),
	/**
	 * Tuile orientée "AACACA".
	 */
	AACACA(Tuile.AAACAC, 1),
	/**
	 * Tuile orientée "AACACC".
	 */
	AACACC(Tuile.AACACC, 0),
	/**
	 * Tuile orientée "AACCAA".
	 */
	AACCAA(Tuile.AAAACC, 2),
	/**
	 * Tuile orientée "AACCAB".
	 */
	AACCAB(Tuile.AACCAB, 0),
	/**
	 * Tuile orientée "AACCCA".
	 */
	AACCCA(Tuile.AAACCC, 1),
	/**
	 * Tuile orientée "AACCCC".
	 */
	AACCCC(Tuile.AACCCC, 0),
	/**
	 * Tuile orientée "ABAAAA".
	 */
	ABAAAA(Tuile.AAAAAB, 4),
	/**
	 * Tuile orientée "ABAACA".
	 */
	ABAACA(Tuile.AABAAC, 1),
	/**
	 * Tuile orientée "ABAACC".
	 */
	ABAACC(Tuile.AACCAB, 4),
	/**
	 * Tuile orientée "ABBAAA".
	 */
	ABBAAA(Tuile.AAAABB, 3),
	/**
	 * Tuile orientée "ABBACA".
	 */
	ABBACA(Tuile.AABBAC, 1),
	/**
	 * Tuile orientée "ABBACC".
	 */
	ABBACC(Tuile.ABBACC, 0),
	/**
	 * Tuile orientée "ABBBAA".
	 */
	ABBBAA(Tuile.AAABBB, 2),
	/**
	 * Tuile orientée "ABBBAC".
	 */
	ABBBAC(Tuile.ABBBAC, 0),
	/**
	 * Tuile orientée "ABBBBA".
	 */
	ABBBBA(Tuile.AABBBB, 1),
	/**
	 * Tuile orientée "ABBBBB".
	 */
	ABBBBB(Tuile.ABBBBB, 0),
	/**
	 * Tuile orientée "ACAAAA".
	 */
	ACAAAA(Tuile.AAAAAC, 4),
	/**
	 * Tuile orientée "ACAAAC".
	 */
	ACAAAC(Tuile.AAACAC, 4),
	/**
	 * Tuile orientée "ACAABA".
	 */
	ACAABA(Tuile.AABAAC, 4),
	/**
	 * Tuile orientée "ACAABB".
	 */
	ACAABB(Tuile.AABBAC, 4),
	/**
	 * Tuile orientée "ACABBB".
	 */
	ACABBB(Tuile.ABBBAC, 4),
	/**
	 * Tuile orientée "ACACAA".
	 */
	ACACAA(Tuile.AAACAC, 2),
	/**
	 * Tuile orientée "ACACCA".
	 */
	ACACCA(Tuile.AACACC, 1),
	/**
	 * Tuile orientée "ACCAAA".
	 */
	ACCAAA(Tuile.AAAACC, 3),
	/**
	 * Tuile orientée "ACCAAC".
	 */
	ACCAAC(Tuile.AACACC, 3),
	/**
	 * Tuile orientée "ACCABA".
	 */
	ACCABA(Tuile.AACCAB, 1),
	/**
	 * Tuile orientée "ACCABB".
	 */
	ACCABB(Tuile.ABBACC, 3),
	/**
	 * Tuile orientée "ACCACC".
	 */
	ACCACC(Tuile.ACCACC, 0),
	/**
	 * Tuile orientée "ACCCAA".
	 */
	ACCCAA(Tuile.AAACCC, 2),
	/**
	 * Tuile orientée "ACCCCA".
	 */
	ACCCCA(Tuile.AACCCC, 1),
	/**
	 * Tuile orientée "ACCCCC".
	 */
	ACCCCC(Tuile.ACCCCC, 0),
	/**
	 * Tuile orientée "BAAAAA".
	 */
	BAAAAA(Tuile.AAAAAB, 5),
	/**
	 * Tuile orientée "BAAAAB".
	 */
	BAAAAB(Tuile.AAAABB, 5),
	/**
	 * Tuile orientée "BAAABB".
	 */
	BAAABB(Tuile.AAABBB, 5),
	/**
	 * Tuile orientée "BAABBB".
	 */
	BAABBB(Tuile.AABBBB, 5),
	/**
	 * Tuile orientée "BAACAA".
	 */
	BAACAA(Tuile.AABAAC, 2),
	/**
	 * Tuile orientée "BAACCA".
	 */
	BAACCA(Tuile.AACCAB, 5),
	/**
	 * Tuile orientée "BABBBB".
	 */
	BABBBB(Tuile.ABBBBB, 5),
	/**
	 * Tuile orientée "BACAAB".
	 */
	BACAAB(Tuile.AABBAC, 3),
	/**
	 * Tuile orientée "BACABB".
	 */
	BACABB(Tuile.ABBBAC, 3),
	/**
	 * Tuile orientée "BACCAB".
	 */
	BACCAB(Tuile.ABBACC, 2),
	/**
	 * Tuile orientée "BBAAAA".
	 */
	BBAAAA(Tuile.AAAABB, 4),
	/**
	 * Tuile orientée "BBAAAB".
	 */
	BBAAAB(Tuile.AAABBB, 4),
	/**
	 * Tuile orientée "BBAABB".
	 */
	BBAABB(Tuile.AABBBB, 4),
	/**
	 * Tuile orientée "BBABBB".
	 */
	BBABBB(Tuile.ABBBBB, 4),
	/**
	 * Tuile orientée "BBACAA".
	 */
	BBACAA(Tuile.AABBAC, 2),
	/**
	 * Tuile orientée "BBACAB".
	 */
	BBACAB(Tuile.ABBBAC, 2),
	/**
	 * Tuile orientée "BBACCA".
	 */
	BBACCA(Tuile.ABBACC, 1),
	/**
	 * Tuile orientée "BBBAAA".
	 */
	BBBAAA(Tuile.AAABBB, 3),
	/**
	 * Tuile orientée "BBBAAB".
	 */
	BBBAAB(Tuile.AABBBB, 3),
	/**
	 * Tuile orientée "BBBABB".
	 */
	BBBABB(Tuile.ABBBBB, 3),
	/**
	 * Tuile orientée "BBBACA".
	 */
	BBBACA(Tuile.ABBBAC, 1),
	/**
	 * Tuile orientée "BBBBAA".
	 */
	BBBBAA(Tuile.AABBBB, 2),
	/**
	 * Tuile orientée "BBBBAB".
	 */
	BBBBAB(Tuile.ABBBBB, 2),
	/**
	 * Tuile orientée "BBBBBA".
	 */
	BBBBBA(Tuile.ABBBBB, 1),
	/**
	 * Tuile orientée "CAAAAA".
	 */
	CAAAAA(Tuile.AAAAAC, 5),
	/**
	 * Tuile orientée "CAAAAC".
	 */
	CAAAAC(Tuile.AAAACC, 5),
	/**
	 * Tuile orientée "CAAACA".
	 */
	CAAACA(Tuile.AAACAC, 5),
	/**
	 * Tuile orientée "CAAACC".
	 */
	CAAACC(Tuile.AAACCC, 5),
	/**
	 * Tuile orientée "CAABAA".
	 */
	CAABAA(Tuile.AABAAC, 5),
	/**
	 * Tuile orientée "CAABBA".
	 */
	CAABBA(Tuile.AABBAC, 5),
	/**
	 * Tuile orientée "CAACAC".
	 */
	CAACAC(Tuile.AACACC, 5),
	/**
	 * Tuile orientée "CAACCC".
	 */
	CAACCC(Tuile.AACCCC, 5),
	/**
	 * Tuile orientée "CABAAC".
	 */
	CABAAC(Tuile.AACCAB, 3),
	/**
	 * Tuile orientée "CABBAC".
	 */
	CABBAC(Tuile.ABBACC, 5),
	/**
	 * Tuile orientée "CABBBA".
	 */
	CABBBA(Tuile.ABBBAC, 5),
	/**
	 * Tuile orientée "CACAAA".
	 */
	CACAAA(Tuile.AAACAC, 3),
	/**
	 * Tuile orientée "CACCAA".
	 */
	CACCAA(Tuile.AACACC, 2),
	/**
	 * Tuile orientée "CACCAC".
	 */
	CACCAC(Tuile.ACCACC, 2),
	/**
	 * Tuile orientée "CACCCC".
	 */
	CACCCC(Tuile.ACCCCC, 5),
	/**
	 * Tuile orientée "CCAAAA".
	 */
	CCAAAA(Tuile.AAAACC, 4),
	/**
	 * Tuile orientée "CCAAAC".
	 */
	CCAAAC(Tuile.AAACCC, 4),
	/**
	 * Tuile orientée "CCAACA".
	 */
	CCAACA(Tuile.AACACC, 4),
	/**
	 * Tuile orientée "CCAACC".
	 */
	CCAACC(Tuile.AACCCC, 4),
	/**
	 * Tuile orientée "CCABAA".
	 */
	CCABAA(Tuile.AACCAB, 2),
	/**
	 * Tuile orientée "CCABBA".
	 */
	CCABBA(Tuile.ABBACC, 4),
	/**
	 * Tuile orientée "CCACCA".
	 */
	CCACCA(Tuile.ACCACC, 1),
	/**
	 * Tuile orientée "CCACCC".
	 */
	CCACCC(Tuile.ACCCCC, 4),
	/**
	 * Tuile orientée "CCCAAA".
	 */
	CCCAAA(Tuile.AAACCC, 3),
	/**
	 * Tuile orientée "CCCAAC".
	 */
	CCCAAC(Tuile.AACCCC, 3),
	/**
	 * Tuile orientée "CCCACC".
	 */
	CCCACC(Tuile.ACCCCC, 3),
	/**
	 * Tuile orientée "CCCCAA".
	 */
	CCCCAA(Tuile.AACCCC, 2),
	/**
	 * Tuile orientée "CCCCAC".
	 */
	CCCCAC(Tuile.ACCCCC, 2),
	/**
	 * Tuile orientée "CCCCCA".
	 */
	CCCCCA(Tuile.ACCCCC, 1),
	/**
	 * Tuile orientée "CCCCCC".
	 */
	CCCCCC(Tuile.CCCCCC, 0);

	/**
	 * Tuile de base (à laquelle correspond la tuile orientée).
	 * 
	 * N. B. : c'est la plus petite dans l'ordre lexicographique de sa classe de
	 * conjugaison.
	 * 
	 * Remarque : cette information est calculable ; on a choisi de la stocker
	 * (et d'en vérifier les valeurs par les tests unitaires) pour gagner un peu
	 * en performance.
	 */
	private final Tuile tuileBase;

	/**
	 * Longueur du préfixe du mot correspondant à la tuile de base pour obtenir
	 * par conjugaison la tuile orientée.
	 * 
	 * Remarque : cette information est calculable ; on a choisi de la stocker
	 * (et d'en vérifier les valeurs par les tests unitaires) pour gagner un peu
	 * en performance.
	 */
	private final int lgPrefixeBase;

	/**
	 * Création d'une tuile orientée.
	 * 
	 * @param tuileBase
	 *            Tuile de base.
	 * @param lgPrefixeBase
	 *            Longueur du préfixe du mot correspondant à la tuile de base
	 *            pour obtenir par conjugaison la tuile orientée.
	 */
	private TuileOrientee(final Tuile tuileBase, final int lgPrefixeBase) {
		this.tuileBase = tuileBase;
		this.lgPrefixeBase = lgPrefixeBase;
	}

	/**
	 * Tuile de base.
	 * 
	 * @return Tuile de base.
	 */
	public Tuile getTuileBase() {
		return tuileBase;
	}

	/**
	 * Longueur du préfixe du mot correspondant à la tuile de base pour obtenir
	 * par conjugaison la tuile orientée.
	 * 
	 * @return Longueur du préfixe du mot correspondant à la tuile de base pour
	 *         obtenir par conjugaison la tuile orientée.
	 */
	public int getLgPrefixeBase() {
		return lgPrefixeBase;
	}

	/**
	 * Codage (par deux caractères lus en sens trigonométrique c.-à-d.
	 * anti-horaire) dans la direction.
	 * 
	 * Exemple : "AZERTY" --> "AZ" pour 120°, "ZE" pour 180°, "ER" pour 240°,
	 * "RT" pour 300°, "TY" pour 0° et "YA" (et non pas "AY") pour 60°.
	 * 
	 * @param direction
	 *            Direction.
	 * @return Codage dans la direction.
	 */
	public String getCodageDirection(final Direction direction) {
		if (direction == null) {
			throw new IllegalArgumentException("La direction doit être renseignée.");
		}
		switch (direction) {
		case DIRECTION_0_DEGRE:
			return this.toString().substring(4, 6);
		case DIRECTION_60_DEGRES:
			return this.toString().charAt(5) + "" + this.toString().charAt(0);
		case DIRECTION_120_DEGRES:
			return this.toString().substring(0, 2);
		case DIRECTION_180_DEGRES:
			return this.toString().substring(1, 3);
		case DIRECTION_240_DEGRES:
			return this.toString().substring(2, 4);
		case DIRECTION_300_DEGRES:
			return this.toString().substring(3, 5);
		}
		return null;
	}

	/**
	 * Codage inverse (par deux caractères lus en sens horaire) dans la
	 * direction.
	 * 
	 * Attention : private --> public à cause des tests.
	 * 
	 * @param direction
	 *            Direction.
	 * @return Codage inverse dans la direction.
	 */
	public String getCodageInverseDirection(final Direction direction) {
		String codageDirection = getCodageDirection(direction);
		return Character.toString(codageDirection.charAt(1)) + Character.toString(codageDirection.charAt(0));
	}

	/**
	 * Indique si la direction contient de la terre.
	 * 
	 * @param direction
	 *            Direction.
	 * @return Indique si la direction contient de la terre.
	 */
	public boolean aTerreDirection(final Direction direction) {
		if (direction == null) {
			throw new IllegalArgumentException("La direction doit être renseignée.");
		}
		return Arrays.asList(getCodageDirection(direction).split("")).stream()
				.anyMatch(codeTerrain -> Terrain.getTerrain(codeTerrain.charAt(0)).estTerre());
	}

	/**
	 * Indique si l'installation d'un pion (un champ en fait) est possible dans
	 * la direction c.-à-d. si on peut passer des terres arables de la tuile
	 * orientée de/vers les terres arables pour cette direction.
	 * 
	 * @param direction
	 *            Direction.
	 * @return Indique si l'installation d'un pion (un champ en fait) sur la
	 *         tuile (existante mais inoccupée) est possible dans la direction.
	 */
	public boolean installationPossibleDirection(final Direction direction) {
		if (direction == null) {
			throw new IllegalArgumentException("La direction doit être renseignée.");
		}
		return Arrays.asList(getCodageDirection(direction).split("")).stream()
				.anyMatch(codeTerrain -> Terrain.getTerrain(codeTerrain.charAt(0)).estInstallationPossible());
	}

	/**
	 * Tuiles orientées (adjacentes) dont les terrains correspondent, pour
	 * chaque direction.
	 * 
	 * @return Tuiles orientées (adjacentes) dont les terrains correspondent,
	 *         pour chaque direction.
	 */
	public Map<Direction, List<TuileOrientee>> terrainCorrespondre() {
		Map<Direction, List<TuileOrientee>> terrainCorrespondre = new HashMap<Direction, List<TuileOrientee>>();
		for (Direction direction : Direction.values()) {
			terrainCorrespondre.put(direction, terrainCorrespondre(direction));
		}
		return terrainCorrespondre;
	}

	/**
	 * Tuiles orientées (adjacentes) dont les terrains correspondent, pour une
	 * seule direction.
	 * 
	 * @param direction
	 *            Direction.
	 * @return Tuiles orientées (adjacentes) dont les terrains correspondent,
	 *         pour une seule direction.
	 */
	public List<TuileOrientee> terrainCorrespondre(final Direction direction) {
		if (direction == null) {
			throw new IllegalArgumentException("La direction doit être renseignée.");
		}
		return Arrays.stream(TuileOrientee.values())
				.filter(tuileOrientee -> this.getCodageDirection(direction)
						.equals(tuileOrientee.getCodageInverseDirection(direction.directionOpposee())))
				.collect(Collectors.toList());
	}

}
