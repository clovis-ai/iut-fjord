/**
 * Couleurs (des joueurs).
 */
package fjord;

import java.awt.Color;

/**
 * Couleurs (des joueurs).
 * 
 * @author Olivier
 */
public enum Couleur {

	/**
	 * Première couleur.
	 */
	FONCE("Foncé", new Color(51, 0, 0)),
	/**
	 * Deuxième couleur.
	 */
	CLAIR("Clair", new Color(255, 255, 153));

	/**
	 * Nom de la couleur.
	 */
	private final String nom;

	/**
	 * Couleur RGB de la couleur.
	 */
	private final Color rgb;

	/**
	 * Création d'une couleur.
	 * 
	 * @param nom
	 *            Nom de la couleur.
	 * @param rgb
	 *            Couleur RGB de la couleur.
	 */
	private Couleur(final String nom, final Color rgb) {
		this.nom = nom;
		this.rgb = rgb;
	}

	/**
	 * Nom de la couleur.
	 * 
	 * @return Nom de la couleur.
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Couleur RGB de la couleur.
	 * 
	 * @return Couleur RGB de la couleur.
	 */
	public Color getRGB() {
		return rgb;
	}

	/**
	 * Couleur RGB de la couleur au format hexadécimal.
	 * 
	 * @return Couleur RGB de la couleur au format hexadécimal.
	 */
	public String getRGBHex() {
		// String.format("%2x", _) meilleur que Integer.toHexString(_).
		return String.format("%2x", rgb.getRed()) + String.format("%2x", rgb.getGreen())
				+ String.format("%2x", rgb.getBlue());
	}

}
