/**
 * Plateau.
 */
package fjord;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Plateau.
 * 
 * @author Olivier
 */
public abstract class Plateau {

	/**
	 * Cases du plateau.
	 */
	protected Map<Point, Case> cases;

	/**
	 * Création d'un plateau.
	 */
	public Plateau() {
		this.cases = new HashMap<Point, Case>();
	}

	/**
	 * Cases du plateau.
	 * 
	 * @return Cases du plateau.
	 */

	public Map<Point, Case> getCases() {
		return cases;
	}

	/**
	 * Case du plateau selon ses coordonnées (ligne et colonne).
	 * 
	 * @param coordLigCol
	 *            Coordonnées (ligne et colonne) de la case du plateau.
	 * @return Case du plateau selon ses coordonnées (ligne et colonne).
	 */
	public Case getCase(final Point coordLigCol) {
		if (coordLigCol == null) {
			throw new IllegalArgumentException("Les coordonnées doivent être renseignées.");
		}
		return cases.get(coordLigCol);
	}

	/**
	 * Nombre de cases occupées par un joueur et un pion.
	 * 
	 * @param joueur
	 *            Joueur.
	 * @return Nombre de cases occupées par un joueur et un pion.
	 */
	public int nbCasesOccupees(final Joueur joueur, final Pion pion) {
		if (joueur == null) {
			throw new IllegalArgumentException("Le joueur doit être renseigné.");
		}
		if (pion == null) {
			throw new IllegalArgumentException("Le pion doit être renseigné.");
		}
		int nbCasesOccupees = 0;
		for (Case c : cases.values()) {
			if (!c.estInoccupee() && c.getJoueur().equals(joueur) && c.getPion().equals(pion)) {
				++nbCasesOccupees;
			}
		}
		return nbCasesOccupees;
	}

	/**
	 * Pose d'une tuile dans une nouvelle case du plateau.
	 * 
	 * @param coordLigCol
	 *            Coordonnées (ligne et colonne) de la case du plateau.
	 * @param tuileOrientee
	 *            Tuile orientée.
	 * @param joueur
	 *            Joueur.
	 * @param pion
	 *            Pion.
	 */
	public void poserTuile(final Point coordLigCol, final TuileOrientee tuileOrientee, final Joueur joueur,
			final Pion pion) {
		if (coordLigCol == null) {
			throw new IllegalArgumentException("Les coordonnées doivent être renseignées.");
		}
		if (tuileOrientee == null) {
			throw new IllegalArgumentException("La tuile orientée doit être renseignée.");
		}
		if (joueur == null && pion != null) {
			throw new IllegalArgumentException("Le joueur est renseigné mais pas le pion.");
		}
		if (joueur != null && pion == null) {
			throw new IllegalArgumentException("Le pion est renseigné mais pas le joueur.");
		}
		if (cases.get(coordLigCol) != null) {
			throw new IllegalArgumentException("La case existe déjà.");
		}
		cases.put(coordLigCol, new Case(tuileOrientee, joueur, pion));
	}

	/**
	 * Coordonnées (ligne et colonne) de toutes les cases sans tuile
	 * (inexistante) adjacentes à au moins deux cases où sont posées des tuiles
	 * (existantes).
	 * 
	 * N. B. : algorithme (cf. les tests pour un autre) = on part de chaque case
	 * où est posée une tuile (existante) et on regarde autour.
	 * 
	 * @return Coordonnées (ligne et colonne) toutes les cases sans tuile
	 *         (inexistante) adjacentes à au moins deux cases où sont posées des
	 *         tuiles (existantes).
	 */
	public List<Point> coordLigColAdjacentes() {
		List<Point> coordLigColAdjacentes = new ArrayList<Point>();
		Map<Point, Integer> nbCasesExistAdjCasesInexist = new HashMap<Point, Integer>();
		for (Point coordLigColCaseExist : cases.keySet()) {
			for (Direction directionCaseExist : Direction.values()) {
				Point coordLigColAdj = directionCaseExist.coordLigColDestination(coordLigColCaseExist);
				if (cases.get(coordLigColAdj) == null && nbCasesExistAdjCasesInexist.get(coordLigColAdj) == null) {
					for (Direction directionCaseInexistAdj : Direction.values()) {
						if (cases.get(directionCaseInexistAdj.coordLigColDestination(coordLigColAdj)) != null) {
							nbCasesExistAdjCasesInexist.put(coordLigColAdj,
									nbCasesExistAdjCasesInexist.getOrDefault(coordLigColAdj, 0) + 1);
						}
					}
					if (nbCasesExistAdjCasesInexist.get(coordLigColAdj) >= 2) {
						coordLigColAdjacentes.add(coordLigColAdj);
					}
				}
			}
		}
		return coordLigColAdjacentes;
	}

	/**
	 * Coordonnées (ligne et colonne) de toutes les cases sans tuile
	 * (inexistante) adjacentes à au moins deux cases où sont posées des tuiles
	 * (existantes) avec toutes les tuiles orientées qui peuvent y être posées
	 * en vérifiant qu'il y a toujours un seul ensemble de terres (3e règle de
	 * pose des tuiles).
	 * 
	 * Algorithme : on calcule l'intersection des tuiles posées (existantes)
	 * adjacentes à la case sans tuile (inexistante) et qui peuvent
	 * correspondre.
	 * 
	 * N. B. : il aurait été plus efficace de le faire simultanément lors de
	 * coordLigColAdjacentes() mais on a décomposé ici pour isoler le travail
	 * des étudiants.
	 * 
	 * @return Coordonnées (ligne et colonne) de toutes les cases sans tuile
	 *         (inexistante) adjacentes à au moins deux cases où sont posées des
	 *         tuiles (existantes) avec toutes les tuiles orientées qui peuvent
	 *         y être posées en vérifiant qu'il y a toujours un seul ensemble de
	 *         terres.
	 */
	public Map<Point, List<TuileOrientee>> coordLigColAdjacentesTuilesOrientees() {
		Map<Point, List<TuileOrientee>> coordLigColAdjacentesTuilesOrientees = new HashMap<Point, List<TuileOrientee>>();
		List<Point> coordLigColAdjacentes = coordLigColAdjacentes();
		for (Point coordLigColAdj : coordLigColAdjacentes) {
			List<TuileOrientee> tuilesOrienteesCorrespondre = null;
			// Intersection, pour la case sans tuile (inexistante), des tuiles
			// orientées qui peuvent correspondre avec celles déjà posées
			// (existantes) autour.
			List<Direction> directionAdj = new ArrayList<Direction>();
			for (Direction direction : Direction.values()) {
				Case caseExist = cases.get(direction.coordLigColDestination(coordLigColAdj));
				if (caseExist != null) {
					directionAdj.add(direction);
					if (tuilesOrienteesCorrespondre == null) {
						tuilesOrienteesCorrespondre = new ArrayList<TuileOrientee>();
						tuilesOrienteesCorrespondre
								.addAll(caseExist.getTuileOrientee().terrainCorrespondre(direction.directionOpposee()));
					} else {
						tuilesOrienteesCorrespondre.retainAll(
								caseExist.getTuileOrientee().terrainCorrespondre(direction.directionOpposee()));
					}
				}
			}
			if (tuilesOrienteesCorrespondre == null) {
				throw new IllegalArgumentException("Aucune case adjacente n'a été retrouvée.");
			}
			// Élimination des tuiles orientées précédemment obtenues mais qui
			// sépareraient la terre (3e règle de pose des tuiles).
			Iterator<TuileOrientee> iterTuilesOrienteesCorrespondre = tuilesOrienteesCorrespondre.iterator();
			while (iterTuilesOrienteesCorrespondre.hasNext()) {
				TuileOrientee tuileOrientee = iterTuilesOrienteesCorrespondre.next();
				boolean aTerre = false;
				for (Direction direction : directionAdj) {
					if (tuileOrientee.aTerreDirection(direction)) {
						aTerre = true;
					}
				}
				if (!aTerre) {
					iterTuilesOrienteesCorrespondre.remove();
				}
			}
			// Enregistrement des tuiles orientées qui peuvent correspondre.
			if (!tuilesOrienteesCorrespondre.isEmpty()) {
				coordLigColAdjacentesTuilesOrientees.put(coordLigColAdj, tuilesOrienteesCorrespondre);
			}
		}
		return coordLigColAdjacentesTuilesOrientees;
	}

	/**
	 * Coordonnées (ligne et colonne) minimale ou maximale de toutes les cases
	 * où sont posées des tuiles.
	 * 
	 * @param minimum
	 *            Indique si on doit renvoyer la coordonnée minimale (maximale
	 *            sinon).
	 */
	public Point coordLigColMinMax(final boolean minimum) {
		if (cases.isEmpty()) {
			return null;
		} else {
			Point coordLigColMinMax = null;
			for (Point coordLigColCaseExist : cases.keySet()) {
				if (coordLigColMinMax == null) {
					coordLigColMinMax = new Point(coordLigColCaseExist);
				} else {
					if ((minimum && coordLigColCaseExist.x < coordLigColMinMax.x)
							|| (!minimum && coordLigColCaseExist.x > coordLigColMinMax.x)) {
						coordLigColMinMax.x = coordLigColCaseExist.x;
					}
					if ((minimum && coordLigColCaseExist.y < coordLigColMinMax.y)
							|| (!minimum && coordLigColCaseExist.y > coordLigColMinMax.y)) {
						coordLigColMinMax.y = coordLigColCaseExist.y;
					}
				}
			}
			return coordLigColMinMax;
		}
	}

	/**
	 * Installation d'un champ dans une ancienne case du plateau.
	 * 
	 * @param coordLigCol
	 *            Coordonnées (ligne et colonne) de la case du plateau.
	 * @param joueur
	 *            Joueur.
	 */
	public void installerChamp(Point coordLigCol, Joueur joueur) {
		if (coordLigCol == null) {
			throw new IllegalArgumentException("Les coordonnées doivent être renseignées.");
		}
		if (joueur == null) {
			throw new IllegalArgumentException("Le joueur est renseigné mais pas le pion.");
		}
		cases.put(coordLigCol, new Case(cases.get(coordLigCol).getTuileOrientee(), joueur, Pion.CHAMP));
	}

	/**
	 * Coordonnées (ligne et colonne) où un joueur peut installer un champ.
	 * 
	 * Remarque : certains tests sont inutiles car
	 * coordLigColCase.getValue().getTuileOrientee().installationPossibleDirection(direction)
	 * ==>
	 * coordLigColCase.getValue().getTuileOrientee().getTuileBase().estInstallationPossible()
	 * coordLigColCase.getValue().getTuileOrientee().installationPossibleDirection(direction)
	 * <==> getCase(coordLigColAdj).getTuileOrientee()
	 * .installationPossibleDirection(direction.directionOpposee())
	 * 
	 * @param joueur
	 *            Joueur.
	 * @return Coordonnées (ligne et colonne) où le joueur peut installer un
	 *         champ.
	 */
	public List<Point> coordLigColPossibleInstallerChamp(final Joueur joueur) {
		if (joueur == null) {
			throw new IllegalArgumentException("Le joueur est renseigné mais pas le pion.");
		}
		List<Point> coordLigColPossibleInstallerChamp = new ArrayList<Point>();
		for (Entry<Point, Case> coordLigColCase : cases.entrySet()) {
			if (coordLigColCase.getValue().estInoccupee()) {
				boolean estPossibleInstallerChamp = false;
				for (Direction direction : Direction.values()) {
					Point coordLigColAdj = direction.coordLigColDestination(coordLigColCase.getKey());
					if (getCase(coordLigColAdj) != null && !getCase(coordLigColAdj).estInoccupee()
							&& getCase(coordLigColAdj).getJoueur().equals(joueur)
							&& coordLigColCase.getValue().getTuileOrientee().getTuileBase().estInstallationPossible()
							&& coordLigColCase.getValue().getTuileOrientee().installationPossibleDirection(direction)
							&& getCase(coordLigColAdj).getTuileOrientee()
									.installationPossibleDirection(direction.directionOpposee())) {
						estPossibleInstallerChamp = true;
					}
				}
				if (estPossibleInstallerChamp) {
					coordLigColPossibleInstallerChamp.add(coordLigColCase.getKey());
				}
			}
		}
		return coordLigColPossibleInstallerChamp;
	}

	/**
	 * Sommets représentant les joueurs (ou null si la case ne contient pas de
	 * pion) pour les différents graphes.
	 * 
	 * @return Sommets représentant les joueurs (ou null si la case ne contient
	 *         pas de pion) pour les différents graphes.
	 */
	public Map<Point, Joueur> sommetsGraphes() {
		Map<Point, Joueur> sommetsGraphes = new HashMap<Point, Joueur>();
		for (Entry<Point, Case> coordLigColCase : cases.entrySet()) {
			sommetsGraphes.put(coordLigColCase.getKey(), coordLigColCase.getValue().getJoueur());
		}
		return sommetsGraphes;
	}

	/**
	 * Graphe des tuiles du plateau en considérant les terres arables durant la
	 * phase de découverte.
	 * 
	 * @return Graphe des tuiles du plateau en considérant les terres arables
	 *         durant la phase de découverte.
	 */
	public abstract Map<Point, Map<Point, Boolean>> grapheDecouverteTerresArables();

	/**
	 * Export, au format Graphviz, du graphe durant la phase de colonisation des
	 * pions reliés entre eux, de chaque couleur, du plateau en considérant les
	 * terres arables.
	 */
	public abstract void exportGvColonisation();

}
