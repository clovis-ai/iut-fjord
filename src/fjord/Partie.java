/**
 * Partie.
 */
package fjord;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

import static fjord.Couleur.CLAIR;

/**
 * Partie.
 *
 * @author Olivier
 */
public class Partie {

    /**
     * Nombre de joueurs.
     */
    public static final int NB_JOUEURS = Couleur.values().length;

    /**
     * Nombre de manches.
     */
    public static final int NB_MANCHES = 3;

    /**
     * Manches.
     */
    private Manche[] manches;

    /**
     * Manche en cours.
     */
    private Manche mancheEnCours;

    /**
     * Nombre de victoire.
     */

    private static int nbVictoire = 0;

    private static int nbDefaite = 0;

    private static int nbMancheG = 0;

    private static int nbMancheP = 0;

    private static int nbMatchNul = 0;

    private static int mancheGTotale = 0;

    private static int manchePTotale = 0;

    /**
     * Joueurs de la partie.
     */
    private List<Joueur> joueurs;

    /**
     * Création d'une partie.
     */
    public Partie() {
        // Joueurs.
        joueurs = new ArrayList<Joueur>();
        for (Couleur couleur : Couleur.values()) {
            if (couleur.equals(Couleur.FONCE)) {
                joueurs.add(new IA(couleur));
            } else {
                joueurs.add(new Joueur(couleur));
            }
        }
        nbMatchNul = 0;
    }

    /**
     * Joueurs de la partie.
     *
     * @return Joueurs de la partie.
     */
    public List<Joueur> getJoueurs() {
        return joueurs;
    }

    /**
     * Manche en cours.
     *
     * @return Manche en cours.
     */
    public Manche getMancheEnCours() {
        return mancheEnCours;
    }

    /**
     * Manches.
     * <p>
     * Attention : cette méthode a été créée à cause des tests.
     *
     * @return Manches
     */
    public Manche[] getManches() {
        return manches;
    }

    /**
     * Manches.
     * <p>
     * Attention : cette méthode a été créée à cause des tests.
     *
     * @param manches
     */
    public void setManches(Manche[] manches) {
        this.manches = manches;
    }

    /**
     * Gestion des tours de jeu de la partie.
     */
    private void toursJeu() throws Exception {
        toursJeuDebut();
        toursJeuEnCours();
        toursJeuFin();
    }

    /**
     * Début de la partie.
     */
    private void toursJeuDebut() {
        System.out.println("Début de la partie.");
    }

    /**
     * Gestion des tours de jeu de la partie.
     */
    private void toursJeuEnCours() throws Exception {
        // Manches.
        manches = new Manche[NB_MANCHES];
        for (int indManches = 0; indManches < NB_MANCHES; ++indManches) {
            System.out.println(" " + (indManches == 0 ? "1re" : +(indManches + 1) + "e") + " manche.");
            mancheEnCours = new Manche(this, getPremierJoueurManche(indManches));
            manches[indManches] = mancheEnCours;
            mancheEnCours.toursJeu();
        }
    }

    /**
     * Fin de la partie.
     */
    private void toursJeuFin() {
        // Calcul des points de victoire cumulés des joueurs.
        Map<Joueur, Integer> pointsVictoireCumules = pointsVictoireCumules();
        for (Entry<Joueur, Integer> joueurNbPointsVictoireCumules : pointsVictoireCumules.entrySet()) {
            System.out.println(" Le joueur " + joueurNbPointsVictoireCumules.getKey().getNom() + " a obtenu "
                    + joueurNbPointsVictoireCumules.getValue() + " point(s) de victoire cumulés pour la partie.");
        }
        // Joueur gagnant.
        Joueur joueurGagnant = getJoueurGagnant(pointsVictoireCumules);
        if (joueurGagnant == null) {
            System.out.println(" Aucun gagnant pour cette partie.");
        } else {
            System.out.println(" Le gagnant de cette partie est " + joueurGagnant.getNom() + ".");
        }
        // Enregistrement des scores.
        toursJeuFinEnregScores(pointsVictoireCumules, joueurGagnant);

        // Fin.
        System.out.println("Fin de la partie.");
    }

    /**
     * Enregistrement des scores de la partie.
     *
     * @param pointsVictoireCumules Points de victoire cumulés au cours de la partie pour chaque
     *                              joueur.
     * @param joueurGagnant         Joueur gagnant de la partie
     */
    private void toursJeuFinEnregScores(final Map<Joueur, Integer> pointsVictoireCumules, final Joueur joueurGagnant) {
        String toursJeuFinEnregScores = "";
        final String SEPARATEUR_JOUEURS = " ~ ";
        final String TERMINATEUR_MANCHE = " ; ";
        final String SEPARATEUR_PARTIE_MANCHES = " = ";
        final String INDICATEUR_GAGNANT = " (+)";
        // Scores et gagnant de la partie.
        for (int indJoueurs = 0; indJoueurs < joueurs.size(); ++indJoueurs) {
            if (indJoueurs > 0) {
                toursJeuFinEnregScores += SEPARATEUR_JOUEURS;
            }
            toursJeuFinEnregScores += joueurs.get(indJoueurs).getCouleur() + " "
                    + pointsVictoireCumules.get(joueurs.get(indJoueurs)).intValue();
            if (joueurs.get(indJoueurs).equals(joueurGagnant)) {
                toursJeuFinEnregScores += INDICATEUR_GAGNANT;
            }
        }
        // Scores et gagnants des manches.
        toursJeuFinEnregScores += SEPARATEUR_PARTIE_MANCHES;
        for (Manche manche : manches) {
            for (int indJoueurs = 0; indJoueurs < joueurs.size(); ++indJoueurs) {
                if (indJoueurs > 0) {
                    toursJeuFinEnregScores += SEPARATEUR_JOUEURS;
                }
                toursJeuFinEnregScores += manche.getPointsVictoire().get(joueurs.get(indJoueurs));
                if (joueurs.get(indJoueurs).equals(manche.getJoueurGagnant())) {
                    toursJeuFinEnregScores += INDICATEUR_GAGNANT;
                    if (joueurs.get(indJoueurs).getCouleur().equals(Couleur.FONCE)) {
                        nbMancheG++;
                    }else{
                        nbMancheP++;
                    }
                }
            }
            toursJeuFinEnregScores += TERMINATEUR_MANCHE;
        }
        toursJeuFinEnregScores += "\n";
        // Affichage.
        System.out.print(" " + toursJeuFinEnregScores);
        // Enregistrement (dans un fichier).
        final String NOM_FICHIER_SCORES = "Fjord_scores.txt";
        final File fic = new File(NOM_FICHIER_SCORES);
        try {
            fic.createNewFile();
            final FileWriter ecrivainFic = new FileWriter(fic, true);
            try {
                ecrivainFic.write(toursJeuFinEnregScores);
                ecrivainFic.write("Pourcentage de victoire : " + (nbVictoire / NB_MANCHES) * 100 + "\n");
                System.out.println("Pourcentage de victoire : " + (nbVictoire / NB_MANCHES) * 100 + "\n");
                ecrivainFic.flush();
            } finally {
                ecrivainFic.close();
            }
        } catch (IOException e) {
            System.err.println("Impossible d'écrire dans le fichier " + NOM_FICHIER_SCORES + ".");
        } catch (Exception e) {
            System.err.println("Impossible de créer le fichier " + NOM_FICHIER_SCORES + ".");
        }
    }

    /**
     * Premier joueur de la manche.
     * <p>
     * Le perdant de la manche précédente commence la suivante ; en cas
     * dégalité, et aussi pour la première manche, le premier joueur est tiré
     * au sort.
     * <p>
     * Remarque : codé pour un nombre quelconque (0, 1, 2 ou plus) de joueurs.
     * Le perdant est alors celui qui suit le gagnant.
     * <p>
     * Attention : private --> public à cause des tests.
     *
     * @param indManches Indice des manches.
     * @return Premier joueur de la manche.
     */
    public Joueur getPremierJoueurManche(final int indManches) {
        if (indManches != 0) {
            Joueur joueurGagnantManchePrecedente = manches[indManches - 1].getJoueurGagnant();
            if (joueurGagnantManchePrecedente != null) {
                // Le premier joueur de la manche est le perdant (c.-à-d. le
                // joueur suivant le gagnant) de la manche précédente.
                return joueurSuivant(joueurGagnantManchePrecedente);
            }
        }
        // Le premier joueur de la manche a été tiré au sort.
        return joueurs.get((new Random()).nextInt(joueurs.size()));
    }

    /**
     * Joueur suivant (devenant le nouveau joueur actif).
     * <p>
     * Remarque : codé pour un nombre quelconque (0, 1, 2 ou plus) de joueurs.
     *
     * @param joueurActif Joueur actif.
     * @return Joueur suivant (devenant le nouveau joueur actif).
     */
    public Joueur joueurSuivant(final Joueur joueurActif) {
        if (joueurActif == null) {
            throw new IllegalArgumentException("Le joueur actif doit être renseignée.");
        }
        int indJoueurActif = joueurs.indexOf(joueurActif);
        if (indJoueurActif == -1) {
            throw new IllegalArgumentException("Il n'y pas ce joueur actif.");
        }
        if (indJoueurActif == joueurs.size() - 1) {
            return joueurs.get(0);
        } else {
            return joueurs.get(indJoueurActif + 1);
        }
    }

    /**
     * Calcul du cumul des points de victoire en fin de la partie.
     * <p>
     * Attention : private --> public à cause des tests.
     *
     * @return Points de victoire cumulés.
     */
    public Map<Joueur, Integer> pointsVictoireCumules() {
        Map<Joueur, Integer> pointsVictoireCumules = new HashMap<Joueur, Integer>();
        if (manches != null) {
            for (Joueur joueur : joueurs) {
                int nbPointsVictoireCumules = Arrays.stream(manches)
                        .filter(manche -> manche.getPointsVictoire().get(joueur) != null)
                        .map(manche -> manche.getPointsVictoire().get(joueur)).reduce(0, (a, b) -> a + b);
                if (nbPointsVictoireCumules > 0) {
                    pointsVictoireCumules.put(joueur, nbPointsVictoireCumules);
                }
            }
        }
        return pointsVictoireCumules;
    }

    /**
     * Joueur gagnant de la partie.
     * <p>
     * Attention : private --> public à cause des tests.
     * <p>
     * N. B. : le gagnant est celui qui aura cumulé le plus de points de
     * victoire (de toutes les manches) ; en cas dégalité, le gagnant est celui
     * qui a remporté le plus de manches ; en cas de nouvelle égalité, il ny a
     * aucun gagnant.
     * <p>
     * Remarque : codé pour un nombre quelconque (0, 1, 2 ou plus) de joueurs.
     *
     * @param pointsVictoireCumules Points de victoire cumulés.
     * @return Joueur gagnant de la partie.
     */
    public Joueur getJoueurGagnant(Map<Joueur, Integer> pointsVictoireCumules) {
        // Recherche de l'unicité du maximum des points de victoire cumulés.
        List<Joueur> joueursMaxPointsVictoire = Partie.joueursValMax(pointsVictoireCumules);
        if (joueursMaxPointsVictoire.size() == 1) {
            // Un gagnant qui a seul maximisé le nombre de points de victoire
            // cumulés.
            return joueursMaxPointsVictoire.get(0);
        } else if (joueursMaxPointsVictoire.isEmpty()) {
            // Aucun gagnant.
            return null;
        } else {
            // Plusieurs joueurs qui maximisent le nombre de points de victoire
            // et donc à départager selon le nombre de manches gagnées.
            Map<Joueur, Integer> nbManchesGagnees = new HashMap<Joueur, Integer>();
            for (Joueur joueur : joueursMaxPointsVictoire) {
                nbManchesGagnees.put(joueur, 0);
            }
            for (Manche manche : manches) {
                Joueur joueurGagnantManche = manche.getJoueurGagnant();
                if (joueurGagnantManche != null && joueursMaxPointsVictoire.contains(joueurGagnantManche)) {
                    nbManchesGagnees.put(joueurGagnantManche, nbManchesGagnees.get(joueurGagnantManche) + 1);
                }
            }
            // Recherche de l'unicité du nombre maximum de manches gagnées.
            List<Joueur> joueursMaxManches = Partie.joueursValMax(nbManchesGagnees);
            if (joueursMaxManches.size() == 1) {
                // Un gagnant qui a maximisé le nombre de points de victoire
                // cumulés et qui a seul remporté le plus de manches.
                return joueursMaxManches.get(0);
            } else {
                // Parmi les gagnants qui ont maximisé le nombre de points de
                // victoire cumulés, plusieurs ont aussi remporté le même nombre
                // de manches.
                return null;
            }
        }
    }

    /**
     * Joueurs qui ont la valeur (entier naturel) maximale.
     *
     * @param valeurs Valeur de chaque joueur.
     * @return Joueurs qui ont la valeur (entier naturel) maximale.
     */
    public static List<Joueur> joueursValMax(final Map<Joueur, Integer> valeurs) {
        if (valeurs == null) {
            throw new IllegalArgumentException("L'information sur les valeurs des joueurs doit être renseignée.");
        }
        List<Joueur> joueursValMax = new ArrayList<Joueur>();
        int maxValeurs = -1; // Moins que le minimum des entiers naturels.
        // Priorité opérateurs ==> init° inutile.
        for (Entry<Joueur, Integer> joueurValeurs : valeurs.entrySet()) {
            if (joueursValMax.isEmpty() || joueurValeurs.getValue() > maxValeurs) {
                joueursValMax.clear();
                joueursValMax.add(joueurValeurs.getKey());
                maxValeurs = joueurValeurs.getValue();
            } else if (joueurValeurs.getValue() == maxValeurs) {
                joueursValMax.add(joueurValeurs.getKey());
            }
        }
        return joueursValMax;
    }


    /**
     * Programme principal.
     *
     * @param args Arguments.
     */
    public static void main(String[] args) throws Exception {
        final int NB_ITERATIONS = 1000; // 1000;
        for (int cpt = 0; cpt <= NB_ITERATIONS; ++cpt) {
            (new Partie()).toursJeu();
            if(nbMancheP < nbMancheG){
                nbVictoire++;
            }else if(nbMancheP > nbMancheG){
                nbDefaite++;
            }else if (nbMancheP == nbMancheG){
                nbMatchNul++;
            }

            mancheGTotale+=nbMancheG;
            manchePTotale+=nbMancheP;

            nbMancheG = 0;
            nbMancheP = 0;
        }
        System.out.println("\nCompte de victoires:");
        System.out.println("Enemi: " + nbDefaite);
        System.out.println("IA: " + nbVictoire);
        System.out.println("Nombre de manche Gagnées : " + mancheGTotale);
        System.out.println("Nombre de manche Perdus : " + manchePTotale);
        System.out.println("Match nul : " + nbMatchNul);
    }

}
