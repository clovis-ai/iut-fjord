/**
 * Tuile (de terrain) de départ.
 */
package fjord;

import java.awt.Point;

/**
 * Tuile (de terrain) de départ.
 * 
 * @author Olivier
 */

/* ~~~~~~A~~~~~~~~~~ */
/* ~~~~~/~~\~~~~~~~~ */
/* ~~~~/~~~~\~~~~~~~ */
/* ~~~~B~~~~~C~~~~~~ */
/* ~~~~|~0,0~|~~~~~~ */
/* ~~~~|~_3~~|~~~~~~ */
/* ~~~~A~~~~~C~~~~~~ */
/* ~~~~/\~~~~/\~~~~~ */
/* ~~~/~~\~~/~~\~~~~ */
/* ~~A~~~~~A~~~~~A~~ */
/* ~~|~1,0~|~1,1~|~~ */
/* ~~|~_1~~|~_2~~|~~ */
/* ~~A~~~~/A~~~~~A~~ */
/* ~~~\~~/~~\~~/~~~~ */
/* ~~~~\/~~~~\/~~~~~ */
/* ~~~~A~~~~~A~~~~~~ */

public enum TuileDepart {

	/**
	 * Première tuile de départ.
	 */
	DEPART_1(Tuile.AAAAAA, 1, TuileOrientee.AAAAAA, new Point(1, 0)),
	/**
	 * Deuxième tuile de départ.
	 */
	DEPART_2(Tuile.AAAAAC, 1, TuileOrientee.CAAAAA, new Point(1, 1)),
	/**
	 * Troisième tuile de départ.
	 */
	DEPART_3(Tuile.AACCAB, 1, TuileOrientee.ABAACC, new Point(0, 0));

	/**
	 * Tuile de départ.
	 */
	private final Tuile tuile;

	/**
	 * Nombre d'exemplaires disponibles au départ.
	 */
	private final int nbExemplaires;

	/**
	 * Tuile orientée de départ.
	 */
	private final TuileOrientee tuileOrientee;

	/**
	 * Coordonnées (ligne et colonne) de la tuile de départ.
	 */
	private final Point coordLigCol;

	/**
	 * Nombre total d'exemplaires disponibles au départ.
	 */
	public static final int TOTAL_NB_EXEMPLAIRES = 3;

	/**
	 * Création d'une tuile.
	 * 
	 * @param tuile
	 *            Tuile de départ.
	 * @param nbExemplaires
	 *            Nombre d'exemplaires disponibles au départ.
	 * @param tuileOrientee
	 *            Tuile orientée de départ.
	 * @param coordLigCol
	 *            Coordonnées (ligne et colonne) de la tuile de départ.
	 */
	private TuileDepart(final Tuile tuile, final int nbExemplaires, final TuileOrientee tuileOrientee,
			final Point coordLigCol) {
		this.tuile = tuile;
		this.nbExemplaires = nbExemplaires;
		this.tuileOrientee = tuileOrientee;
		this.coordLigCol = coordLigCol;
	}

	/**
	 * Tuile de départ.
	 * 
	 * @return Tuile de départ.
	 */
	public Tuile getTuile() {
		return tuile;
	}

	/**
	 * Nombre d'exemplaires disponibles au départ.
	 * 
	 * @return Nombre d'exemplaires disponibles au départ.
	 */
	public int getNbExemplaires() {
		return nbExemplaires;
	}

	/**
	 * Tuile orientée de départ.
	 * 
	 * @return Tuile orientée de départ.
	 */
	public TuileOrientee getTuileOrientee() {
		return tuileOrientee;
	}

	/**
	 * Coordonnées (ligne et colonne) de la tuile de départ.
	 * 
	 * @return Coordonnées (ligne et colonne) de la tuile de départ.
	 */
	public Point getCoordLigCol() {
		return coordLigCol;
	}

}
