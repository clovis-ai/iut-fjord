/**
 * Manche.
 */
package fjord;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;

/**
 * Manche.
 *
 * @author Olivier
 */
public class Manche {

    /**
     * État de la manche.
     */
    private EtatManche etat;

    /**
     * Partie en cours.
     */
    private Partie partieEnCours;

    /**
     * Pioches des tuiles faces cachées et visibles.
     */
    private List<TuileAPiocher> pioches;

    /**
     * Plateau.
     */
    private Plateau plateau;

    /**
     * Premier joueur.
     */
    private Joueur premierJoueur;

    /**
     * Joueur actif.
     */
    private Joueur joueurActif;

    /**
     * Points de victoire des joueurs.
     */
    private Map<Joueur, Integer> pointsVictoire;

    /**
     * Création d'une manche.
     *
     * @param partieEnCours Partie en cours.
     * @param premierJoueur Premier joueur.
     */
    public Manche(final Partie partieEnCours, final Joueur premierJoueur) {
        if (partieEnCours == null) {
            throw new IllegalArgumentException("La partie en cours doit être renseignée.");
        }
        if (premierJoueur == null) {
            throw new IllegalArgumentException("Le premier joueur doit être renseigné.");
        }
        this.partieEnCours = partieEnCours;
        this.premierJoueur = premierJoueur;
        plateau = new Plateau_ETD(); // Tests ==> pas dans poserTuilesDepart().
    }

    /**
     * Plateau.
     *
     * @return Plateau.
     */
    public Plateau getPlateau() {
        return plateau;
    }

    /**
     * Points de victoire des joueurs.
     *
     * @return Points de victoire des joueurs.
     */
    public Map<Joueur, Integer> getPointsVictoire() {
        return pointsVictoire;
    }

    /**
     * Joueur gagnant de la manche.
     * <p>
     * N. B. : gagnant ssi le nombre maximum de points de victoire est détenu
     * par un seul joueur (ni aucun, ni deux ou plus).
     * <p>
     * Remarque : codé pour un nombre quelconque (0, 1, 2 ou plus) de joueurs.
     *
     * @return Joueur gagnant de la manche.
     */
    public Joueur getJoueurGagnant() {
        if (pointsVictoire == null) {
            return null;
        } else {
            List<Joueur> joueursMaxPointsVictoire = Partie.joueursValMax(pointsVictoire);
            return joueursMaxPointsVictoire.size() == 1 ? joueursMaxPointsVictoire.get(0) : null;
        }
    }

    /**
     * Gestion des tours de jeu de la manche.
     */
    public void toursJeu() throws Exception {
        if (partieEnCours == null) {
            throw new IllegalArgumentException("La partie en cours doit être renseignée.");
        }
        if (premierJoueur == null) {
            throw new IllegalArgumentException("Le premier joueur doit être renseigné.");
        }
        toursJeuDebut();
        toursJeuEnCours();
        toursJeuFin();
    }

    /**
     * Début de la manche.
     */
    private void toursJeuDebut() {
        System.out.println(" Début d'une manche.");
        etat = EtatManche.DEBUT;
        creerPioches();
        poserTuilesDepart();
        if (plateau.getCases().size() != TuileDepart.TOTAL_NB_EXEMPLAIRES) {
            throw new IllegalArgumentException("Le nombre de tuiles posées au départ n'est pas correct.");
        }
        System.out.println("  " + plateau.getCases().size() + " tuiles ont été posées au départ.");
        if (getPioche(false, true).size() != 0) {
            throw new IllegalArgumentException("La pioche des tuiles faces visibles n'est pas vide.");
        }
        System.out.println("  " + getPioche(true, true).size()
                + " tuiles sont dans la pioche de celles faces cachées et aucune tuile n'est dans la pioche de celles faces visibles.");
        joueurActif = premierJoueur;
        etat = EtatManche.DECOUVERTE;
    }

    /**
     * Création des pioches (faces cachées et visibles).
     */
    private void creerPioches() {
        pioches = new ArrayList<TuileAPiocher>();
        for (Tuile tuile : Tuile.values()) {
            for (int exemplaire = 0; exemplaire < tuile.getNbExemplaires(); ++exemplaire) {
                pioches.add(new TuileAPiocher(tuile, true));
            }
        }
    }

    /**
     * Pose des tuiles de départ posées sur le plateau (et supprimées de la
     * pioche).
     */
    private void poserTuilesDepart() {
        for (TuileDepart tuileDepart : TuileDepart.values()) {
            plateau.poserTuile(tuileDepart.getCoordLigCol(), tuileDepart.getTuileOrientee(), null, null);
            TuileAPiocher tuileAPiocher = getTuileAPiocher(tuileDepart.getTuile());
            if (tuileAPiocher == null) {
                throw new IllegalArgumentException(
                        "Une tuile à piocher n'a pas été trouvée dans les pioches à partir de la tuile.");
            }
            pioches.remove(tuileAPiocher);
        }
    }

    /**
     * Tuile à piocher obtenue des pioches, à partir d'une tuile.
     *
     * @param tuile Tuile.
     * @return Tuile à piocher obtenue des pioches, à partir d'une tuile.
     */
    private TuileAPiocher getTuileAPiocher(final Tuile tuile) {
        for (TuileAPiocher tuileAPiocher : pioches) {
            if (tuileAPiocher.getTuile().equals(tuile)) {
                return tuileAPiocher;
            }
        }
        return null;
    }

    /**
     * Pioche des tuiles, avec ou sans répétition, faces cachées ou visibles.
     *
     * @param estCachee      Indique si on veut la pioche des tuiles cachées (visibles
     *                       sinon).
     * @param avecRepetition Indique si on veut les tuiles avec (sans sinon) répétition.
     * @return Pioche des tuiles, avec ou sans répétition, faces cachées ou
     * visibles.
     */
    private List<Tuile> getPioche(final boolean estCachee, final boolean avecRepetition) {
        List<Tuile> getPioche = new ArrayList<Tuile>();
        if (avecRepetition) {
            for (TuileAPiocher tuileAPiocher : pioches) {
                if (tuileAPiocher.estCachee() == estCachee) {
                    getPioche.add(tuileAPiocher.getTuile());
                }
            }
        } else {
            Map<Tuile, TuileAPiocher> piocheSansRepetition = new HashMap<Tuile, TuileAPiocher>();
            for (TuileAPiocher tuileAPiocher : pioches) {
                if (tuileAPiocher.estCachee() == estCachee) {
                    piocheSansRepetition.put(tuileAPiocher.getTuile(), tuileAPiocher);
                }
            }
            for (TuileAPiocher tuileAPiocher : piocheSansRepetition.values()) {
                getPioche.add(tuileAPiocher.getTuile());
            }
        }
        return getPioche;
    }

    /**
     * Suppression d'une tuile de l'une des pioches des tuiles faces cachées ou
     * visibles.
     *
     * @param tuile     Tuile à supprimer.
     * @param estCachee Indique si on supprimer dans la pioche des tuiles cachées
     *                  (visibles sinon).
     */
    private void removePioche(final Tuile tuile, final boolean estCachee) {
        boolean suppressionEffectuee = false;
        Iterator<TuileAPiocher> iterPioches = pioches.iterator();
        while (!suppressionEffectuee && iterPioches.hasNext()) {
            TuileAPiocher tuileAPiocher = iterPioches.next();
            if (tuileAPiocher.getTuile().equals(tuile) && tuileAPiocher.estCachee() == estCachee) {
                iterPioches.remove();
                suppressionEffectuee = true;
            }
        }
        if (!suppressionEffectuee) {
            throw new IllegalArgumentException("La tuile piochée parmi celles cachées n'a pas été supprimée.");
        }
    }

    /**
     * Gestion des tours de jeu de la manche.
     */
    private void toursJeuEnCours() throws Exception {
        System.out.println("  Le premier joueur de la phase de découverte est " + joueurActif.getNom() + ".");
        System.out.println("  Début de la phase de découverte.");
        while (etat.equals(EtatManche.DECOUVERTE)) {
            toursJeuEnCoursDecouverte1Tour();
        }
        if (!getPioche(true, true).isEmpty()) {
            throw new IllegalArgumentException("La pioche des tuiles faces cachées n'est pas vide.");
        }
        if (plateau.getCases().size() + pioches.size() != Tuile.TOTAL_NB_EXEMPLAIRES) {
            throw new IllegalArgumentException("Des tuiles ont été perdues.");
        }
        System.out.println("   " + (plateau.getCases().size() - TuileDepart.TOTAL_NB_EXEMPLAIRES)
                + " tuiles ont été posées en plus des " + TuileDepart.TOTAL_NB_EXEMPLAIRES + " de départ.");
        if (getPioche(false, true).size() == 0) {
            System.out.println("   Aucune tuile de la pioche de celles visibles n'est à mettre de côté.");
        } else if (getPioche(false, true).size() == 1) {
            System.out.println("   Une seule tuile de la pioche de celles visibles est mise de côté.");
        } else {
            System.out.println("   " + getPioche(false, true).size()
                    + " tuiles de la pioche de celles visibles sont mises de côté.");
        }
        pioches.clear();
        System.out.println("  Fin de la phase de découverte.");
        System.out.println("  Le premier joueur de la phase de colonisation est " + joueurActif.getNom() + ".");
        System.out.println("  Début de la phase de colonisation.");
        toursJeuEnCoursColonisation();
        plateau.exportGvColonisation();
        System.out.println("  Fin de la phase de colonisation.");
    }

    /**
     * Gestion d'un tour de jeu du joueur actif durant la phase de découverte de
     * la manche.
     */
    private void toursJeuEnCoursDecouverte1Tour() {
        if (getPioche(true, true).isEmpty()) {
            throw new IllegalArgumentException("La pioche des tuiles faces cachées est vide.");
        }
        System.out.println("   Le joueur actif est " + joueurActif.getNom() + ".");
        // Récupération de toutes les actions possibles.
        final Map<Point, List<TuileOrientee>> coordLigColAdjacentesTuilesOrientees = plateau
                .coordLigColAdjacentesTuilesOrientees();
        // Création de la pioche des tuiles, sans répétition, faces visibles qui
        // peuvent être posées.
        final List<Tuile> piocheSansRepetTuilesVisibles = getPioche(false, false);
        List<Tuile> piocheSansRepetTuilesVisiblesPossiblePoser = new ArrayList<Tuile>();
        for (Tuile tuileVisible : piocheSansRepetTuilesVisibles) {
            if (estTuilePossiblePoser(tuileVisible, coordLigColAdjacentesTuilesOrientees)) {
                piocheSansRepetTuilesVisiblesPossiblePoser.add(tuileVisible);
            }
        }
        // Pioche d'une tuile.
        Tuile tuilePiochee = null;
        while (tuilePiochee == null && etat.equals(EtatManche.DECOUVERTE)) {
            tuilePiochee = toursJeuEnCoursDecouverte1TourPiocheTuilesVisibles(
                    piocheSansRepetTuilesVisiblesPossiblePoser);
            if (tuilePiochee == null) {
                tuilePiochee = toursJeuEnCoursDecouverte1TourPiocheTuilesCachees(coordLigColAdjacentesTuilesOrientees);
            }
        }
        // Pose de la tuile piochée voire installation d'une hutte.
        if (tuilePiochee != null) {
            toursJeuEnCoursDecouverte1TourPose(tuilePiochee, coordLigColAdjacentesTuilesOrientees);
        }
        // Joueur suivant (même si la phase a changé car premierJoueur a été
        // actualisé).
        joueurActif = partieEnCours.joueurSuivant(joueurActif);
    }

    /**
     * Gestion de la pioche d'une tuile parmi celles faces visibles qui peuvent
     * être posées lors d'un tour de jeu du joueur actif durant la phase de
     * découverte de la manche.
     *
     * @param piocheSansRepetTuilesVisiblesPossiblePoser Pioche des tuiles sans répétition faces visibles qui peuvent
     *                                                   être posées.
     * @return Tuile piochée : soit une tuile choisie parmi celles de la pioche
     * des tuiles faces visibles qui peuvent être posées, soit null pour
     * demander qu'une tuile soit tirée aléatoirement parmi celles de la
     * pioche des tuiles faces cachées.
     */
    private Tuile toursJeuEnCoursDecouverte1TourPiocheTuilesVisibles(
            final List<Tuile> piocheSansRepetTuilesVisiblesPossiblePoser) {
        if (!piocheSansRepetTuilesVisiblesPossiblePoser.isEmpty()) {
            // Le joueur actif veut-il piocher une tuile parmi celles faces
            // visibles qui peuvent être posées ?
            Tuile tuilePiochee = joueurActif.jouerDecouvertePioche(piocheSansRepetTuilesVisiblesPossiblePoser);
            if (tuilePiochee != null && piocheSansRepetTuilesVisiblesPossiblePoser.contains(tuilePiochee)) {
                // Le joueur actif a pioché une tuile en la choisissant
                // parmi celles faces visibles qui peuvent être posées.
                System.out.println("    Le joueur actif a choisi la tuile " + tuilePiochee
                        + " de la pioche de celles faces visibles qui peuvent être posées.");
                removePioche(tuilePiochee, false);
                return tuilePiochee;
            }
        }
        return null;
    }

    /**
     * Gestion de la pioche d'une tuile parmi celles faces cachées lors d'un
     * tour de jeu du joueur actif durant la phase de découverte de la manche.
     *
     * @param coordLigColAdjacentesTuilesOrientees Coordonnées (ligne et colonne) de toutes les cases sans tuile
     *                                             où on peut poser des tuiles orientées.
     * @return Tuile piochée parmi celles faces cachées : soit qui peut être
     * posée, soit null sinon.
     */
    private Tuile toursJeuEnCoursDecouverte1TourPiocheTuilesCachees(
            final Map<Point, List<TuileOrientee>> coordLigColAdjacentesTuilesOrientees) {
        // Le joueur actif pioche une tuile en la tirant aléatoirement parmi
        // celles, avec répétition, faces cachées.
        final List<Tuile> piocheTuilesCachees = getPioche(true, true);
        Tuile tuilePiochee = piocheTuilesCachees.get((new Random()).nextInt(piocheTuilesCachees.size()));
        removePioche(tuilePiochee, true);
        // La tuile tirée aléatoirement parmi celles faces cachées peut-elle
        // être posée ?
        if (estTuilePossiblePoser(tuilePiochee, coordLigColAdjacentesTuilesOrientees)) {
            System.out.println("    Le joueur actif a tiré aléatoirement au sort la tuile " + tuilePiochee
                    + " de la pioche de celles faces cachées et qui peut être posée.");
            if (getPioche(true, true).isEmpty()) {
                // La pioche des tuiles faces cachées a été vidée.
                System.out.println(
                        "    La pioche des tuiles faces cachées est vide et le premier joueur de la phase de colonisation sera l'adversaire du joueur actif.");
                etat = EtatManche.COLONISATION;
                premierJoueur = partieEnCours.joueurSuivant(joueurActif);
            }
            return tuilePiochee;
        } else {
            System.out.println("    Le joueur actif a tiré aléatoirement au sort la tuile " + tuilePiochee
                    + " de la pioche de celles faces cachées mais qui ne peut pas être posée.");
            pioches.add(new TuileAPiocher(tuilePiochee, false));
            // Inutile de la rajouter à la pioche des tuiles, sans répétition,
            // faces visibles qui peuvent être posées.
            if (getPioche(true, true).isEmpty()) {
                // La pioche des tuiles faces cachées a été vidée.
                System.out.println(
                        "    La pioche des tuiles faces cachées est vide et le premier joueur de la phase de colonisation sera encore le joueur actif.");
                etat = EtatManche.COLONISATION;
                premierJoueur = joueurActif;
            }
            return null;
        }
    }

    /**
     * Gestion de la pose d'une tuile piochée, voire l'installation d'une hutte,
     * lors d'un tour de jeu du joueur actif durant la phase de découverte de la
     * manche.
     *
     * @param tuilePiochee                         Tuile piochée que le joueur actif veut poser.
     * @param coordLigColAdjacentesTuilesOrientees Coordonnées (ligne et colonne) de toutes les cases sans tuile
     *                                             où on peut poser des tuiles orientées.
     */
    private void toursJeuEnCoursDecouverte1TourPose(final Tuile tuilePiochee,
                                                    final Map<Point, List<TuileOrientee>> coordLigColAdjacentesTuilesOrientees) {
        // Coordonnée (ligne et colonne) où poser la tuile piochée.
        List<Point> coordLigColTuilePossiblePoser = new ArrayList<Point>();
        coordLigColTuilePossiblePoser = coordLigColTuilePossiblePoser(tuilePiochee,
                coordLigColAdjacentesTuilesOrientees);
        if (coordLigColTuilePossiblePoser.isEmpty()) {
            throw new IllegalArgumentException(
                    "Aucune coordonnée (ligne et colonne) n'est possible pour poser la tuile piochée.");
        }
        Point coordLigColTuilePoser = joueurActif.jouerDecouvertePoseCoordLigCol(coordLigColTuilePossiblePoser);
        if (coordLigColTuilePoser == null || !coordLigColTuilePossiblePoser.contains(coordLigColTuilePoser)) {
            coordLigColTuilePoser = coordLigColTuilePossiblePoser.get(0);
        }
        // Tuile orientée de la tuile piochée.
        List<TuileOrientee> tuilesOrienteesTuilePossiblePoser = new ArrayList<TuileOrientee>();
        tuilesOrienteesTuilePossiblePoser = tuilesOrienteesTuilePossiblePoser(tuilePiochee, coordLigColTuilePoser,
                coordLigColAdjacentesTuilesOrientees);
        if (tuilesOrienteesTuilePossiblePoser.isEmpty()) {
            throw new IllegalArgumentException(
                    "Aucune tuile orientée ne peut être posée pour la tuile piochée et la coordonnée.");
        }
        TuileOrientee tuileOrienteeTuilePoser = joueurActif
                .jouerDecouvertePoseTuileOrientee(tuilesOrienteesTuilePossiblePoser);
        if (tuileOrienteeTuilePoser == null || !tuilesOrienteesTuilePossiblePoser.contains(tuileOrienteeTuilePoser)) {
            tuileOrienteeTuilePoser = tuilesOrienteesTuilePossiblePoser.get(0);
        }
        // Éventuellement, installation d'une hutte.
        if (toursJeuEnCoursDecouverte1TourPoseHutteAInstaller(tuilePiochee)) {
            System.out.println("    Le joueur actif pose la tuile orientée " + tuileOrienteeTuilePoser + " en ("
                    + coordLigColTuilePoser.x + "," + coordLigColTuilePoser.y + ") et y installe une hutte.");
            plateau.poserTuile(coordLigColTuilePoser, tuileOrienteeTuilePoser, joueurActif, Pion.HUTTE);
        } else {
            System.out.println("    Le joueur actif pose la tuile orientée " + tuileOrienteeTuilePoser + " en ("
                    + coordLigColTuilePoser.x + "," + coordLigColTuilePoser.y + ") sans y installer de hutte.");
            plateau.poserTuile(coordLigColTuilePoser, tuileOrienteeTuilePoser, null, null);
        }
    }

    /**
     * Gestion de la pose d'une tuile piochée, indique si une hutte doit être
     * installée, lors d'un tour de jeu du joueur actif durant la phase de
     * découverte de la manche.
     *
     * @param tuilePiochee Tuile piochée que le joueur actif veut poser.
     */
    private boolean toursJeuEnCoursDecouverte1TourPoseHutteAInstaller(final Tuile tuilePiochee) {
        if (tuilePiochee.estInstallationPossible()
                && plateau.nbCasesOccupees(joueurActif, Pion.HUTTE) < Pion.HUTTE.getNbExemplaires()) {
            return joueurActif.jouerDecouverteInstallationHutte();
        }
        return false;
    }

    /**
     * Indique si la tuile peut être posée.
     *
     * @param tuilePiochee                         Tuile piochée que le joueur actif veut poser.
     * @param coordLigColAdjacentesTuilesOrientees Coordonnées (ligne et colonne) de toutes les cases sans tuile
     *                                             où on peut poser des tuiles orientées.
     * @return Indique si la tuile peut être posée.
     */
    private boolean estTuilePossiblePoser(final Tuile tuilePiochee,
                                          final Map<Point, List<TuileOrientee>> coordLigColAdjacentesTuilesOrientees) {
        for (List<TuileOrientee> tuilesOrientees : coordLigColAdjacentesTuilesOrientees.values()) {
            for (TuileOrientee tuileOrientee : tuilesOrientees) {
                if (tuileOrientee.getTuileBase().equals(tuilePiochee)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Coordonnées (ligne et colonne) de toutes les cases sans tuile où on peut
     * poser la tuile piochée.
     *
     * @param tuilePiochee                         Tuile piochée que le joueur actif veut poser.
     * @param coordLigColAdjacentesTuilesOrientees Coordonnées (ligne et colonne) de toutes les cases sans tuile
     *                                             où on peut poser des tuiles orientées.
     * @return Coordonnées (ligne et colonne) de toutes les cases sans tuile où
     * on peut poser la tuile piochée.
     */
    private List<Point> coordLigColTuilePossiblePoser(final Tuile tuilePiochee,
                                                      final Map<Point, List<TuileOrientee>> coordLigColAdjacentesTuilesOrientees) {
        List<Point> coordLigColTuilePossiblePoser = new ArrayList<Point>();
        for (Entry<Point, List<TuileOrientee>> coordLigColAdjTuilesOrientees : coordLigColAdjacentesTuilesOrientees
                .entrySet()) {
            boolean estTuilePossiblePoser = false;
            for (TuileOrientee tuileOrientee : coordLigColAdjTuilesOrientees.getValue()) {
                if (tuileOrientee.getTuileBase().equals(tuilePiochee)) {
                    estTuilePossiblePoser = true;
                }
            }
            if (estTuilePossiblePoser) {
                coordLigColTuilePossiblePoser.add(coordLigColAdjTuilesOrientees.getKey());
            }
        }
        return coordLigColTuilePossiblePoser;
    }

    /**
     * Tuiles orientées de la tuile piochée que l'on peut poser à une coordonnée
     * (ligne et colonne).
     *
     * @param tuilePiochee                         Tuile piochée que le joueur actif veut poser.
     * @param coordLigColTuilePoser                Coordonnée (ligne et colonne) où on doit poser la tuile.
     * @param coordLigColAdjacentesTuilesOrientees Coordonnées (ligne et colonne) de toutes les cases sans tuile
     *                                             où on peut poser des tuiles orientées.
     * @return Tuiles orientées de la tuile piochée que l'on peut poser à une
     * coordonnée (ligne et colonne).
     */
    private List<TuileOrientee> tuilesOrienteesTuilePossiblePoser(final Tuile tuilePiochee,
                                                                  final Point coordLigColTuilePoser,
                                                                  final Map<Point, List<TuileOrientee>> coordLigColAdjacentesTuilesOrientees) {
        List<TuileOrientee> tuilesOrienteesTuilePossiblePoser = new ArrayList<TuileOrientee>();
        for (TuileOrientee tuileOrientee : coordLigColAdjacentesTuilesOrientees.get(coordLigColTuilePoser)) {
            if (tuileOrientee.getTuileBase().equals(tuilePiochee)
                    && !tuilesOrienteesTuilePossiblePoser.contains(tuileOrientee)) {
                tuilesOrienteesTuilePossiblePoser.add(tuileOrientee);
            }
        }
        return tuilesOrienteesTuilePossiblePoser;
    }

    /**
     * Gestion de la phase de colonisation de la manche.
     */
    private void toursJeuEnCoursColonisation() throws Exception {
        if (!etat.equals(EtatManche.COLONISATION)) {
            throw new IllegalArgumentException("L'état de la manche n'est pas correct.");
        }
        Map<Point, Joueur> sommetsGraphes = plateau.sommetsGraphes();
        final Map<Point, Map<Point, Boolean>> grapheDecouverteTerresArables = plateau.grapheDecouverteTerresArables();
        List<Joueur> joueursActifs = new ArrayList<Joueur>();
        joueursActifs.addAll(partieEnCours.getJoueurs());
        while (!joueursActifs.isEmpty()) {
            if (joueursActifs.contains(joueurActif)) {
                boolean joueurActifASupprimer = false;
                System.out.println("   Le joueur actif est " + joueurActif.getNom() + ".");
                if (plateau.nbCasesOccupees(joueurActif, Pion.CHAMP) < Pion.CHAMP.getNbExemplaires()) {
                    // Création des coordonnées (ligne et colonne) où un champ
                    // peut être installé.
                    List<Point> coordLigColPossibleInstallerChamp = new ArrayList<Point>();
                    coordLigColPossibleInstallerChamp = plateau.coordLigColPossibleInstallerChamp(joueurActif);
                    if (coordLigColPossibleInstallerChamp.isEmpty()) {
                        joueurActifASupprimer = true;
                    } else {
                        // Installation d'un champ.
                        List<Point> coordLigColPossibleInstallerChampAdversaire = new ArrayList<Point>();
                        coordLigColPossibleInstallerChampAdversaire = plateau
                                .coordLigColPossibleInstallerChamp(partieEnCours.joueurSuivant(joueurActif));
                        Point coordLigColInstallerChamp = joueurActif.jouerColonisationInstallationChamp(
                                coordLigColPossibleInstallerChamp, coordLigColPossibleInstallerChampAdversaire,
                                sommetsGraphes, grapheDecouverteTerresArables);
                        if (coordLigColInstallerChamp == null
                                || !coordLigColPossibleInstallerChamp.contains(coordLigColInstallerChamp)) {
                            coordLigColInstallerChamp = coordLigColPossibleInstallerChamp.get(0);
                        }
                        System.out.println("    Le joueur actif pose un champ en (" + coordLigColInstallerChamp.x + ","
                                + coordLigColInstallerChamp.y + ").");
                        plateau.installerChamp(coordLigColInstallerChamp, joueurActif);
                        sommetsGraphes.put(coordLigColInstallerChamp, joueurActif);
                    }
                } else {
                    joueurActifASupprimer = true;
                }
                if (joueurActifASupprimer) {
                    joueursActifs.remove(joueurActif);
                    System.out.println("    Le joueur actif ne peut plus poser de champ.");
                }
            }
            joueurActif = partieEnCours.joueurSuivant(joueurActif);
        }
        etat = EtatManche.FIN;
    }

    /**
     * Fin de la manche.
     */
    private void toursJeuFin() {
        // Scores.
        toursJeuFinScores();
        // Affichage des points de victoire des joueurs.
        for (Entry<Joueur, Integer> joueurNbPointsVictoire : pointsVictoire.entrySet()) {
            System.out.println("  Le joueur " + joueurNbPointsVictoire.getKey().getNom() + " a obtenu "
                    + joueurNbPointsVictoire.getValue() + " point(s) de victoire pour cette manche.");
        }
        // Joueur gagnant.
        Joueur joueurGagnant = getJoueurGagnant();
        if (joueurGagnant == null) {
            System.out.println("  Aucun gagnant pour cette manche.");
        } else {
            System.out.println("  Le gagnant de cette manche est " + joueurGagnant.getNom() + ".");
        }
        // Fin de manche.
        System.out.println(" Fin d'une manche.");
    }

    /**
     * Calcul des scores en fin de la manche.
     * <p>
     * Attention : private --> public à cause des tests.
     */
    public void toursJeuFinScores() {
        pointsVictoire = new HashMap<Joueur, Integer>();
        partieEnCours.getJoueurs().stream().filter(joueur -> plateau.nbCasesOccupees(joueur, Pion.CHAMP) > 0)
                .forEach(joueur -> pointsVictoire.put(joueur, plateau.nbCasesOccupees(joueur, Pion.CHAMP)));
    }

    /**
     * Programme principal.
     *
     * @param args Arguments.
     */
    public static void main(String[] args) throws Exception {
        Partie partie;
        final int NB_ITERATIONS = 1; // 42;
        for (int cpt = 1; cpt <= NB_ITERATIONS; ++cpt) {
            partie = new Partie();
            (new Manche(partie, partie.getJoueurs().get(0))).toursJeu();
        }
    }

}
