/**
 * Tuiles (faces cachées ou visibles) à piocher.
 */
package fjord;

/**
 * Tuiles (faces cachées ou visibles) à piocher.
 * 
 * @author Olivier
 */
public class TuileAPiocher {

	/**
	 * Tuile.
	 */
	private Tuile tuile;

	/**
	 * Indique si la tuile est dans la pioche des tuiles cachées (visibles
	 * sinon).
	 */
	private boolean estCachee;

	/**
	 * Création d'une tuile (face cachée ou visible) à piocher.
	 */
	/**
	 * @param tuile
	 *            Tuile.
	 * @param estCachee
	 *            Indique si la tuile est dans la pioche des tuiles cachées
	 *            (visibles sinon).
	 */
	public TuileAPiocher(final Tuile tuile, final boolean estCachee) {
		if (tuile == null) {
			throw new IllegalArgumentException("La tuile doit être renseignée.");
		}
		this.tuile = tuile;
		this.estCachee = estCachee;
	}

	/**
	 * Tuile.
	 * 
	 * @return Tuile.
	 */
	public Tuile getTuile() {
		return tuile;
	}

	/**
	 * Indique si la tuile est dans la pioche des tuiles cachées (visibles
	 * sinon).
	 * 
	 * @return Indique si la tuile est dans la pioche des tuiles cachées
	 *         (visibles sinon).
	 */
	public boolean estCachee() {
		return estCachee;
	}

	/**
	 * Rend visible une tuile cachée (pour la changer de pioche).
	 */
	public void setVisible() {
		estCachee = false;
	}

}
