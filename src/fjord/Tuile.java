/**
 * Tuile (de terrain).
 */
package fjord;

import java.util.Arrays;

/**
 * Tuile (de terrain).
 * 
 * @author Olivier
 */

/* Les lettres de l'étiquette correspondent aux codes du terrain, */
/* pour chaque angle, et sont données dans le sens trigonométrique, */
/* en partant du point le plus haut : */
/* ~~~~1re~~~ */
/* ~~~~/\~~~~ */
/* ~~~/~~\~~~ */
/* 2e/~~~~\6e */
/* ~|~~~~~~|~ */
/* ~|~~~~~~|~ */
/* ~|~~~~~~|~ */
/* 3e\~~~~/5e */
/* ~~~\~~/~~~ */
/* ~~~~\/~~~~ */
/* ~~~~4e~~~~ */

public enum Tuile {

	/**
	 * Tuile "AAAAAA".
	 */
	AAAAAA(2 + 1),
	/**
	 * Tuile "AAAAAB".
	 */
	AAAAAB(1),
	/**
	 * Tuile "AAAAAC".
	 */
	AAAAAC(2 + 1),
	/**
	 * Tuile "AAAABB".
	 */
	AAAABB(1),
	/**
	 * Tuile "AAAACC".
	 */
	AAAACC(5),
	/**
	 * Tuile "AAABBB".
	 */
	AAABBB(5),
	/**
	 * Tuile "AAACAC".
	 */
	AAACAC(1),
	/**
	 * Tuile "AAACCC".
	 */
	AAACCC(3),
	/**
	 * Tuile "AABAAC".
	 */
	AABAAC(1),
	/**
	 * Tuile "AABBAC".
	 */
	AABBAC(1),
	/**
	 * Tuile "AABBBB".
	 */
	AABBBB(4),
	/**
	 * Tuile "AACACC".
	 */
	AACACC(1),
	/**
	 * Tuile "AACCAB".
	 */
	AACCAB(1 + 1),
	/**
	 * Tuile "AACCCC".
	 */
	AACCCC(2),
	/**
	 * Tuile "ABBACC".
	 */
	ABBACC(1),
	/**
	 * Tuile "ABBBAC".
	 */
	ABBBAC(2),
	/**
	 * Tuile "ABBBBB".
	 */
	ABBBBB(1),
	/**
	 * Tuile "ACCACC".
	 */
	ACCACC(1),
	/**
	 * Tuile "ACCCCC".
	 */
	ACCCCC(1),
	/**
	 * Tuile "CCCCCC".
	 */
	CCCCCC(1);

	/**
	 * Nombre d'exemplaires.
	 */
	private final int nbExemplaires;

	/**
	 * Nombre total d'exemplaires.
	 */
	public static final int TOTAL_NB_EXEMPLAIRES = 40;

	/**
	 * Création d'une tuile.
	 * 
	 * @param nbExemplaires
	 *            Nombre d'exemplaires.
	 */
	private Tuile(final int nbExemplaires) {
		this.nbExemplaires = nbExemplaires;
	}

	/**
	 * Nombre d'exemplaires.
	 * 
	 * @return Nombre d'exemplaires.
	 */
	public int getNbExemplaires() {
		return nbExemplaires;
	}

	/**
	 * L'installation d'un pion est-elle possible sur cette tuile ?
	 * 
	 * @return L'installation d'un pion est-elle possible sur cette tuile ?
	 */
	public boolean estInstallationPossible() {
		return Arrays.asList(this.toString().split("")).stream()
				.anyMatch(codeTerrain -> Terrain.getTerrain(codeTerrain.charAt(0)).estInstallationPossible());
	}

}
