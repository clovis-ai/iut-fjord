/**
 * Pions (des joueurs).
 */
package fjord;

/**
 * Pions (des joueurs)
 * 
 * @author Olivier
 */
public enum Pion {

	/**
	 * Hutte.
	 */
	HUTTE(4),
	/**
	 * Champ.
	 */
	CHAMP(20);

	/**
	 * Nombre d'exemplaires du pion.
	 */
	private final int nbExemplaires;

	/**
	 * Création d'un pion.
	 * 
	 * @param nbExemplaires
	 *            Nombre d'exemplaires du pion
	 */
	private Pion(final int nbExemplaires) {
		this.nbExemplaires = nbExemplaires;
	}

	/**
	 * Nombre d'exemplaires du pion
	 * 
	 * @return Nombre d'exemplaires du pion
	 */
	public int getNbExemplaires() {
		return nbExemplaires;
	}

}
