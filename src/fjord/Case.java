/**
 * Cases du plateau.
 */
package fjord;

/**
 * Cases du plateau.
 * 
 * @author Olivier
 */
public class Case {

	/**
	 * Tuile orientée.
	 */
	private TuileOrientee tuileOrientee;

	/**
	 * Joueur.
	 */
	private Joueur joueur;

	/**
	 * Pion.
	 */
	private Pion pion;

	/**
	 * Création d'une case du plateau.
	 * 
	 * @param tuileOrientee
	 *            Tuile orientée.
	 * @param joueur
	 *            Joueur.
	 * @param pion
	 *            Pion.
	 */
	public Case(final TuileOrientee tuileOrientee, final Joueur joueur, final Pion pion) {
		if (tuileOrientee == null) {
			throw new IllegalArgumentException("La tuile orientée doit être renseignée.");
		}
		if (joueur == null && pion != null) {
			throw new IllegalArgumentException("Le joueur est renseigné mais pas le pion.");
		}
		if (joueur != null && pion == null) {
			throw new IllegalArgumentException("Le pion est renseigné mais pas le joueur.");
		}
		this.tuileOrientee = tuileOrientee;
		this.joueur = joueur;
		this.pion = pion;
	}

	/**
	 * Tuile orientée.
	 * 
	 * @return Tuile orientée.
	 */
	public TuileOrientee getTuileOrientee() {
		return tuileOrientee;
	}

	/**
	 * Joueur.
	 * 
	 * @return Joueur.
	 */
	public Joueur getJoueur() {
		return joueur;
	}

	/**
	 * Pion.
	 * 
	 * @return Pion.
	 */
	public Pion getPion() {
		return pion;
	}

	/**
	 * Indique si la case est inoccupée (pas de pion d'un joueur).
	 * 
	 * @return Indique si la case est inoccupée.
	 */
	public boolean estInoccupee() {
		return joueur == null; // ou pion == null
	}

}
