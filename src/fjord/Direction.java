/**
 * Direction (pour le plateau).
 */
package fjord;

import java.awt.Point;
import java.util.Arrays;

/**
 * Direction (pour le plateau).
 * 
 * @author Olivier
 */

/* ~~~/~\~~~/~\~~~~ */
/* ~~/~~~\~/~~~\~~~ */
/* ~~|~~~~|~~~~|~~~ */
/* ~~|120°|~60°|~~~ */
/* ~~|~~~~|~~~~|~~~ */
/* ~~/\~~~/\~~~/\~~ */
/* ~/~~\~/~~\~/~~\~ */
/* |~~~~|~~~~|~~~~| */
/* |180°|~~~~|~0°~| */
/* |~~~~|~~~~|~~~~| */
/* ~\~~/~\~~/~\~~/~ */
/* ~~\/~~~\/~~~\/~~ */
/* ~~|~~~~|~~~~|~~~ */
/* ~~|240°|300°|~~~ */
/* ~~|~~~~|~~~~|~~~ */
/* ~~\~~~/~\~~~/~~~ */
/* ~~~\~/~~~\~/~~~~ */

public enum Direction {

	/**
	 * Direction de 0°.
	 */
	DIRECTION_0_DEGRE(0, 180),
	/**
	 * Direction de 60°.
	 */
	DIRECTION_60_DEGRES(60, 240),
	/**
	 * Direction de 120°.
	 */
	DIRECTION_120_DEGRES(120, 300),
	/**
	 * Direction de 180°.
	 */
	DIRECTION_180_DEGRES(180, 0),
	/**
	 * Direction de 240°.
	 */
	DIRECTION_240_DEGRES(240, 60),
	/**
	 * Direction de 3000°.
	 */
	DIRECTION_300_DEGRES(300, 120);

	/**
	 * Direction (en degrés).
	 */
	private final int directionDegres;

	/**
	 * Direction (en degrés) opposée.
	 * 
	 * N. B. : comme il n'est pas possible de spécifier directement la direction
	 * opposée (car l'étiquette n'est pas forcément créée auparavant), on gère
	 * la direction (en degrés) opposée.
	 */
	private final int directionDegresOpposee;

	/**
	 * Nombre de directions.
	 */
	public static final int NB_DIRECTIONS = Direction.values().length;

	/**
	 * Création d'une direction.
	 * 
	 * @param directionDegres
	 *            Direction (en degrés).
	 * @param directionDegresOpposee
	 *            Direction (en degrés) opposée.
	 */
	private Direction(final int directionDegres, final int directionDegresOpposee) {
		this.directionDegres = directionDegres;
		this.directionDegresOpposee = directionDegresOpposee;
	}

	/**
	 * Direction (en degrés).
	 * 
	 * @return Direction (en degrés).
	 */
	public int getDirectionDegres() {
		return directionDegres;
	}

	/**
	 * Direction (en degrés) opposée.
	 * 
	 * @return Direction (en degrés) opposée.
	 */
	public int getDirectionDegresOpposee() {
		return directionDegresOpposee;
	}

	/**
	 * Direction opposée.
	 * 
	 * @return Direction opposée.
	 */
	public Direction directionOpposee() {
		return Arrays.stream(Direction.values())
				.filter(direction -> direction.directionDegres == directionDegresOpposee).findFirst().orElse(null);
	}

	/**
	 * Coordonnées (ligne et colonne) de la case (du plateau) de destination à
	 * partir des coordonnées d'une case (du plateau) source et de la direction.
	 * 
	 * @param coordLigColSource
	 *            Coordonnées d'une case (du plateau) source.
	 * @return Coordonnées (ligne et colonne) de la case (du plateau) de
	 *         destination.
	 */
	public Point coordLigColDestination(final Point coordLigColSource) {
		Point coordLigColDestination = new Point();
		switch (this) {
		case DIRECTION_0_DEGRE:
			coordLigColDestination.x = coordLigColSource.x;
			coordLigColDestination.y = coordLigColSource.y + 1;
			break;
		case DIRECTION_60_DEGRES:
			coordLigColDestination.x = coordLigColSource.x - 1;
			coordLigColDestination.y = coordLigColSource.y + (coordLigColSource.x % 2 == 0 ? +1 : 0);
			break;
		case DIRECTION_120_DEGRES:
			coordLigColDestination.x = coordLigColSource.x - 1;
			coordLigColDestination.y = coordLigColSource.y + (coordLigColSource.x % 2 == 0 ? 0 : -1);
			break;
		case DIRECTION_180_DEGRES:
			coordLigColDestination.x = coordLigColSource.x;
			coordLigColDestination.y = coordLigColSource.y - 1;
			break;
		case DIRECTION_240_DEGRES:
			coordLigColDestination.x = coordLigColSource.x + 1;
			coordLigColDestination.y = coordLigColSource.y + (coordLigColSource.x % 2 == 0 ? 0 : -1);
			break;
		case DIRECTION_300_DEGRES:
			coordLigColDestination.x = coordLigColSource.x + 1;
			coordLigColDestination.y = coordLigColSource.y + (coordLigColSource.x % 2 == 0 ? +1 : 0);
			break;
		}
		return coordLigColDestination;
	}

	/**
	 * Direction de la case source vers la case destination lorsque les
	 * coordonnées (ligne et colonne) correspondent à deux cases (du plateau)
	 * adjacentes.
	 * 
	 * @param coordLigColSource
	 *            Coordonnées d'une case (du plateau) source.
	 * @param coordLigColDestination
	 *            Coordonnées d'une case (du plateau) destination.
	 * @return Direction de la case source vers la case destination lorsque les
	 *         coordonnées (ligne et colonne) correspondent à deux cases (du
	 *         plateau) adjacentes, ou null si elles ne sont pas adjacentes.
	 */
	public static Direction direction2CasesAdjacentes(final Point coordLigColSource,
			final Point coordLigColDestination) {
		return Arrays.stream(Direction.values())
				.filter(direction -> direction.coordLigColDestination(coordLigColSource).equals(coordLigColDestination))
				.findFirst().orElse(null);
	}

}
